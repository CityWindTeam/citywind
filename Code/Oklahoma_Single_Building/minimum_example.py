from dolfin import *
from mshr import *
domain = Surface3D("test_16x16x10.stl")
mesh = generate_mesh(domain,10)
plot(mesh, interactive=True)

# This script test osb.py with manufactured solution for the convergence rate as function of mesh fineness.

from fenics import *
from dolfin import *
from mshr import *
from scipy import *
from scipy.interpolate import RegularGridInterpolator
#from astropy.io import ascii

# import arcpy
import numpy as np


# This is based on "poisson_mesh.py" and should encompass oklahoma_single_building topography and initial velocity fields as set up by NUD-Fortran environement.

#### Read initial velocity fields and griddata
N = 4;

for idx_i in range (1,N):
	NN = 4*pow(2,idx_i)
	h = 1.0/NN
	if idx_i == 1:
		h_vector = np.array([h])
	else:
		h_vector = np.append(h_vector,[h])

	u0_car = loadtxt("u0.txt", comments="#", delimiter=",", unpack=False)
	v0_car = loadtxt("v0.txt", comments="#", delimiter=",", unpack=False)
	w0_car = loadtxt("w0.txt", comments="#", delimiter=",", unpack=False)
	#u0v0w0_RGI = loadtxt("u0v0w0_RGI.txt", dtype={'formats':('S1','S1','S1')}, delimiter=" ", unpack=False)
	#u0v0w0_RGI = np.loadtxt("u0v0w0_RGI.txt", dtype={'names': ('gender', 'age', 'weight'), 'formats': ('f', 'f', 'f')})
	u0v0w0_RGI = loadtxt("u0v0w0_RGI.txt")
	#u0_RGI = loadtxt("u0_RGI.txt", comments="#", delimiter=",", unpack=False)
	#v0_RGI = loadtxt("v0_RGI.txt", comments="#", delimiter=",", unpack=False)
	#w0_RGI = loadtxt("u0_RGI.txt", comments="#", delimiter=",", unpack=False)
	#GRID_ORIGOZ = 0
	#GRID_ORIGOX = loadtxt("GRID%ORIGOX.txt", comments="#", delimiter=",", unpack=False)
	#GRID_ORIGOY = loadtxt("GRID%ORIGOY.txt", comments="#", delimiter=",", unpack=False)
	GRID_XMIN = loadtxt("GRID%XMIN.txt", comments="#", delimiter=",", unpack=False)
	GRID_XMAX = loadtxt("GRID%XMAX.txt", comments="#", delimiter=",", unpack=False)
	GRID_YMIN = loadtxt("GRID%YMIN.txt", comments="#", delimiter=",", unpack=False)
	GRID_YMAX = loadtxt("GRID%YMAX.txt", comments="#", delimiter=",", unpack=False)
	GRID_ZMIN = loadtxt("GRID%ZMIN.txt", dtype='float', comments="#", delimiter=",", unpack=False)
	GRID_ZMAX = loadtxt("GRID%ZMAX.txt", dtype='float', comments="#", delimiter=",", unpack=False)
	GRID_DX = loadtxt("GRID%DX.txt", comments="#", delimiter=",", unpack=False)
	GRID_DY = loadtxt("GRID%DY.txt", comments="#", delimiter=",", unpack=False)
	GRID_DZ = loadtxt("GRID%DZ.txt", comments="#", delimiter=",", unpack=False)
	#GRID_NCOLS = loadtxt("GRID%NCOLS.txt", comments="#", delimiter=",", unpack=False)
	#GRID_NROWS = loadtxt("GRID%NROWS.txt", comments="#", delimiter=",", unpack=False)
	GRID_THETA = loadtxt("GRID%THETA.txt", comments="#", delimiter=",", unpack=False)
	GRID_NX = loadtxt("GRID%NX.txt",dtype='int', comments="#", delimiter=",", unpack=False)
	GRID_NY = loadtxt("GRID%NY.txt",dtype='int', comments="#", delimiter=",", unpack=False)
	GRID_NZ = loadtxt("GRID%NZ.txt",dtype='int', comments="#", delimiter=",", unpack=False)

	Vertices_shp = loadtxt("Vertices_shp.txt", dtype='float', delimiter="\t", comments="#", unpack=False)

	#### Topography and mesh
	#Read .off file that was created manually
	#if not has_cgal():
	#    print "DOLFIN must be compiled with CGAL to run this demo."Vertices_shp
	#    exit(0)
	mesh = Mesh()  #empty mesh
	domain = Surface3D("oklahoma_single_building.off")
	mesh = generate_mesh(domain,NN)
	#plot(mesh, interactive=True)

	#### Define function space
	#V = FunctionSpace(mesh, 'CG', 1) # so that dofs are only in mesh vertices
	V = FunctionSpace(mesh, 'P', 1) # are dofs only in mesh vertices?
	#V = FunctionSpace(mesh, 'DG', 1) # are dofs only in mesh vertices?

	#VFS = VectorFunctionSpace(mesh, 'CG', 1, dim=3)
	VFS = VectorFunctionSpace(mesh, 'P', 1, dim=3)
	n = V.dim()
	d = mesh.geometry().dim()



	#### Define boundary conditions chapter 4.4.4. in the Tutorial and conditions is set out by manual drawing: These boundary conditions are such that the volume aligns with the flow direction, the inflow is thus in line with to x[0]. The inflow is constant (u0) over the leading face (Neumann, dlambda/dn=0) whereas the outflow may vary freely (Dirichlet, lambda=0). No flow is allowed to exit in other directions, x[1] and x[2], hence Neumann, dlambda/dn=0 (and v0=w0=0 in the initial field.) //CL

	# First mark the different boundaries using MeshFunction "facetfunction" and SubDomain.
	 
	boundary_markers = FacetFunction('size_t',mesh)
	boundary_markers.set_all(9999) # This is beacause default is SubDomain 0 for the facets which coincides with the numbering of one of the SubDomains defined explicitly below.

	class Wholeboundary(SubDomain): #This SubDomain is created for debugging purposes to see that all vertices correctly belong to any of the correct subdomains
	    def inside(self, x, on_boundary):
	       return on_boundary

	bx500 = Wholeboundary()
	bx500.mark(boundary_markers, 500)

	class BoundaryX0(SubDomain): # spacedomain x[0] positive; outflow boundary
	#Define the plane spanned by points 11,12,15,14 in 1-based indexing, or 10,11,14,13 in 0-based indexing  
		vector1 = [Vertices_shp[14][0] - Vertices_shp[10][0], Vertices_shp[14][1] - Vertices_shp[10][1], Vertices_shp[14][2] - Vertices_shp[10][2]]

		vector2 = [Vertices_shp[11][0] - Vertices_shp[10][0], Vertices_shp[11][1] - Vertices_shp[10][1], Vertices_shp[11][2] - Vertices_shp[10][2]]

		cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

		a = cross_product[0]
		b = cross_product[1]
		c = cross_product[2]
		d = - (cross_product[0] * Vertices_shp[10][0] + cross_product[1] * Vertices_shp[10][1] + cross_product[2] * Vertices_shp[10][2])
	#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

		norm=np.linalg.norm([a,b,c])
		nx0=a/norm
		ny0=b/norm
		nz0=c/norm

		def inside(self, x, on_boundary):
			tol = 1E-4
			return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

	bx0 = BoundaryX0()
	bx0.mark(boundary_markers, 0)	

	class BoundaryX1(SubDomain): # building: dim x[0] positive
	#Define the plane spanned by points 3,4,7,8 in 1-based indexing, or 2,3,6,7 in 0-based indexing  	
		vector1 = [Vertices_shp[7][0] - Vertices_shp[3][0], Vertices_shp[7][1] - Vertices_shp[3][1], 	Vertices_shp[7][2] - Vertices_shp[3][2]]

		vector2 = [Vertices_shp[2][0] - Vertices_shp[3][0], Vertices_shp[2][1] - Vertices_shp[3][1], 	Vertices_shp[2][2] - Vertices_shp[3][2]]

		cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

		a = cross_product[0]
		b = cross_product[1]
		c = cross_product[2]
		d = - (cross_product[0] * Vertices_shp[3][0] + cross_product[1] * Vertices_shp[3][1] + 		cross_product[2] * Vertices_shp[3][2])
	#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

		norm=np.linalg.norm([a,b,c])
		nx1=a/norm
		ny1=b/norm
		nz1=c/norm

		def inside(self, x, on_boundary):
			tol = 1E-2 #I decrease from machine tolerance because of propating errors while finding the equation of the plane as the precision of the numbers in vertices_shp and in the .off file is perhaps not the same
			return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

	bx1 = BoundaryX1()
	bx1.mark(boundary_markers, 1)


	class BoundaryX2(SubDomain): # building x[2] positive (Roof of building)
			
		nx2=0
		ny2=0
		nz2=-1

		def inside(self, x, on_boundary):
			tol = 1E-4
			return on_boundary and near(x[2], Vertices_shp[5][2], tol) # Building has uniform height
	bx2 = BoundaryX2()
	bx2.mark(boundary_markers, 2)


	class BoundaryX3(SubDomain): # building: dim x[0] negative
	#Define the plane spanned by points 1,2,6,5 in 1-based indexing, or 0,1,5,4 in 0-based indexing  	
		vector1 = [Vertices_shp[1][0] - Vertices_shp[0][0], Vertices_shp[1][1] - Vertices_shp[0][1], Vertices_shp[1][2] - Vertices_shp[0][2]]

		vector2 = [Vertices_shp[4][0] - Vertices_shp[0][0], Vertices_shp[4][1] - Vertices_shp[0][1], Vertices_shp[4][2] - Vertices_shp[0][2]]
		cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

		a = cross_product[0]
		b = cross_product[1]
		c = cross_product[2]
		d = - (cross_product[0] * Vertices_shp[0][0] + cross_product[1] * Vertices_shp[0][1] + cross_product[2] * Vertices_shp[0][2])
	#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

		norm=np.linalg.norm([a,b,c])
		nx3=a/norm
		ny3=b/norm
		nz3=c/norm

		def inside(self, x, on_boundary):
			tol = 1E-2 #I decrease from machine tolerance because of propating errors while finding the equation of the plane as the precision of the numbers in vertices_shp and in the .off file is perhaps not the same
			return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

	bx3 = BoundaryX3()
	bx3.mark(boundary_markers, 3)

	class BoundaryX4(SubDomain): # building: dim x[1] positive
	#Define the plane spanned by points 2,3,6,7 in 1-based indexing, or 1,2,5,6 in 0-based indexing  	
		vector1 = [Vertices_shp[6][0] - Vertices_shp[2][0], Vertices_shp[6][1] - Vertices_shp[2][1], Vertices_shp[6][2] - Vertices_shp[2][2]]

		vector2 = [Vertices_shp[1][0] - Vertices_shp[2][0], Vertices_shp[1][1] - Vertices_shp[2][1], Vertices_shp[1][2] - Vertices_shp[2][2]]
		cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

		a = cross_product[0]
		b = cross_product[1]
		c = cross_product[2]
		d = - (cross_product[0] * Vertices_shp[2][0] + cross_product[1] * Vertices_shp[2][1] + cross_product[2] * Vertices_shp[2][2])
	#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

		norm=np.linalg.norm([a,b,c])
		nx4=a/norm
		ny4=b/norm
		nz4=c/norm

		def inside(self, x, on_boundary):
			tol = 1E-2 #I decrease from machine tolerance because of propating errors while finding the equation of the plane as the precision of the numbers in vertices_shp and in the .off file is perhaps not the same
			return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

	bx4 = BoundaryX4()
	bx4.mark(boundary_markers, 4)

	class BoundaryX5(SubDomain):# building: dim x[1] negative
	#Define the plane spanned by points 1,4,5,8 in 1-based indexing, or 0,3,4,7 in 0-based indexing  	
		vector1 = [Vertices_shp[4][0] - Vertices_shp[0][0], Vertices_shp[4][1] - Vertices_shp[0][1], Vertices_shp[4][2] - Vertices_shp[0][2]]

		vector2 = [Vertices_shp[3][0] - Vertices_shp[0][0], Vertices_shp[3][1] - Vertices_shp[0][1], Vertices_shp[3][2] - Vertices_shp[0][2]]
		cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

		a = cross_product[0]
		b = cross_product[1]
		c = cross_product[2]
		d = - (cross_product[0] * Vertices_shp[0][0] + cross_product[1] * Vertices_shp[0][1] + cross_product[2] * Vertices_shp[0][2])
	#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

		norm=np.linalg.norm([a,b,c])
		nx5=a/norm
		ny5=b/norm
		nz5=c/norm

		def inside(self, x, on_boundary):
			tol = 1E-2 #I decrease from machine tolerance because of propating errors while finding the equation of the plane as the precision of the numbers in vertices_shp and in the .off file is perhaps not the same
			return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)
	bx5 = BoundaryX5()
	bx5.mark(boundary_markers, 5)

	class BoundaryX6(SubDomain): # spacedomain x[0] negative
	#Define the plane spanned by points 9,10,13,16 in 1-based indexing, or 8,9,12,15 in 0-based indexing  	
		vector1 = [Vertices_shp[12][0] - Vertices_shp[8][0], Vertices_shp[12][1] - Vertices_shp[8][1], Vertices_shp[12][2] - Vertices_shp[8][2]]

		vector2 = [Vertices_shp[9][0] - Vertices_shp[8][0], Vertices_shp[9][1] - Vertices_shp[8][1], Vertices_shp[9][2] - Vertices_shp[8][2]]
		cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

		a = cross_product[0]
		b = cross_product[1]
		c = cross_product[2]
		d = - (cross_product[0] * Vertices_shp[8][0] + cross_product[1] * Vertices_shp[8][1] + cross_product[2] * Vertices_shp[8][2])
	#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

		norm=np.linalg.norm([a,b,c])
		nx6=a/norm
		ny6=b/norm
		nz6=c/norm

		def inside(self, x, on_boundary):
			tol = 1E-4
			return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

	bx6 = BoundaryX6()
	bx6.mark(boundary_markers, 6)

	class BoundaryX7(SubDomain): # spacedomain x[2] negative
		
		nx7=0
		ny7=0
		nz7=-1

		def inside(self, x, on_boundary):
			tol = 1E-4
			return on_boundary and near(x[2], 0, tol)

	bx7 = BoundaryX7()
	bx7.mark(boundary_markers, 7)

	class BoundaryX8(SubDomain): # spacedomain x[1] positive
	#Define the plane spanned by points 10,11,15,16 in 1-based indexing, or 9,10,14,15 in 0-based indexing  	
		vector1 = [Vertices_shp[15][0] - Vertices_shp[9][0], Vertices_shp[15][1] - Vertices_shp[9][1], Vertices_shp[15][2] - Vertices_shp[9][2]]

		vector2 = [Vertices_shp[10][0] - Vertices_shp[9][0], Vertices_shp[10][1] - Vertices_shp[9][1], Vertices_shp[10][2] - Vertices_shp[9][2]]
		cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

		a = cross_product[0]
		b = cross_product[1]
		c = cross_product[2]
		d = - (cross_product[0] * Vertices_shp[9][0] + cross_product[1] * Vertices_shp[9][1] + cross_product[2] * Vertices_shp[9][2])
	#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

		norm=np.linalg.norm([a,b,c])
		nx8=a/norm
		ny8=b/norm
		nz8=c/norm

		def inside(self, x, on_boundary):
			tol = 1E-4
			return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

	bx8 = BoundaryX8()
	bx8.mark(boundary_markers, 8)

	class BoundaryX9(SubDomain): # spacedomain x[1] negative
	#Define the plane spanned by points 9,12,13,14 in 1-based indexing, or 8,11,12,13 in 0-based indexing  	
		vector1 = [Vertices_shp[13][0] - Vertices_shp[11][0], Vertices_shp[13][1] - Vertices_shp[11][1], Vertices_shp[13][2] - Vertices_shp[11][2]]

		vector2 = [Vertices_shp[8][0] - Vertices_shp[11][0], Vertices_shp[8][1] - Vertices_shp[11][1], Vertices_shp[8][2] - Vertices_shp[11][2]]
		cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

		a = cross_product[0]
		b = cross_product[1]
		c = cross_product[2]
		d = - (cross_product[0] * Vertices_shp[11][0] + cross_product[1] * Vertices_shp[11][1] + cross_product[2] * Vertices_shp[11][2])
	#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

		norm=np.linalg.norm([a,b,c])
		nx9=a/norm
		ny9=b/norm
		nz9=c/norm

		def inside(self, x, on_boundary):
			tol = 1E-4
			return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)
	bx9 = BoundaryX9()
	bx9.mark(boundary_markers, 9)

	class BoundaryX10(SubDomain): # spacedomain x[2] positive
		
		nz10=0
		ny10=0
		nz10=1

		def inside(self, x, on_boundary):
			tol = 1E-4
			return on_boundary and near(x[2], Vertices_shp[15][2], tol)

	bx10 = BoundaryX10()
	bx10.mark(boundary_markers, 10)


	#### Create the manufactured solution

	# Do not use SymPy
	n=FacetNormal(mesh)

	#GRID_ORIGOX=Constant(634648)
	#GRID_ORIGOY=Constant(3926132)
	#GRID_ORIGOZ = Constant(55)

	# exact solution quadratic
	u_e=Expression('1+x[0]*x[0]+x[1]*x[1]+x[2]*x[2]', degree=5)	
	neu_expr_x=Expression('2*x[0]', degree=5)
	neu_expr_y=Expression('2*x[1]', degree=5)
	neu_expr_z=Expression('2*x[2]', degree=5)
	f = Constant(-6.0) # -Laplace(u_e) #the source term

	# exact solution linear
	#u_e=Expression('1+(x[0]-GRID_ORIGOX)+(x[1]-GRID_ORIGOY)+(x[2]-GRID_ORIGOZ)', GRID_ORIGOX=GRID_ORIGOX, GRID_ORIGOY=GRID_ORIGOY, GRID_ORIGOZ=GRID_ORIGOZ, degree=5)
	#u_e=Expression('1+x[0]+x[1]+x[2]', degree=5)
	#neu_expr_x=Expression('1', degree=5)
	#neu_expr_y=Expression('1', degree=5)
	#neu_expr_z=Expression('1', degree=5)
	#f = Constant(0) # -Laplace(u_e) #the source term

	# Alternative 1
	#vertex_values_neu_expr_x = neu_expr_x.compute_vertex_values(mesh)
	#vertex_values_neu_expr_y = neu_expr_y.compute_vertex_values(mesh)
	#vertex_values_neu_expr_z = neu_expr_z.compute_vertex_values(mesh)
	#neu_vec_x=Function(V)
	#neu_vec_y=Function(V)
	#neu_vec_z=Function(V)
	#neu_vec_x.vector()[:]=vertex_values_neu_expr_x
	#neu_vec_y.vector()[:]=vertex_values_neu_expr_y
	#neu_vec_z.vector()[:]=vertex_values_neu_expr_z
	#neu_vec = as_vector([neu_vec_x, neu_vec_y, neu_vec_z])

	# Alternative 2
	neu_vec_x = interpolate(neu_expr_x, V)
	neu_vec_y = interpolate(neu_expr_y, V)
	neu_vec_z = interpolate(neu_expr_z, V)

	neu_vec = as_vector([neu_vec_x, neu_vec_y, neu_vec_z])
	# print(neu_vec.array())

	# neu_func=project(neu_expr, VFS)
	neu_bc=-dot(neu_vec, n)
	#neu_bc2=Function(V)
	#neu_bc2=project(neu_bc,V)
	# print(neu_bc.array())
	#from IPython import embed; embed()
	# Then specify the form of boundary conditions (which is now different from osb.py since I have a manufactured solution	

	boundary_conditions =  {0: {'Dirichlet': u_e},  # spacedomain x[0] positive; outflow boundary
				1: {'Neumann': neu_bc}, # building: dim x[0] positive 
				2: {'Neumann': neu_bc},   # building x[2] positive (Roof of building)
				3: {'Neumann': neu_bc}, # building: dim x[0] negative
	 			4: {'Neumann': neu_bc},   # building: dim x[1] positive
				5: {'Neumann': neu_bc},   # building: dim x[1] negative
				6: {'Neumann': neu_bc},    # spacedomain x[0] negative
				7: {'Neumann': neu_bc},    # spacedomain x[2] negative
				8: {'Neumann': neu_bc},    # spacedomain x[1] positive
				9: {'Neumann': neu_bc},    # spacedomain x[1] negative
				10: {'Neumann': neu_bc}}   # spacedomain x[2] positive

	#from IPython import embed; embed()

	# Visualizee subdomains, to see that they are correct
	#plot(boundary_markers, interactive=True)
	vtkfile = File('osb_mfs_boundary_markers.pvd')
	vtkfile << boundary_markers 

	# Define variational problem
	LagrangeMultiplier = TrialFunction(V)
	testv = TestFunction(V)

	#Redefine the measure "ds" in terms of our boundary markers
	ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)

	# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are none Robin conditions. 
	integrals_N = []
	for i in boundary_conditions:
		if 'Neumann' in boundary_conditions[i]:
			if boundary_conditions[i]['Neumann'] != 0:
				g = boundary_conditions[i]['Neumann']
				print(g)
				integrals_N.append(g*testv*ds(i))

	# Collect Dirichlet boundary conditions
	bcs = []
	for i in boundary_conditions:
		if 'Dirichlet' in boundary_conditions[i]:
			bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
			bcs.append(bc)
			

	# Continue defining variational problem

	a = dot(grad(LagrangeMultiplier), grad(testv))*dx
	# g = Expression('4*x[1]') #is replaced with other bc's 
	L = f*testv*dx-sum(integrals_N)
	#L = div(u0v0w0)*testv*dx-sum(integrals_N)

	#L = f*v*dx #This is the code for Dirichlet bc's only. Should be equivalent as long as the Neumann-bc-term g=0./CL

	# Compute solution
	LagrangeMultiplier = Function(V)
	solve(a == L, LagrangeMultiplier, bcs)

	# Plot solution
	#LagrangeMultiplier.rename('LagrangeMultiplier', 'LagrangeMultiplier')
	#plot(LagrangeMultiplier, interactive=True)

	# Save solution to file in VTK format

	vtkfile = File('osb_mfs_LagrangeMultiplier.pvd')
	vtkfile << LagrangeMultiplier

	# Save manufactured solution to file in VTK format

	u_e_save=Function(V)
	u_e_save=project(u_e,V)
	#u_e_save.rename('u_e_save', 'Exact Solution')
	#plot(u_e_save, title='Exact solution', interactive=True)
	vtkfile = File('osb_mfs_Manufactured.pvd')
	vtkfile << u_e_save

	#Compare! Errornorm different meshes
	E=errornorm(u_e, LagrangeMultiplier, norm_type='L2')
	#print('Error =', E)
	n_expr = norm(u_e, 'L2', mesh)
	n_func = norm(u_e_save, 'L2', mesh)
	u_e_vec = u_e_save.vector()
	n_vec = norm(u_e_vec, 'L2', mesh)
	rel_error_expr=E/n_expr
	rel_error_func=E/n_func
	rel_error_vec=E/n_vec
	if idx_i == 1:
		error_vector = np.array([rel_error_expr])
	else :
		error_vector = np.append(error_vector,[rel_error_expr])
	print('h_vector: ', h_vector)
	print('error vector: ',error_vector)
	print('Relative Error, rel_error_expr =', rel_error_expr)
	print('Relative Error, rel_error_func =', rel_error_func)
	print('Relative Error, rel_error_vec =', rel_error_vec)

	debug1 = False
	if debug1:

		# Print number of vertices in mesh
		#print('%s is the number of vertices in the mesh' % mesh.num_vertices()) 
		print('Number of vertices in the mesh:', mesh.num_vertices())
	        # Print all vertices that belong to the boundary (and 9999 WholeBoundary) parts
		for x in mesh.coordinates():
			#if bx0.inside(x, True): print('%s is on bx0' % x)
			#if bx1.inside(x, True): print('%s is on bx1' % x)
			#if bx2.inside(x, True): print('%s is on bx2' % x)
			#if bx3.inside(x, True): print('%s is on bx3' % x)
			#if bx4.inside(x, True): print('%s is on bx4' % x)
			#if bx5.inside(x, True): print('%s is on bx5' % x)
			#if bx6.inside(x, True): print('%s is on bx6' % x)
			#if bx7.inside(x, True): print('%s is on bx7' % x)
			#if bx8.inside(x, True): print('%s is on bx8' % x)
			#if bx9.inside(x, True): print('%s is on bx9' % x)
			#if bx10.inside(x, True): print('%s is on bx10' % x)
			#if bx9999.inside(x, True): print('%s is on bx9999' % x)
			if bx500.inside(x, True) and bx0.inside(x, False) and bx1.inside(x, False) and bx2.inside(x, False) and bx3.inside(x, False) and bx4.inside(x, False) and bx5.inside(x, False) and bx6.inside(x, False) and bx7.inside(x, False) and bx8.inside(x, False) and bx9.inside(x, False) and bx10.inside(x, False): print('%s is exlusively on bx500' % x)
			#if bx500.inside(x, True): print('%s is on bx500' % x)

	        # Print the Dirichlet conditions
	        print('Number of Dirichlet conditions:', len(bcs))
	        if V.ufl_element().degree() == 1:  # P1 elements
	            d2v = dof_to_vertex_map(V)
	            coor = mesh.coordinates()
	            for i, bc in enumerate(bcs):
	                print('Dirichlet condition %d' % i)
	                boundary_values = bc.get_boundary_values()
			print ('Number of boundary values in the above Dirichlet condition:', len(bc.get_boundary_values()))
	#                for dof in boundary_values:
	#                    print('   dof %2d: u = %g' % (dof, boundary_values[dof]))
	#                    if V.ufl_element().degree() == 1:
	#                        print('    at point %s' %
	#                              (str(tuple(coor[d2v[dof]].tolist()))))
np.savetxt('txt_h.txt', h_vector, delimiter='newline')
np.savetxt('txt_error.txt', error_vector, delimiter='newline')

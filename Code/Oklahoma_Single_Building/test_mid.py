# Here I am trying to plot a VectorFunction on facet midpoints to see that the VectorFunction fulfills boundary conditions (in the plane of the boundary)

from fenics import *
#from dolfin import *
from IPython import embed
from mshr import *
import numpy as np
from fenicstools import interpolate_nonmatching_mesh_any

#Create mesh

N=float(100)
n=float(60)

#Here I am creating small surface geometry with primitives. 
BigBox = Box(Point(0, 0, 0), Point(N, N, N))
SmallBox = Box(Point(N/2-n/2,N/2-n/2,0), Point(N/2+n/2,N/2+n/2,n))
domain = BigBox - SmallBox
mesh = generate_mesh(domain, 10)
plot(mesh, interactive=True)

# Define function spaces
P2 = VectorElement("Lagrange", mesh.ufl_cell(), 2)
P1 = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
TH = P2 * P1
W = FunctionSpace(mesh, TH)

#### Define SubDomains for boundaries and mark

boundary_markers = FacetFunction('size_t',mesh) #FacetFunction is a subclass of MeshFunction
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.
#Whole_boundary = CompiledSubDomain('on_boundary')
#MaxX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_x)).mark(boundary_markers, 1)
#MinX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_x)).mark(boundary_markers, 2)
#MaxY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_y)).mark(boundary_markers, 3)
#MinY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_y)).mark(boundary_markers, 4)
#MaxZ = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_z)).mark(boundary_markers, 5)

MaxX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=N).mark(boundary_markers, 1)
MinX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=0).mark(boundary_markers, 2)
MaxY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=N).mark(boundary_markers, 3)
MinY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=0).mark(boundary_markers, 4)
MaxZ = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=N).mark(boundary_markers, 5)

# The free-slip boundary
MinZ = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=0).mark(boundary_markers, 6)
MaxX_b = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=N/2+n/2).mark(boundary_markers, 7)
MinX_b = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=N/2-n/2).mark(boundary_markers, 8)
MaxY_b = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=N/2+n/2).mark(boundary_markers, 9)
MinY_b = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=N/2-n/2).mark(boundary_markers, 10)
MaxZ_b = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=n).mark(boundary_markers, 11)


# Define free-slip Dirichlet boundary for velocity on SmallBox. Since normals are simply alonged with the Cartersian coordinate axes.
zero = Constant(0.0)
bc6 = DirichletBC(W.sub(0).sub(2), zero, boundary_markers, 6)    # u.n = 0
bc7 = DirichletBC(W.sub(0).sub(0), zero, boundary_markers, 7)    # u.n = 0
bc8 = DirichletBC(W.sub(0).sub(0), zero, boundary_markers, 8)   # u.n = 0
bc9 = DirichletBC(W.sub(0).sub(1), zero, boundary_markers, 9)    # u.n = 0
bc10 = DirichletBC(W.sub(0).sub(1), zero, boundary_markers, 10)    # u.n = 0
bc11 = DirichletBC(W.sub(0).sub(2), zero, boundary_markers, 11)    # u.n = 0

# Define other boundary condition. Inflow boundary condition for velocity at BigBox.
#inflow = Expression(("-sin(x[1]*pi)", "-sin(x[0]*pi)", "0.0"), degree=2)
inflow = Expression(("5.0", "5.0", "0.0"), degree=2)
bc1 = DirichletBC(W.sub(0), inflow, boundary_markers, 1)
bc2 = DirichletBC(W.sub(0), inflow, boundary_markers, 2)
bc3 = DirichletBC(W.sub(0), inflow, boundary_markers, 3)
bc4 = DirichletBC(W.sub(0), inflow, boundary_markers, 4)
bc5 = DirichletBC(W.sub(0), inflow, boundary_markers, 5)


# Collect boundary conditions 
bcs = [bc2, bc4, bc5, bc6, bc7, bc8, bc9, bc10, bc11] # Leave out bc1 abd bc3 for the "pressure gauge"

# Define variational problem
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)
f = Constant((0, 0, 0))
a = (inner(grad(u), grad(v)) - div(v)*p + q*div(u))*dx
L = inner(f, v)*dx

# Compute solution
w = Function(W)
solve(a == L, w, bcs)

# Split the mixed solution using deepcopy
# (needed for further computation on coefficient vector)
(u, p) = w.split(True)

print("Norm of velocity coefficient vector: %.15g" % u.vector().norm("l2"))
print("Norm of pressure coefficient vector: %.15g" % p.vector().norm("l2"))

# # Split the mixed solution using a shallow copy
(u, p) = w.split()

# Save solution in VTK format
#ufile_pvd = File("velocity.pvd")
#ufile_pvd << u
#pfile_pvd = File("pressure.pvd")
#pfile_pvd << p

# Plot solution
plot(u)
plot(p)
interactive()



function out = readTextFile(a)
    fileID = fopen(a,'r');
    formatSpec = '%f';
    out = fscanf(fileID,formatSpec);
# This script is the start to read geometry from shapefile and setup initialfield from that
import numpy as np
import shapefile

#### Setup initial windfield u0

# This data is valid for Oklahoma City according to Jan B. 
z0=Constant(0.6)
Ustar=Constant(0.4891)
U=(Ustar/0.4)*ln(z/z0)
Ux=Constant(1.97) # at z=10m	
Uy=Constant(2.82) # at z=10m
theta=np.arctan(Uy/Ux) # Degrees of incoming wind in "x-y" coordinate system

#### Some commands within PyShp
sf = shapefile.Reader("OKC_SINGLE_BUILDING")
shapeRecs = sf.shapeRecords()

Using expressions to define subdomains

#for shpnum in range(0,len(shapes))
shpnum = sf.iterShapeRecords() #shpnum is all buidlings
for j in shpnum:  # j goes through all buildings

	for i in len(shapeRecs[j].shape.points[:])-1  # i goes through all edges of the j:th building. First and last corner are the same by def

		#Coordinates for corner i and building j
	
		Xcorner[j][i] = shapeRecs[j].shape.points[i][0]
		Ycorner[j][i] = shapeRecs[j].shape.points[i][1]
		Zcorner[j][i] = shapeRecs[j].record[2] # This is the HEIGHT attribute field
		
		# Assume that corners of buildings are defined oriented clockwise in shapefile to keep track of front and lee sides of the building.			
		# Check whether edge is lee or front side of building by comparing angle
		edge_angle=np.arctan((Ycorner[j][i+1]-Ycorner[j][i])/(Xcorner[j][i+1]-Xcorner[j][i]))		
		if  edgeangle > theta and edgeangle < theta+np.pi:
			# assign frontwake
		elif edgeangle < theta and edgeangle > theta-np.pi:
			# assign leewake
		else:
			#Edge of building is aligned with initial wind direction - do nothing







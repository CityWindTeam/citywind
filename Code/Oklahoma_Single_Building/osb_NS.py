from fenics import *
from dolfin import *
from mshr import *
from scipy import *
from scipy.interpolate import RegularGridInterpolator
import numpy as np

# Applying NSSolver to osb geometry and Boundary Conditions 
T = 2.5           # final time
num_steps = 100   # number of time steps
dt = T / num_steps # time step size
mu = 0.001         # dynamic viscosity
rho = 1            # density

#### Read initial velocity fields and griddata

u0_car = loadtxt("u0.txt", comments="#", delimiter=",", unpack=False)
v0_car = loadtxt("v0.txt", comments="#", delimiter=",", unpack=False)
w0_car = loadtxt("w0.txt", comments="#", delimiter=",", unpack=False)
#u0v0w0_RGI = loadtxt("u0v0w0_RGI.txt", dtype={'formats':('S1','S1','S1')}, delimiter=" ", unpack=False)
#u0v0w0_RGI = np.loadtxt("u0v0w0_RGI.txt", dtype={'names': ('gender', 'age', 'weight'), 'formats': ('f', 'f', 'f')})
u0v0w0_RGI = loadtxt("u0v0w0_RGI.txt")
#u0_RGI = loadtxt("u0_RGI.txt", comments="#", delimiter=",", unpack=False)
#v0_RGI = loadtxt("v0_RGI.txt", comments="#", delimiter=",", unpack=False)
#w0_RGI = loadtxt("u0_RGI.txt", comments="#", delimiter=",", unpack=False)
GRID_ORIGOZ = 0
GRID_ORIGOX = loadtxt("GRID%ORIGOX.txt", comments="#", delimiter=",", unpack=False)
GRID_ORIGOY = loadtxt("GRID%ORIGOY.txt", comments="#", delimiter=",", unpack=False)
GRID_XMIN = loadtxt("GRID%XMIN.txt", comments="#", delimiter=",", unpack=False)
GRID_XMAX = loadtxt("GRID%XMAX.txt", comments="#", delimiter=",", unpack=False)
GRID_YMIN = loadtxt("GRID%YMIN.txt", comments="#", delimiter=",", unpack=False)
GRID_YMAX = loadtxt("GRID%YMAX.txt", comments="#", delimiter=",", unpack=False)
GRID_ZMIN = loadtxt("GRID%ZMIN.txt", dtype='float', comments="#", delimiter=",", unpack=False)
GRID_ZMAX = loadtxt("GRID%ZMAX.txt", dtype='float', comments="#", delimiter=",", unpack=False)
GRID_DX = loadtxt("GRID%DX.txt", comments="#", delimiter=",", unpack=False)
GRID_DY = loadtxt("GRID%DY.txt", comments="#", delimiter=",", unpack=False)
GRID_DZ = loadtxt("GRID%DZ.txt", comments="#", delimiter=",", unpack=False)
#GRID_NCOLS = loadtxt("GRID%NCOLS.txt", comments="#", delimiter=",", unpack=False)
#GRID_NROWS = loadtxt("GRID%NROWS.txt", comments="#", delimiter=",", unpack=False)
GRID_THETA = loadtxt("GRID%THETA.txt", comments="#", delimiter=",", unpack=False)
GRID_NX = loadtxt("GRID%NX.txt",dtype='int', comments="#", delimiter=",", unpack=False)
GRID_NY = loadtxt("GRID%NY.txt",dtype='int', comments="#", delimiter=",", unpack=False)
GRID_NZ = loadtxt("GRID%NZ.txt",dtype='int', comments="#", delimiter=",", unpack=False)

Vertices_shp = loadtxt("Vertices_shp.txt", dtype='float', delimiter="\t", comments="#", unpack=False)

#### Topography and mesh
#Read .off file that was created manually
#if not has_cgal():
#    print "DOLFIN must be compiled with CGAL to run this demo."
#    exit(0)
mesh = Mesh()  #empty mesh
domain = Surface3D("oklahoma_single_building.off")
mesh = generate_mesh(domain,8)
#plot(mesh, interactive=True)

#### Define function space
#V = FunctionSpace(mesh, 'CG', 1) # so that dofs are only in mesh vertices
#VFS = VectorFunctionSpace(mesh, 'CG', 1, dim=3)
#ndim = V.dim()
#d = mesh.geometry().dim()

# Define function spaces
#V = VectorFunctionSpace(mesh, 'CG', 1, dim=3)
#Q = FunctionSpace(mesh, 'CG', 1)
V = VectorFunctionSpace(mesh, 'P', 1)
Q = FunctionSpace(mesh, 'P', 1)
ndim = V.dim()
d = mesh.geometry().dim()

#### Find the RHS of the poisson equation (= div (initial velocity field on the FEniCS grid)). 
### Interpolate the initial wind field to the fenics mesh

## FEniCS mesh: coordinates of vertices in numpy format 
#CoorFE2 = np.zeros(mesh.num_vertices()) # This is the wrong ordering (plot of initial windfield is corrupt)
#CoorFE2 = mesh.coordinates()
#CoorFE_d2v = CoorFE2[dof_to_vertex_map(V)]

#np.savetxt("CoorFE_d2v.csv", CoorFE_d2v, delimiter=",")
#CoorFE = V.tabulate_dof_coordinates() # This is the correct ordering
#np.savetxt("tab_dof_coord.csv", CoorFE, delimiter=",")


CoorFE = Q.tabulate_dof_coordinates()
V_dof_coordinates = Q.tabulate_dof_coordinates()
V_dof_coordinates.resize((ndim, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]

# Alternative ways to obtain vertex coordinates
# https://fenicsproject.org/qa/1093/obtaining-vertex-values-after-loading-a-mesh
# https://fenicsproject.org/qa/3975/interpolating-vector-function-from-python-code-to-fenics
# https://fenicsproject.org/qa/3932/accessing-the-coordinates-of-a-degree-of-freedom 

## Cartesian mesh: 
# Coordinates (for the RegularGridInterpolator)!

x_c = linspace(GRID_ORIGOX+GRID_XMIN+0.5*GRID_DX,GRID_ORIGOX+GRID_XMAX-0.5*GRID_DX,GRID_NX)
y_c = linspace(GRID_ORIGOY+GRID_YMIN+0.5*GRID_DY,GRID_ORIGOY+GRID_YMAX-0.5*GRID_DY,GRID_NY)
z_c = linspace(GRID_ZMIN+0.5*GRID_DZ,GRID_ZMAX-0.5*GRID_DZ,GRID_NZ)
# Plot these "simulated" Cartesian coordinates to see if the coincide with NUD-Fortran environment. Print to .vtk


# Initialfield of the Cartesian mesh (for the RegularGridInterpolator)
#The below is applicable (with some error...) when u0 v0 w0 is read 
u0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
v0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
w0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
u0v0w0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ,3))
#ir=0
#for iz in range(0,GRID_NZ):
#	for iy in range(0,GRID_NY):
#		for ix in range(0,GRID_NX):
#			u0_RGI[ix][iy][iz]=u0_car[ir]
#			v0_RGI[ix][iy][iz]=v0_car[ir]
#			w0_RGI[ix][iy][iz]=w0_car[ir]
#			ir+=1

ir=0
for iz in range(0,GRID_NZ):
	for iy in range(0,GRID_NY):
		for ix in range(0,GRID_NX):
			u0v0w0_RGI[ix][iy][iz][0]=u0_car[ir]
			u0v0w0_RGI[ix][iy][iz][1]=v0_car[ir]
			u0v0w0_RGI[ix][iy][iz][2]=w0_car[ir]

			u0_RGI[ix][iy][iz]=u0_car[ir]
			v0_RGI[ix][iy][iz]=v0_car[ir]
			w0_RGI[ix][iy][iz]=w0_car[ir]
			ir+=1

N=GRID_NX*GRID_NY*GRID_NZ

## Interpolation to retrieve initial field defined in the FEniCS mesh.

# Define functions to interpolate each space-dimension of the initialfield
fn_u0 = RegularGridInterpolator((x_c,y_c,z_c), u0_RGI, bounds_error=False, fill_value=None) 
fn_v0 = RegularGridInterpolator((x_c,y_c,z_c), v0_RGI, bounds_error=False, fill_value=None) 
fn_w0 = RegularGridInterpolator((x_c,y_c,z_c), w0_RGI, bounds_error=False, fill_value=None)
 
fn_u0v0w0 = RegularGridInterpolator((x_c,y_c,z_c), u0v0w0_RGI, bounds_error=False, fill_value=None)
 

#Interpolate!
u0_FE=fn_u0(CoorFE)
v0_FE=fn_v0(CoorFE)
w0_FE=fn_w0(CoorFE)
u0v0w0_FE=fn_u0v0w0(CoorFE)

#Assemble to verify with Paraview that the initial wind field is correct.
#u0v0w0_FE2=np.zeros((ndim,3)) ##This yields the same resulting vectorfield
#u0v0w0_FE2[:,0]=u0_FE
#u0v0w0_FE2[:,1]=v0_FE
#u0v0w0_FE2[:,2]=w0_FE

## Transfer to FEniCS "function"
#https://fenicsproject.org/qa/2715/coordinates-u_nodal_values-using-numerical-source-function
u0 = Function(Q)
v0 = Function(Q)
w0 = Function(Q)
u0v0w0 = Function(V)
u0v0w02 = Function(V)
u0.vector()[:] = u0_FE # Also possible to import divergence of the initial field from (i.e. the RHS) from NUD Fortran interpolate with Python and apply to the mesh as a scalar field.
v0.vector()[:] = v0_FE
w0.vector()[:] = w0_FE

#uvw.vector()[:] = u0v0w0_FE
#uvw2.vector()[:] = u0v0w0_FE2
assign(u0v0w0.sub(0), u0)
assign(u0v0w0.sub(1), v0)
assign(u0v0w0.sub(2), w0)


#### Define boundary conditions chapter 4.4.4. in the Tutorial and conditions is set out by manual drawing: These boundary conditions are such that the volume aligns with the flow direction, the inflow is thus in line with to x[0]. The inflow is constant (u0) over the leading face (Neumann, dlambda/dn=0) whereas the outflow may vary freely (Dirichlet, lambda=0). No flow is allowed to exit in other directions, x[1] and x[2], hence Neumann, dlambda/dn=0 (and v0=w0=0 in the initial field.) //CL

# First mark the different boundaries using MeshFunction "facetfunction" and SubDomain.
 
boundary_markers = FacetFunction('size_t',mesh)
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.
	

class InflowBND(SubDomain): # spacedomain x[0] negative; inflow boundary

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], min(V_dof_x), tol)

bx1 = InflowBND()
bx1.mark(boundary_markers, 1)	

class SidesBND(SubDomain): # sides

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and (near(x[1], min(V_dof_y), tol) or near(x[1], max(V_dof_y), tol) or near(x[2], min(V_dof_z), tol) or near(x[2], max(V_dof_z), tol))

bx2 = SidesBND()
bx2.mark(boundary_markers, 2)	

class OutflowBND(SubDomain): # spacedomain x[0] positive; outflow boundary

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], max(V_dof_x), tol)

bx3 = OutflowBND()
bx3.mark(boundary_markers, 3)

# Define boundary conditions

n=FacetNormal(mesh)
inflow_profile = as_vector([u0, v0, w0]) 
#neu_bc=2*alpha*alpha*dot(neu_vec, n)

boundary_conditions =  {0: {'Dirichlet': 0},  # buildings (remaining)
			1: {'Dirichlet': inflow_profile}, # inflow
			2: {'Dirichlet': inflow_profile},	# sides
			3: {'Dirichlet': 0}}   # outflow

## Define boundary conditions
#bcu_inflow = DirichletBC(V, Expression(inflow_profile, degree=2), inflow)
#bcu_walls = DirichletBC(V, Constant((0, 0, 0)), walls)
#bcu_cylinder = DirichletBC(V, Constant((0, 0,0)), cylinder)
#bcp_outflow = DirichletBC(Q, Constant(0), outflow)
#bcu = [bcu_inflow, bcu_walls, bcu_cylinder]
#bcp = [bcp_outflow]

# Collect Dirichlet boundary conditions, u
#bcu = []
#for i in (boundary_conditions-1):
#	if 'Dirichlet' in boundary_conditions[i]:
#		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
#		bcu.append(bc)
		
# Dirichlet boundary conditions, p
bcp = []
bcp_outflow = DirichletBC(Q, boundary_conditions[3]['Dirichlet'], boundary_markers, 3)
bcp = [bcp_outflow]

# Define trial and test functions
u = TrialFunction(V)
v = TestFunction(V)
p = TrialFunction(Q)
q = TestFunction(Q)

# Define functions for solutions at previous and current time steps
u_n = Function(V)
u_  = Function(V)
p_n = Function(Q)
p_  = Function(Q)

# Define expressions used in variational forms
U  = 0.5*(u_n + u)
n  = FacetNormal(mesh)
f  = Constant((0, 0, 0))
k  = Constant(dt)
mu = Constant(mu)
rho = Constant(rho)

# Define symmetric gradient
def epsilon(u):
    return sym(nabla_grad(u))

# Define stress tensor
def sigma(u, p):
    return 2*mu*epsilon(u) - p*Identity(len(u))

# Define variational problem for step 1
F1 = rho*dot((u - u_n) / k, v)*dx \
   + rho*dot(dot(u_n, nabla_grad(u_n)), v)*dx \
   + inner(sigma(U, p_n), epsilon(v))*dx \
   + dot(p_n*n, v)*ds - dot(mu*nabla_grad(U)*n, v)*ds \
   - dot(f, v)*dx
a1 = lhs(F1)
L1 = rhs(F1)

# Define variational problem for step 2
a2 = dot(nabla_grad(p), nabla_grad(q))*dx
L2 = dot(nabla_grad(p_n), nabla_grad(q))*dx - (1/k)*div(u_)*q*dx

# Define variational problem for step 3
a3 = dot(u, v)*dx
L3 = dot(u_, v)*dx - k*dot(nabla_grad(p_ - p_n), v)*dx

# Assemble matrices
A1 = assemble(a1)
A2 = assemble(a2)
A3 = assemble(a3)

# Apply boundary conditions to matrices
[bc.apply(A1) for bc in bcu]
[bc.apply(A2) for bc in bcp]

# Create XDMF files for visualization output
xdmffile_u = File('navier_stokes_osb/velocity2.pvd')
xdmffile_p = File('navier_stokes_osb/pressure2.pvd')

# Create time series (for use in reaction_system.py)
timeseries_u = TimeSeries('navier_stokes_osb/velocity_series')
timeseries_p = TimeSeries('navier_stokes_osb/pressure_series')

# Save mesh to file (for use in reaction_system.py)
File('navier_stokes_osb/osb.xml.gz') << mesh

# Create progress bar
progress = Progress('Time-stepping')
set_log_level(PROGRESS)

# Time-stepping
t = 0
for n in range(num_steps):

    # Update current time
    t += dt

    # Step 1: Tentative velocity step
    b1 = assemble(L1)
    [bc.apply(b1) for bc in bcu]
    solve(A1, u_.vector(), b1, 'bicgstab', 'hypre_amg')

    # Step 2: Pressure correction step
    b2 = assemble(L2)
    [bc.apply(b2) for bc in bcp]
    solve(A2, p_.vector(), b2, 'bicgstab', 'hypre_amg')

    # Step 3: Velocity correction step
    b3 = assemble(L3)
    solve(A3, u_.vector(), b3, 'cg', 'sor')

    # Plot solution

    # Save solution to file (XDMF/HDF5)
    xdmffile_u << (u_, t)
    xdmffile_p << (p_, t)

    # Save nodal values to file
    timeseries_u.store(u_.vector(), t)
    timeseries_p.store(p_.vector(), t)

    # Update previous solution
    u_n.assign(u_)
    p_n.assign(p_)

    # Update progress bar
    progress.update(t / T)
    print('u max:', u_.vector().array().max())







debug1 = True
if debug1:

	# Print number of vertices in mesh
	#print('%s is the number of vertices in the mesh' % mesh.num_vertices()) 
	print('Number of vertices in the mesh:', mesh.num_vertices())
        # Print all vertices that belong to the boundary (and 9999 WholeBoundary) parts
	#for x in mesh.coordinates():
		#if bx0.inside(x, True): print('%s is on bx0' % x)
		#if bx1.inside(x, True): print('%s is on bx1' % x)
		#if bx2.inside(x, True): print('%s is on bx2' % x)
		#if bx3.inside(x, True): print('%s is on bx3' % x)
		#if bx4.inside(x, True): print('%s is on bx4' % x)
		#if bx5.inside(x, True): print('%s is on bx5' % x)
		#if bx6.inside(x, True): print('%s is on bx6' % x)
		#if bx7.inside(x, True): print('%s is on bx7' % x)
		#if bx8.inside(x, True): print('%s is on bx8' % x)
		#if bx9.inside(x, True): print('%s is on bx9' % x)
		#if bx10.inside(x, True): print('%s is on bx10' % x)
		#if bx9999.inside(x, True): print('%s is on bx9999' % x)
		# if bx500.inside(x, True) and bx0.inside(x, False) and bx1.inside(x, False) and bx2.inside(x, False) and bx3.inside(x, False) and bx4.inside(x, False) and bx5.inside(x, False) and bx6.inside(x, False) and bx7.inside(x, False) and bx8.inside(x, False) and bx9.inside(x, False) and bx10.inside(x, False): print('%s is exlusively on bx500' % x)
		#if bx500.inside(x, True): print('%s is on bx500' % x)

        # Print the Dirichlet conditions
        print('Number of Dirichlet conditions:', len(bcs))
        if V.ufl_element().degree() == 1:  # P1 elements
            d2v = dof_to_vertex_map(V)
            coor = mesh.coordinates()
            for i, bc in enumerate(bcs):
                print('Dirichlet condition %d' % i)
                boundary_values = bc.get_boundary_values()
		print ('Number of boundary values in the above Dirichlet condition:', len(bc.get_boundary_values()))
#                for dof in boundary_values:
#                    print('   dof %2d: u = %g' % (dof, boundary_values[dof]))
#                    if V.ufl_element().degree() == 1:
#                        print('    at point %s' %
#                              (str(tuple(coor[d2v[dof]].tolist()))))









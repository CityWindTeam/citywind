# This file is to test meshing from .obj files (created in GAL)

# Create mesh
from dolfin import *
from IPython import embed
import time
from mshr import *
set_log_level(DBG);
start = time.time()
domain = Surface3D("oklahoma_single_building_small.off")
#domain = Surface3D("obj/de_16x16x1000_x529075_y6469405.obj")
#domain = Surface3D("obj/de_3008x3008x100_x529075_y6469405.obj")
#domain = Surface3D("obj/de_3008x3008x1000_x529075_y6469405.obj")
#domain = Surface3D("test_500x500x2000.stl")
#domain = Surface3D("test_500x500x2000.obj")
#domain = Surface3D("test_3000x3000x2000.obj")
#domain = Surface3D("test_5000x5000x2000.obj")
#domain = Surface3D("test_16x16x10.obj")
#domain = Surface3D("test_16x16x10.stl")
mesh = generate_mesh(domain,1)
end = time.time()
print(end - start)
print(mesh.num_vertices())
plot(mesh, interactive=True, title="Initial mesh")

'''
typeelement='CG'
degreeelement_V=1
degreeelement_VFS=1
V = FunctionSpace(mesh, typeelement, degreeelement_V)
VFS = VectorFunctionSpace(mesh, typeelement, degreeelement_VFS, dim=3)

ndim = V.dim()
d = mesh.geometry().dim()
V_dof_coordinates = V.tabulate_dof_coordinates()
V_dof_coordinates.resize((ndim, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]



print(max(V_dof_x))
print(min(V_dof_x))
print(max(V_dof_y))
print(min(V_dof_y))
print(max(V_dof_z))
print(min(V_dof_z))

'''



# This is implementation of adaptive grid refinement based on divergence of final wind field as the measure of where to refine grid. It does not use AdaptiveLinearVariationalSolver. Geometry single building with no wakes. 
from fenics import *
from mshr import *
import numpy as np
from IPython import embed
import time
from fenicstools import interpolate_nonmatching_mesh_any

#### Load/Import mesh
mesh=Mesh()
domain = Surface3D("oklahoma_single_building.off")
mesh = generate_mesh(domain,4)


#### Define function space
print("Define function space")

typeelement='CG'
degreeelement_V=2
degreeelement_VFS=2
V = FunctionSpace(mesh, typeelement, degreeelement_V)
VFS = VectorFunctionSpace(mesh, typeelement, degreeelement_VFS, dim=3)

ndim = V.dim()
d = mesh.geometry().dim()

V_dof_coordinates = V.tabulate_dof_coordinates()
V_dof_coordinates.resize((ndim, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]

#### Setup initial windfield u0v0w0
print("Setup initial windfield u0v0w0")
# This data is valid for Oklahoma City according to Jan B. 
z0=0.6
Ux=1.97 # at z=10m	
Uy=2.82 # at z=10m
vK=0.4 # von Karman constant
Ustar=(np.sqrt(Ux*Ux+Uy*Uy)*0.4)/np.log(10/z0)
Uxstar=(Ux*0.4)/np.log(10/z0)
Uystar=(Uy*0.4)/np.log(10/z0)
vK_f = Constant(vK)
z0_f=Constant(z0) 
w0_exp_f = Constant(0.0)
w0_exp = 0.0
degree = 5
# Redfine constants so FEniCS understands
Ustar_f=Constant(Ustar)
Uxstar_f=Constant(Uxstar)
Uystar_f=Constant(Uystar)
z0_f=Constant(z0) 

u0v0w0_exp = Expression(("x[2] > 0 ? Uxstar/vK*log((x[2]+z0)/z0) : 0","x[2] > 0 ? Uystar/vK*log((x[2]+z0)/z0) : 0", "w0_exp"), Uxstar=Uxstar_f, Uystar=Uystar_f, w0_exp=w0_exp_f, z0=z0_f, vK = vK_f, degree = degree)


##################################################################
start = time.time()
#print("Start assign initial wind field - cpp")

cppcode = """
class K : public Expression
{
public:
K() : Expression(3) {}

void eval(Array<double>& values,
const Array<double>& x,
const ufc::cell& cell) const
{

values [0] = Uxstar/vK*log((x[2]+z0)/z0);
values [1] = Uystar/vK*log((x[2]+z0)/z0);
values [2] = w0_exp;
}
double vK;
double Uxstar;
double Uystar;
double w0_exp;
double z0;

};
"""
b = Expression(cppcode=cppcode, degree=degree)
b.z0 = z0
b.Uxstar = Uxstar
b.Uystar = Uystar
b.w0_exp = w0_exp
b.vK = vK

end = time.time()
print("Stop assign initial wind field - cpp")
print(end - start)

start = time.time()
print("interpolate onto vector function space from cpp structure")
u0v0w0 = interpolate(b, VFS)
end = time.time()
print(end-start)

#### Define SubDomains for boundaries and mark

boundary_markers = FacetFunction('size_t',mesh) #FacetFunction is a subclass of MeshFunction
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.
# method 3 is to add another FacetFunction, to visualize only on the boundary

print("Define boundaries and mark - c++")

start = time.time()

MaxX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_x)).mark(boundary_markers, 1)
MinX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_x)).mark(boundary_markers, 2)
MaxY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_y)).mark(boundary_markers, 3)
MinY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_y)).mark(boundary_markers, 4)
MaxZ = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_z)).mark(boundary_markers, 5)
end = time.time()
print(end-start)

### Implement the boundary conditions	
print("Implement boundary conditions")
alpha = Constant(1.0)
n=FacetNormal(mesh)
neu_bc=2*alpha*alpha*dot(u0v0w0_exp, n)  

# Now is the domain not aligned with the wind direction. Therefore I set Dirichlet=0 (wind is free to vary) on the TWO outflow boundaries. 
# Neumann = 0 is unchanged wind. Dirichlet = 0 is free wind. Neumann = neu_bc is wind is free to slip along the boundary but not go through.

boundary_conditions =  {0: {'Neumann': neu_bc},  # Buildings
			1: {'Dirichlet': 0}, # Spacedomain; MaxX - outflow for Ux,Uy>0
			2: {'Neumann': 0},	# Spacedomains: MinX - inflow for Ux,Uy>0 
			3: {'Dirichlet': 0},   # Spacedomains: MaxY - outflow for Ux,Uy>0
			4: {'Neumann': 0},	# Spacedomain: MinY - inflow for Ux,Uy>0
			5: {'Neumann': 0}}	# Spacedomain: MaxZ (top)

# Define variational problem
LagrangeMultiplier = TrialFunction(V)
testv = TestFunction(V)

#Redefine the measure "ds" in terms of our boundary markers
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)
#dx = Measure('dx', domain=mesh, subdomain_data=wake_markers) #is this needed?? Should not be needed unless I want to integrate over different parts of the domain

# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are none Robin conditions. 
integrals_N = []
for i in boundary_conditions:
	if 'Neumann' in boundary_conditions[i]:
		if boundary_conditions[i]['Neumann'] != 0:
			g = boundary_conditions[i]['Neumann']
			integrals_N.append(g*testv*ds(i))
# Collect Dirichlet boundary conditions
bcs = []
for i in boundary_conditions:
	if 'Dirichlet' in boundary_conditions[i]:
		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
		bcs.append(bc)

# Continue defining variational problem
a = dot(grad(LagrangeMultiplier), grad(testv))*dx
L = div(u0v0w0)*testv*dx-sum(integrals_N)
# Compute solution
LagrangeMultiplier = Function(V)
start=time.time()
solve(a == L, LagrangeMultiplier, bcs)
end=time.time()
print(end-start)

#### Find the winddata
print("Find the winddata")
print("method 4: projection by solving inner(TrialFunction(VFS), TestFunction(VFS))*dx = inner(TestFunction(VFS), grad(LagrangeMultiplier))*dx")
# https://fenicsproject.org/qa/1425/derivatives-at-the-quadrature-points?show=1425#q1425

start = time.time()
uvfs = TrialFunction(VFS)
vvfs = TestFunction(VFS)

# The mass matrix is diagonal. Get the diagonal 
lhs = assemble(inner(uvfs, vvfs)*dx)
ones = Function(VFS)
ones.vector()[:] = 1.
lhs_d = lhs*ones.vector()
# If you only want to assemble right hand side once:
rhs = assemble(inner(vvfs, (u0v0w0+(1/(2*alpha**2))*grad(LagrangeMultiplier)))*dx)

# solve
uvw_4 = Function(VFS)
uvw_4.vector().set_local(rhs.array() / lhs_d.array())

end=time.time()
print('Time to solve inner(TrialFunction(VFS), TestFunction(VFS))*dx = inner(TestFunction(VFS), u0v0w0+(1/(2*alpha**2))*grad(LagrangeMultiplier)))*dx')
print(end-start)


### Refine mesh based on divergence of cells
phi0=div(uvw_4)
#embed()
cell_markers = CellFunction("bool", mesh)
cell_markers.set_all(False)
for cell in cells(mesh):
	p = cell.midpoint()
	embed()
    	if phi0(p) > 0.4:
        	cell_markers[cell] = True

mesh = refine(mesh, cell_markers)

embed()
exit()


### Refine mesh based on number of vertices in mesh


'''
## Boundary mesh
bmesh = BoundaryMesh(mesh, "exterior")
VFSb = VectorFunctionSpace(bmesh, typeelement, degreeelement_VFS, dim=3)
uvw_4b = Function(VFSb)
uvw_4b = interpolate_nonmatching_mesh_any(uvw_4, VFSb) #https://github.com/mikaem/fenicstools/wiki/Interpolation-nonmatching-mesh-in-parallel
'''

## Visualize
#plot(boundary_markers, interactive=True, title="Boundary Markers")
# Plot solution
#LagrangeMultiplier.rename('LagrangeMultiplier', 'LagrangeMultiplier_solution')
#plot(LagrangeMultiplier, wireframe=True, interactive=True)

plot(div(u0v0w0), wireframe=True, interactive=True, title="Divergence of the initial wind field")
exit()
#plot(div(uvw_2), wireframe=True, interactive=True, title="Divergence of the final wind field - method 2 ")
#plot(div(uvw_4), wireframe=True, interactive=True, title="Divergence of the final wind field - method 4")

#plot(u0v0w0, wireframe=True, interactive=True, title="Initial windfield")
#plot(uvw_2, wireframe=True, interactive=True, title="Final windfield_2")
#plot(uvw_4, wireframe=True, interactive=True, title="Final windfield_4")
#plot(uvw_4b, wireframe=True, interactive=True, title="Final windfield_4 on the boundary")
#plot(mesh, interactive=True)
#plot(bmesh, interactive = True)

## Write to file
#File("mesh.pvd") << mesh
#File("u0v0w0.pvd") << u0v0w0
#File("boundary_markers.pvd") << boundary_markers
File("uvw_4.pvd") << uvw_4
File('uvw_boundary_' + typeelement + str(degreeelement_V) +str(degreeelement_VFS) + '.pvd') << uvw_4b
exit()


from dolfin import *
from fenicstools import gauss_divergence

# Compute divergence of vector field
u = Expression(('sin(pi*x[0]*x[1])', 'cos(pi*x[0]*x[1])'), degree=5)
divu_exact = Expression('pi*(cos(pi*x[0]*x[1])*x[1] - sin(pi*x[0]*x[1])*x[0])', degree=5)

mesh = UnitSquareMesh(100, 100)
V = VectorFunctionSpace(mesh, 'CG', 1)

# Use expression
divu = gauss_divergence(u, mesh)
plot(divu, title='Div(u) from Expression')

# Use function
u = interpolate(u, V)
divu = gauss_divergence(u)
plot(divu, title='Div(u) from CG1 function')

# Compare with the exact divergence
S = FunctionSpace(mesh, 'DG', 0)
divu_exact = interpolate(divu_exact, S)
plot(divu_exact, title='Exact div(u) in DG0')

# FEniCS div CL

plot(div(u), title='div(u) in FEniCS')

interactive()

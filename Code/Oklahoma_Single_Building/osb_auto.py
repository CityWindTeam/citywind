# This solves the Rockle equation with adaptive mesh refinement with AdaptivLinearVariationalSolver. refinement criteria is based on some errornorm upon solving the dual problem for the given specified goal functional. Typically taken as integral of the solutionvector*dx(). Geometry is based on .off or .obj (if .obj file reader is present in mshr).

from dolfin import *
from IPython import embed
from mshr import *
import numpy as np
from fenicstools import interpolate_nonmatching_mesh_any

parameters["refinement_algorithm"] = "plaza_with_parent_facets"
#### Load/Import mesh
domain = Surface3D("oklahoma_single_building.off")
mesh = generate_mesh(domain,4)
# Create functionspace
typeelement='CG'
degreeelement_V=1
degreeelement_VFS=1
V = FunctionSpace(mesh, typeelement, degreeelement_V)
VFS = VectorFunctionSpace(mesh, typeelement, degreeelement_VFS, dim=3)

ndim = V.dim()
d = mesh.geometry().dim()
V_dof_coordinates = V.tabulate_dof_coordinates()
V_dof_coordinates.resize((ndim, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]

#### Setup initial windfield u0v0w0
print("Setup initial windfield u0v0w0")
# This data is valid for Oklahoma City according to Jan B. 
z0=0.6
Ux=1.97 # at z=10m	
Uy=2.82 # at z=10m
vK=0.4 # von Karman constant

Ustar=(np.sqrt(Ux*Ux+Uy*Uy)*vK)/np.log(10/z0)
Uxstar=(Ux*vK)/np.log(10/z0)
Uystar=(Uy*vK)/np.log(10/z0)

w0_exp_f = Constant(0.0)
w0_exp = 0.0
degree = 5
# Redfine constants so FEniCS understands
Ustar_f=Constant(Ustar)
Uxstar_f=Constant(Uxstar)
Uystar_f=Constant(Uystar)
vK_f = Constant(vK)
z0_f=Constant(z0) 

u0v0w0_exp = Expression(("x[2] > 0 ? Uxstar/vK*log((x[2]+z0)/z0) : 0","x[2] > 0 ? Uystar/vK*log((x[2]+z0)/z0) : 0", "w0_exp"), Uxstar=Uxstar_f, Uystar=Uystar_f, w0_exp=w0_exp_f, z0=z0_f, vK = vK_f, degree = degree)

cppcode = """
class K : public Expression
{
public:
K() : Expression(3) {}

void eval(Array<double>& values,
const Array<double>& x,
const ufc::cell& cell) const
{
values [0] = Uxstar/vK*log((x[2]+z0)/z0);
values [1] = Uystar/vK*log((x[2]+z0)/z0);
values [2] = w0_exp;
}

double vK;
double Uxstar;
double Uystar;
double w0_exp;
double z0;

};
"""
b = Expression(cppcode=cppcode, degree=degree)
b.z0 = z0
b.Uxstar = Uxstar
b.Uystar = Uystar
b.w0_exp = w0_exp
b.vK = vK

u0v0w0 = interpolate(b, VFS)

#### Define SubDomains for boundaries and mark

boundary_markers = FacetFunction('size_t',mesh) #FacetFunction is a subclass of MeshFunction
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.
MaxX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_x)).mark(boundary_markers, 1)
MinX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_x)).mark(boundary_markers, 2)
MaxY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_y)).mark(boundary_markers, 3)
MinY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_y)).mark(boundary_markers, 4)
MaxZ = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_z)).mark(boundary_markers, 5)

alpha = Constant(1.0)
n=FacetNormal(mesh)
neu_bc=2*alpha*alpha*dot(u0v0w0, n)
 
boundary_conditions =  {0: {'Neumann': neu_bc},  # MinZ. / Building&Ground
			1: {'Dirichlet': 0}, # MaxX
			2: {'Neumann': 0},	# MinX
			3: {'Dirichlet': 0},   # MaxY
			4: {'Neumann': 0},	# MinY
			5: {'Neumann': 0}}	# MaxZ

#Redefine the measure "ds" in terms of our boundary markers
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)

# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are nonevK Robin conditions. 
testv = TestFunction(V)
integrals_N = []
for i in boundary_conditions:
	if 'Neumann' in boundary_conditions[i]:
		if boundary_conditions[i]['Neumann'] != 0:
			g = boundary_conditions[i]['Neumann']
			integrals_N.append(g*testv*ds(i))

# Collect Dirichlet boundary conditions
bcs = []
for i in boundary_conditions:
	if 'Dirichlet' in boundary_conditions[i]:
		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
		bcs.append(bc)


# Define variational problem
LagrangeMultiplier = TrialFunction(V)
a = dot(grad(LagrangeMultiplier), grad(testv))*dx
L = div(u0v0w0)*testv*dx-sum(integrals_N)

# Define function for the solution
LagrangeMultiplier = Function(V)

# Define goal functional (quantity of interest)
M = LagrangeMultiplier*dx()

# Define error tolerance
#tol = 2.e5
tol = 5.e6

# Solve equation a = L with respect to u and the given boundary
# conditions, such that the estimated error (measured in M) is less
# than tol
problem = LinearVariationalProblem(a, L, LagrangeMultiplier, bcs)
solver = AdaptiveLinearVariationalSolver(problem, M)
solver.parameters["error_control"]["dual_variational_solver"]["linear_solver"] = "cg"
solver.parameters["error_control"]["dual_variational_solver"]["symmetric"] = True
solver.solve(tol)

solver.summary()

# Plot solution(s)
#plot(LagrangeMultiplier.root_node(), title="Solution on initial mesh")
#plot(LagrangeMultiplier.leaf_node(), title="Solution on final mesh")
#plot(LagrangeMultiplier.child(), title="Solution on final mesh child")
#interactive()

#plot(mesh.leaf_node(), interactive=True, title="Final mesh")
#plot(mesh.root_node(), interactive=True, title="Initial mesh")

#u0v0w0.child()

#### Find the winddata on the refined mesh

print("Find the winddata")
print("projection by solving inner(TrialFunction(VFS), TestFunction(VFS))*dx = inner(TestFunction(VFS), grad(LagrangeMultiplier))*dx")
# https://fenicsproject.org/qa/1425/derivatives-at-the-quadrature-points?show=1425#q1425

uvfs = TrialFunction(VFS.child())
vvfs = TestFunction(VFS.child())

# The mass matrix is diagonal. Get the diagonal 
lhs = assemble(inner(uvfs, vvfs)*dx)
ones = Function(VFS.child())
ones.vector()[:] = 1.
lhs_d = lhs*ones.vector()
# If you only want to assemble right hand side once:
rhs = assemble(inner(vvfs, (u0v0w0.child()+(1/(2*alpha**2))*grad(LagrangeMultiplier.child())))*dx)

# solve to find projection
uvw = Function(VFS.child())
uvw.vector().set_local(rhs.array() / lhs_d.array())

## Boundary mesh on alma mater mesh
bmesh = BoundaryMesh(mesh, "exterior")
VFSb = VectorFunctionSpace(bmesh, typeelement, degreeelement_VFS, dim=3)
uvw_b = Function(VFSb)
uvw_b = interpolate_nonmatching_mesh_any(uvw, VFSb) #https://github.com/mikaem/fenicstools/wiki/Interpolation-nonmatching-mesh-in-parallel


## Obtain final windfield on facet midpoints to verify the boundary conditions: no flow across ground/building boundaries. 
#https://fenicsproject.org/qa/2822/how-to-calculate-facet-average
CR = VectorFunctionSpace(mesh.child(), "CR", 1)
facet_avg_int = interpolate(uvw, CR)
facet_avg_proj = project(uvw, CR)

File("facet_avg_int.pvd") << facet_avg_int
File("facet_avg_proj.pvd") << facet_avg_proj


#facet average on the boundary
bmesh_child = BoundaryMesh(mesh.child(), "exterior")
CRb = VectorFunctionSpace(mesh.child(), "CR", 1)
uvw_b = Function(CRb)
uvw_b = interpolate_nonmatching_mesh_any(uvw, CRb)



## Visualize
#plot(uvw_b, wireframe=True, interactive=True, title="Boundary Final windfield - boundarymesh ")
#plot(boundary_markers, interactive=True, title="Boundary Markers")
#LagrangeMultiplier.rename('LagrangeMultiplier', 'LagrangeMultiplier_solution')
#plot(LagrangeMultiplier, wireframe=True, interactive=True)

#plot(div(u0v0w0), wireframe=True, interactive=True, title="Divergence of the initial wind field")
#plot(div(uvw), wireframe=True, interactive=True, title="Divergence of the final wind field")

#plot(u0v0w0, wireframe=True, interactive=True, title="Initial windfield")
#plot(uvw, wireframe=True, interactive=True, title="Final windfield")
plot(uvw_b, wireframe=True, interactive=True, title="Final windfield on the boundary")
#plot(mesh, interactive=True)
#plot(bmesh, interactive = True)

## Write to file
#File("mesh.pvd") << mesh
#File("u0v0w0.pvd") << u0v0w0
#File("boundary_markers.pvd") << boundary_markers
File("uvw.pvd") << uvw
#File("uvw.vtk") << uvw
File("uvw_b.pvd") << uvw_b


exit()



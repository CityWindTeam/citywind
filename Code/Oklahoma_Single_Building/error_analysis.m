%Error plotting in matlab

h = readTextFile('txt_h.txt');
error = readTextFile('txt_error.txt');
if length(h) > length(error)
    disp('Hej')
    h = h(1:end-1);
end
loglog(h,error, '*')
title('l_2 error')
ylabel('l_2 error')
xlabel('h')
hold on
fit_err=polyfit(log(h),log(error),1);
L21 = fit_err(1);
loglog(h,exp(fit_err(1)*log(h) + fit_err(2)),'r','linewidth',2);
hold off
Convergence = (log(error(2:end))-log(error(1:end-1)))./(log(h(2:end))-log(h(1:end-1)))
legend('Error in L_2 - norm',['Reference with slope ', num2str(Convergence(2))])
ylabel('L_2 - error','fontsize',14)
xlabel('Element size','fontsize',14)
title('Convergence in L_2 - norm','fontsize',14)
xlim([10^-2, 2*10^-1])

# This script sets up initial field in python and defines boundaries based on queries of the mesh such as min/max.
# No wakes
from fenics import *
from mshr import *
#from scipy import *
import numpy as np
from IPython import embed


#### Load/Import mesh
print("Create the mesh")
mesh=Mesh()
domain = Surface3D("oklahoma_single_building.off")
mesh = generate_mesh(domain,4)
#mesh=UnitCubeMesh(50,50,5)
#plot(mesh, interactive=True)
# File("mesh.pvd") << mesh

#### Define function space
print("Define function space")
V = FunctionSpace(mesh, 'CG', 1) # so that dofs are only in mesh vertices
VFS = VectorFunctionSpace(mesh, 'CG', 1, dim=3)
ndim = V.dim()
d = mesh.geometry().dim()

V_dof_coordinates = V.tabulate_dof_coordinates()
V_dof_coordinates.resize((ndim, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]

#### Setup initial windfield u0v0w0
print("Setup initial windfield u0v0w0")
# This data is valid for Oklahoma City according to Jan B. 
z0=0.6
Ux=1.97 # at z=10m	
Uy=2.82 # at z=10m

Ustar=(np.sqrt(Ux*Ux+Uy*Uy)*0.4)/np.log(10/z0)
Uxstar=(Ux*0.4)/np.log(10/z0)
Uystar=(Uy*0.4)/np.log(10/z0)

#u0v0w0_abs_exp = Expression("x[2] > 0 ? Ustar/0.4*log(x[2]/z0) : 0", Ustar=Ustar, z0=z0, degree = degree) 
#u0_exp = Expression("x[2] > 0 ? Uxstar/0.4*log(x[2]/z0) : 0", Uxstar=Uxstar, z0=z0, degree = degree) 
#v0_exp = Expression("x[2] > 0 ? Uystar/0.4*log(x[2]/z0) : 0", Uystar=Uystar, z0=z0, degree = degree) 
w0_exp = Constant(0.0)

degree = 5
# Redfine constants so FEniCS understands
Ustar=Constant(Ustar)
Uxstar=Constant(Uxstar)
Uystar=Constant(Uystar)
z0=Constant(z0) 

u0v0w0_exp = Expression(("x[2] > 0 ? Uxstar/0.4*log(x[2]/z0) : 0","x[2] > 0 ? Uystar/0.4*log(x[2]/z0) : 0", "w0_exp"), Uxstar=Uxstar, Uystar=Uystar, w0_exp=w0_exp, z0=z0, degree = degree)

class Initfield(Expression):
	print("Assigning initial wind field")
	def __init__(self, u0v0w0_exp, **kwargs):
		self.u0v0w0_exp = u0v0w0_exp
	def eval_cell(self, values, x, cell):
        	values[:] =  self.u0v0w0_exp(x)
	def value_shape(self): return (3, ) 
cpp
print("Start assign initial wind field")
b = Initfield(u0v0w0_exp, degree=degree)
u0v0w0 = project(b, VFS)

	
#### Define SubDomains for boundaries and mark
print("Define boundaries and mark")

boundary_markers = FacetFunction('size_t',mesh) #FacetFunction is a subclass of MeshFunction
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.

class MaxX(SubDomain): 

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], max(V_dof_x), tol)

bx1 = MaxX()
bx1.mark(boundary_markers, 1)	



class MinX(SubDomain): 

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], min(V_dof_x), tol)

bx2 = MinX()
bx2.mark(boundary_markers, 2)	

class MaxY(SubDomain):

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[1], max(V_dof_y), tol)

bx3 = MaxY()
bx3.mark(boundary_markers, 3)	



class MinY(SubDomain): 

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[1], min(V_dof_y), tol)

bx4 = MinY()
bx4.mark(boundary_markers, 4)

class MaxZ(SubDomain): 

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[2], max(V_dof_z), tol)

bx5 = MinY()
bx5.mark(boundary_markers, 5)


#plot(boundary_markers, interactive=True, title="Boundary Markers")
plot(u0v0w0, mesh=mesh, interactive=True, title="Initial wind field without wakes")
#plot(div(u0v0w0),interactive=True, title="div(u0v0w0)")

#File("u0v0w0.pvd") << u0v0w0
#File("boundary_markers.pvd") << boundary_markers


### Implement the boundary conditions	
print("Implement boundary conditions")

alpha = Constant(1.0)
n=FacetNormal(mesh)
neu_bc=2*alpha*alpha*dot(u0v0w0_exp, n)

# Now is the domain not aligned with the wind direction. Therefore I set Dirichlet=0 (wind is free to vary) on the TWO outflow boundaries. 
# Neumann = 0 is unchanged wind. Dirichlet = 0 is free wind. Neumann = neu_bc is wind is free to slip along the boundary but not go through.

boundary_conditions =  {0: {'Neumann': neu_bc},  # Buildings
			1: {'Dirichlet': 0}, # Spacedomain; MaxX - outflow for Ux,Uy>0
			2: {'Neumann': 0},	# Spacedomains: MinX - inflow for Ux,Uy>0 
			3: {'Dirichlet': 0},   # Spacedomains: MaxY - outflow for Ux,Uy>0
			4: {'Neumann': 0},	# Spacedomain: MinY - inflow for Ux,Uy>0
			5: {'Neumann': 0}}	# Spacedomain: MaxZ (top)

# Define variational problem
LagrangeMultiplier = TrialFunction(V)
testv = TestFunction(V)

#Redefine the measure "ds" in terms of our boundary markers
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)
#dx = Measure('dx', domain=mesh, subdomain_data=wake_markers) #is this needed?? Should not be needed unless I want to integrate over different parts of the domain

# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are none Robin conditions. 
integrals_N = []
for i in boundary_conditions:
	if 'Neumann' in boundary_conditions[i]:
		if boundary_conditions[i]['Neumann'] != 0:
			g = boundary_conditions[i]['Neumann']
			integrals_N.append(g*testv*ds(i))

# Collect Dirichlet boundary conditions
bcs = []
for i in boundary_conditions:
	if 'Dirichlet' in boundary_conditions[i]:
		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
		bcs.append(bc)

# Continue defining variational problem
a = dot(grad(LagrangeMultiplier), grad(testv))*dx
L = div(u0v0w0)*testv*dx-sum(integrals_N)

# Compute solution
print("Compute solution")
LagrangeMultiplier = Function(V)
solve(a == L, LagrangeMultiplier, bcs)

# Plot solution
LagrangeMultiplier.rename('LagrangeMultiplier', 'LagrangeMultiplier_solution')
#plot(LagrangeMultiplier, wireframe=True, interactive=True)

#### Find the winddata

uvw = Function(VFS)

# Correction from initial field to final field

LMdx = Function(V)
LMdx = project(LagrangeMultiplier.dx(0), V)
LMdy = Function(V)
LMdy = project(LagrangeMultiplier.dx(1), V)
LMdz = Function(V)
LMdz = project(LagrangeMultiplier.dx(2), V)
LM_derivatives = Function(VFS)
assign(LM_derivatives.sub(0), LMdx)
assign(LM_derivatives.sub(1), LMdx)
assign(LM_derivatives.sub(2), LMdx)

# The final field
#uvw = u0v0w0+(1/(2*alpha**2))*LM_derivatives
uvw = project(u0v0w0+(1/(2*alpha**2))*LM_derivatives, VFS)

#plot(u0v0w0, wireframe=True, interactive=True, title="Initial windfield")
#plot((1/(2*alpha**2))*LM_derivatives, wireframe=True, interactive=True, title="Correction windfield")
plot(uvw, wireframe=True, interactive=True, title="Final windfield")
#plot(uvw_2, wireframe=True, interactive=True, title="Final windfield_2")
#Correction = project(1/((2*alpha**2)*LM_derivatives), VFS)
#Correction = (1/(2*alpha**2))*LM_derivatives
#File("Correction.pvd") << Correction
#File("uvw.pvd") << uvw
exit()

# This script should read mesh created from CGAL and .obj file.

from fenics import *
from mshr import *
#from scipy import *
import numpy as np
from IPython import embed
import time

set_log_level(DBG)

#### Load/Import mesh
mesh=Mesh()
#domain = Surface3D("oklahoma_single_building.obj")
domain = Surface3D("test.obj")
mesh = generate_mesh(domain,4)
plot(mesh, interactive=True)

'''
mesh2=Mesh()
domain2 = Surface3D("oklahoma_single_building.off")
mesh2 = generate_mesh(domain,4)
plot(mesh2, interactive=True)
'''
exit()

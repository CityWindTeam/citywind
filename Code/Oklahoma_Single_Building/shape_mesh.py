# Create mesh from shapefile using Python and FEniCS.

from fenics import *
from dolfin import *
from mshr import *
from scipy import *
import numpy as np
import shapefile
from IPython import embed

## Read shapedata
print("Read shapedata")
sf = shapefile.Reader("OKC_SINGLE_BUILDING")
shapeRecs = sf.shapeRecords()

## Create mesh
print("Create the mesh")
#Iterate over all buildings and edges 
shpnum = sf.iterShapeRecordsdx = Measure('dx', domain=mesh, subdomain_data=domain_markers)() #shpnum is all buidlings
for j in range(0,len(shapeRecs)):  # j goes through all buildings
	print("j=", j)	
	for i in range(0,len(shapeRecs[j].shape.points[:])-1):  # i goes through all edges of the j:th building
		print("i=", i)

#Generate mesh
mesh=Mesh()
domain = Outer-Buildings
mesh = generate_mesh(domain,32)
#plot(mesh, interactive=True)

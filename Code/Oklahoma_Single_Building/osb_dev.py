from fenics import *
from dolfin import *
from mshr import *
from scipy import *
from scipy.interpolate import RegularGridInterpolator
import numpy as np


# Development on osb.py. -> Find boundaries from mesh. Save FEniCS initial field and RHS=div(initial field). Save bondary markers xml format. # Also testing implicit and anisotropic boundary condition.

#### Read initial velocity fields and griddata

u0_car = loadtxt("u0.txt", comments="#", delimiter=",", unpack=False)
v0_car = loadtxt("v0.txt", comments="#", delimiter=",", unpack=False)
w0_car = loadtxt("w0.txt", comments="#", delimiter=",", unpack=False)
u0v0w0_RGI = loadtxt("u0v0w0_RGI.txt")
GRID_ORIGOZ = 0
GRID_ORIGOX = loadtxt("GRID%ORIGOX.txt", comments="#", delimiter=",", unpack=False)
GRID_ORIGOY = loadtxt("GRID%ORIGOY.txt", comments="#", delimiter=",", unpack=False)
GRID_XMIN = loadtxt("GRID%XMIN.txt", comments="#", delimiter=",", unpack=False)
GRID_XMAX = loadtxt("GRID%XMAX.txt", comments="#", delimiter=",", unpack=False)
GRID_YMIN = loadtxt("GRID%YMIN.txt", comments="#", delimiter=",", unpack=False)
GRID_YMAX = loadtxt("GRID%YMAX.txt", comments="#", delimiter=",", unpack=False)
GRID_ZMIN = loadtxt("GRID%ZMIN.txt", dtype='float', comments="#", delimiter=",", unpack=False)
GRID_ZMAX = loadtxt("GRID%ZMAX.txt", dtype='float', comments="#", delimiter=",", unpack=False)
GRID_DX = loadtxt("GRID%DX.txt", comments="#", delimiter=",", unpack=False)
GRID_DY = loadtxt("GRID%DY.txt", comments="#", delimiter=",", unpack=False)
GRID_DZ = loadtxt("GRID%DZ.txt", comments="#", delimiter=",", unpack=False)
GRID_THETA = loadtxt("GRID%THETA.txt", comments="#", delimiter=",", unpack=False)
GRID_NX = loadtxt("GRID%NX.txt",dtype='int', comments="#", delimiter=",", unpack=False)
GRID_NY = loadtxt("GRID%NY.txt",dtype='int', comments="#", delimiter=",", unpack=False)
GRID_NZ = loadtxt("GRID%NZ.txt",dtype='int', comments="#", delimiter=",", unpack=False)

Vertices_shp = loadtxt("Vertices_shp.txt", dtype='float', delimiter="\t", comments="#", unpack=False)


#### Topography and mesh
domain = Surface3D("oklahoma_single_building.off")
mesh = generate_mesh(domain,32)
#plot(mesh, interactive=True)

#### Define function space
V = FunctionSpace(mesh, 'CG', 1) # so that dofs are only in mesh vertices
VFS = VectorFunctionSpace(mesh, 'CG', 1, dim=3)
ndim = V.dim()
d = mesh.geometry().dim()

#### Find the RHS of the poisson equation (= div (initial velocity field on the FEniCS grid)). 
### Interpolate the initial wind field to the fenics mesh

## FEniCS mesh: coordinates of vertices in numpy format 
CoorFE2 = np.zeros(mesh.num_vertices()) # This is the wrong ordering (plot of initial windfield is corrupt)print
CoorFE2 = mesh.coordinates()
CoorFE_d2v = CoorFE2[dof_to_vertex_map(V)]

np.savetxt("CoorFE_d2v.csv", CoorFE_d2v, delimiter=",")
CoorFE = V.tabulate_dof_coordinates() # This is the correct ordering
np.savetxt("tab_dof_coord.csv", CoorFE, delimiter=",")

V_dof_coordinates = V.tabulate_dof_coordinates() #This is to find the boundaries of the mesh
V_dof_coordinates.resize((ndim, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]


## Cartesian mesh: 
# Coordinates (for the RegularGridInterpolator)!

x_c = linspace(GRID_ORIGOX+GRID_XMIN+0.5*GRID_DX,GRID_ORIGOX+GRID_XMAX-0.5*GRID_DX,GRID_NX)
y_c = linspace(GRID_ORIGOY+GRID_YMIN+0.5*GRID_DY,GRID_ORIGOY+GRID_YMAX-0.5*GRID_DY,GRID_NY)
z_c = linspace(GRID_ZMIN+0.5*GRID_DZ,GRID_ZMAX-0.5*GRID_DZ,GRID_NZ)
# Plot these "simulated" Cartesian coordinates to see if the coincide with NUD-Fortran environment. Print to .vtk


# Initialfield of the Cartesian mesh (for the RegularGridInterpolator)
#The below is applicable (with some error...) when u0 v0 w0 is read 
u0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
v0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
w0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
u0v0w0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ,3))

ir=0
for iz in range(0,GRID_NZ):
	for iy in range(0,GRID_NY):
		for ix in range(0,GRID_NX):
			u0v0w0_RGI[ix][iy][iz][0]=u0_car[ir]
			u0v0w0_RGI[ix][iy][iz][1]=v0_car[ir]
			u0v0w0_RGI[ix][iy][iz][2]=w0_car[ir]

			u0_RGI[ix][iy][iz]=u0_car[ir]
			v0_RGI[ix][iy][iz]=v0_car[ir]
			w0_RGI[ix][iy][iz]=w0_car[ir]
			ir+=1


N=GRID_NX*GRID_NY*GRID_NZ

## Interpolation to retrieve initial field defined in the FEniCS mesh.

# Define functions to interpolate each space-dimension of the initialfield
fn_u0 = RegularGridInterpolator((x_c,y_c,z_c), u0_RGI, bounds_error=False, fill_value=None) 
fn_v0 = RegularGridInterpolator((x_c,y_c,z_c), v0_RGI, bounds_error=False, fill_value=None) 
fn_w0 = RegularGridInterpolator((x_c,y_c,z_c), w0_RGI, bounds_error=False, fill_value=None)
 
fn_u0v0w0 = RegularGridInterpolator((x_c,y_c,z_c), u0v0w0_RGI, bounds_error=False, fill_value=None)
 

#Interpolate!
u0_FE=fn_u0(CoorFE)
v0_FE=fn_v0(CoorFE)
w0_FE=fn_w0(CoorFE)
u0v0w0_FE=fn_u0v0w0(CoorFE)

#Assemble to verify with Paraview that the initial wind field is correct.
u0v0w0_FE2=np.zeros((ndim,3)) ##This yields the same resulting vectorfield
u0v0w0_FE2[:,0]=u0_FE
u0v0w0_FE2[:,1]=v0_FE
u0v0w0_FE2[:,2]=w0_FE

## Transfer to FEniCS "function"
#https://fenicsproject.org/qa/2715/coordinates-u_nodal_values-using-numerical-source-function
u0 = Function(V)
v0 = Function(V)
w0 = Function(V)
u0v0w0 = Function(VFS)
u0v0w02 = Function(VFS)
u0.vector()[:] = u0_FE # Also possible to import divergence of the initial field from (i.e. the RHS) from NUD Fortran interpolate with Python and apply to the mesh as a scalar field.
v0.vector()[:] = v0_FE
w0.vector()[:] = w0_FE

#uvw.vector()[:] = u0v0w0_FE
#uvw2.vector()[:] = u0v0w0_FE2
assign(u0v0w0.sub(0), u0)
assign(u0v0w0.sub(1), v0)
assign(u0v0w0.sub(2), w0)


vtkfile = File('osb_u0_FEniCS.pvd')
vtkfile << u0
vtkfile = File('osb_v0_FEniCS.pvd')
vtkfile << v0
vtkfile = File('osb_w0_FEniCS.pvd')
vtkfile << w0
vtkfile = File('osb_u0v0w0_FEniCS.pvd')
vtkfile << u0v0w0
File("osb_u0v0w0.xml") << u0v0w0

##Calculate divergence to obtain the R.H.S.

#Make sure everything is in correct dimension and so forth..
#RHS = grad(uvw)[0] + grad(uvw)[1] + grad(uvw)[2]

RHS_VFS = div(u0v0w0)
RHS = project(RHS_VFS, V)
File("osb_RHS.pvd") << RHS
File("osb_RHS.xml") << RHS
#plot(RHS, title='Source term',  interactive=True)

#### Define boundary conditions chapter 4.4.4. in the Tutorial and conditions is set out by manual drawing: These boundary conditions are such that the volume aligns with the flow direction, the inflow is thus in line with to x[0]. The inflow is constant (u0) over the leading face (Neumann, dlambda/dn=0) whereas the outflow may vary freely (Dirichlet, lambda=0). No flow is allowed to exit in other directions, x[1] and x[2], hence Neumann, dlambda/dn=0 (and v0=w0=0 in the initial field.) //CL

# First mark the different boundaries using MeshFunction "facetfunction" and SubDomain.
 
boundary_markers = FacetFunction('size_t',mesh)
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.

class OutflowBND(SubDomain): # spacedomain x[0] positive; outflow boundary

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], max(V_dof_x), tol)

bx1 = OutflowBND()
bx1.mark(boundary_markers, 1)	



class InflowBND(SubDomain): # spacedomain x[0] negative; inflow boundary

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], min(V_dof_x), tol)

bx2 = InflowBND()
bx2.mark(boundary_markers, 2)	

class SidesBND(SubDomain): # spacedomain x[1] negative & positive

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and (near(x[1], min(V_dof_y), tol) or near(x[1], max(V_dof_y), tol) or near(x[2], min(V_dof_z), tol) or near(x[2], max(V_dof_z), tol))

bx3 = SidesBND()
bx3.mark(boundary_markers, 3)	

# print boundary markers and subdomains to file. To speed up oc.
File("osb_boundary_markers.xml") << boundary_markers
File("osb_subdomains.xml") 

### Implement the boundary conditions	
from IPython import embed; embed()
alpha = Constant(1.0)

n=FacetNormal(mesh)
#neu_bc=2*alpha*alpha*dot(u0v0w0, n) # There is actually a double minus sign here. one for the facetnormal, found out with the manufactured solution. And one in the boundary condition itself.
neu_vec = as_vector([u0, v0, w0]) 
neu_bc=2*alpha*alpha*dot(neu_vec, n) # There is actually a double minus sign here. one for the facetnormal, found out with the manufactured solution. And one in the boundary condition itself.


boundary_conditions =  {0: {'Neumann': neu_bc},  # buildings  ///  if class 3 not defined then also spacedomains aligned with wind (u0 or x[0])
			1: {'Dirichlet': 0}, # spacedomain; outflow boundary
			2: {'Neumann': 0},	# spacedomains: inflow 
			3: {'Neumann': 0}}   # Spacedomains sides and top, and ground.


#from IPython import embed; embed()
# Visualizee subdomains, to see that they are correct
#plot(boundary_markers, interactive=True)
vtkfile = File('osb_boundary_markers.pvd')
vtkfile << boundary_markers 

# Define variational problem
LagrangeMultiplier = TrialFunction(V)
testv = TestFunction(V)

#Redefine the measure "ds" in terms of our boundary markers
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)

# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are none Robin conditions. 
integrals_N = []
for i in boundary_conditions:
	if 'Neumann' in boundary_conditions[i]:
		if boundary_conditions[i]['Neumann'] != 0:
			g = boundary_conditions[i]['Neumann']
			integrals_N.append(g*testv*ds(i))

# Collect Dirichlet boundary conditions
bcs = []
for i in boundary_conditions:
	if 'Dirichlet' in boundary_conditions[i]:
		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
		bcs.append(bc)
		

# Continue defining variational problem
# Here testing implicit inhomogeneous Neumann bc 
#alpha = Constant(1.0)
#beta = Constant(1.0)
#gamma = Constant(1.0)
#noll = Constant(0.0)
#C=Constant(1000) #sufficiently large to force the added inhomogeneous Neumann boundary integral to zero in the weak formulation
#I = Identity(3)
#k=(("1"),"0","0"),("0","1","0"),("0","0","1"))
#k=((alpha,noll,noll),(noll,beta,noll),(noll,noll,gamma))
#print("Type I")
#print type(I)
#print("Type k")
#print type(k)

##
#kgradLM=k*grad(LagrangeMultiplier)
#print type(kgradLM)o
#IgradLM=I*grad(LagrangeMultiplier)
#print("Type IgradLM")
#print type(IgradLM)

#dot(kgradLM,n)

## 
#print type(I)
#k=Matrix()
#print type(k)
#k = [[alpha, 0, 0], [0, beta, 0], [0, 0, gamma]]   # 3x3 matrix with coefficients alpha, beta, gamma
#print type(k)

#nx = Expression(('1.0','0.0','0.0'), degree=2)
#ny = Expression(('0.0','1.0','0.0'), degree=2)
#nz = Expression(('0.0','0.0','1.0'), degree=2)

# b,c,d equals "k*grad(LagrangeMultiplier))"
#b=alpha*(dot(grad(LagrangeMultiplier),nx))
#c=beta*(dot(grad(LagrangeMultiplier),ny))
#d=gamma*(dot(grad(LagrangeMultiplier),nz))

# e,f,g are components of grad(testv)
#e=dot(grad(testv),nx)
#f=dot(grad(testv),ny)
#g=dot(grad(testv),nz)

#h equals "dot(k*grad(LagrangeMultiplier)),grad(testv))"   which appears as volume integral in the weak form.
#h=b+c+d+e+f+g
#print(h)

# i,j,l are components of n , the normalvector  (Facetnormal) 
#i=dot(n,nx)
#j=dot(n,ny)
#l=dot(n,nz)

#m equals "dot(k*grad(LagrangeMultiplier)),n)"
#m=i+j+l+b+c+d

#ndim = V.dim()
#tempvec=np.zeros((ndim,3))
#print(tempvec)
#print type(u0_FE)
#print(tempvec[:,0])
#print(b)

#tempvec[:,0]=b
#tempvec[:,1]=c
#tempvec[:,2]=d
#tempvec should be "dot(k,grad(LagrangeMultiplier))" 

#print type(grad(LagrangeMultiplier))
#grad(LagrangeMultiplier)

#a_imp = h*dx #+C*m*testv*ds

#a = dot(dot(k,grad(LagrangeMultiplier)), grad(testv))*dx+C*dot(k,grad(LagrangeMultiplier))*testv*ds # dot product of k and grad(LM) should yield desired result as off-diagonal elements of k are zero. #Here implicitly add inhomogeneous Neumann boundary condition 


# Nonlinear solve
#F = q(u)*dot(grad(u), grad(v))*dx - f*v*dx #example of book
#F = div(u0v0w0)*testv*dx - dot(grad(LagrangeMultiplier), grad(testv))*dx
#solve(F == 0, LagrangeMultiplier, bcs) # nonlinear solve


# Linear solve: Here is finally the lines before solving to assemble the bilinear form a & L.
#a_exp = dot(grad(LagrangeMultiplier), grad(testv))*dx
#a=a_exp+dot(kgradLM,n)*dx #+C*m*testv*ds #implicit added neumann condition
#L = div(u0v0w0)*testv*dx-sum(integrals_N) 
# print (a_imp-a_exp)

# Linear sovle: original that works
#LagrangeMultiplier = Function(V)
#a = dot(grad(LagrangeMultiplier), grad(testv))*dx
#L = div(u0v0w0)*testv*dx-sum(integrals_N)
#print type(a)
#print type(L)
#solve(a == L, LagrangeMultiplier, bcs)

a = dot(grad(LagrangeMultiplier), grad(testv))*dx
L = div(u0v0w0)*testv*dx-sum(integrals_N)

# Compute solution
LagrangeMultiplier = Function(V)
solve(a == L, LagrangeMultiplier, bcs)

# Plot solution
LagrangeMultiplier.rename('LagrangeMultiplier', 'LagrangeMultiplier_solution')
#plot(LagrangeMultiplier, wireframe=True, interactive=True)


# Save solution to file in VTK format
vtkfile = File('osb_LagrangeMultiplier.pvd')
vtkfile << LagrangeMultiplier

#### Find the winddata

uvw = Function(VFS)

### Look at the correction from the initialfield

LMdx = Function(V)
LMdx = project(LagrangeMultiplier.dx(0), V)
LMdy = Function(V)
LMdy = project(LagrangeMultiplier.dx(1), V)
LMdz = Function(V)
LMdz = project(LagrangeMultiplier.dx(2), V)
LM_derivatives = Function(VFS)
assign(LM_derivatives.sub(0), LMdx)
assign(LM_derivatives.sub(1), LMdx)
assign(LM_derivatives.sub(2), LMdx)
vtkfile = File('osb_LMdx.pvd')
vtkfile << LMdx

LM_derivatives.rename('LM_derivatives', 'LM_derivatives')
#plot(LM_derivatives, wireframe=True, interactive=True)
vtkfile = File('LM_derivatives.pvd')
vtkfile << LM_derivatives

u = project(u0+LMdx, V)
v = project(v0+LMdy, V)
w = project(w0+LMdz, V)
assign(uvw.sub(0), u)
assign(uvw.sub(1), v)
assign(uvw.sub(2), w)

vtkfile = File('osb_uvw.pvd')
vtkfile << uvw
uvw.rename('uvw','osb_uvw')
#plot(uvw, wireframe=True, interactive=True)

## Get the prefactor there
u2v2w2 = Function(VFS)
u2 = project(u0+(1/(2*alpha*alpha))*LMdx, V)
v2 = project(v0+(1/(2*alpha*alpha))*LMdy, V)
w2 = project(w0+(1/(2*alpha*alpha))*LMdz, V)
assign(u2v2w2.sub(0), u2)
assign(u2v2w2.sub(1), v2)
assign(u2v2w2.sub(2), w2)
u2v2w2.rename('u2v2w2','osb_u2v2w2')
plot(u2v2w2, wireframe=True, interactive=True)
vtkfile = File('osb_u2v2w2.pvd')
vtkfile << u2v2w2


debug1 = True
if debug1:

	# Print number of vertices in mesh
	#print('%s is the number of vertices in the mesh' % mesh.num_vertices()) 
	print('Number of vertices in the mesh:', mesh.num_vertices())
	for x in mesh.coordinates():
		d=d  #Here can you add some debugging, for example print dofs that are wakes etc.. 

        # Print the Dirichlet conditions
        print('Number of Dirichlet conditions:', len(bcs))
        if V.ufl_element().degree() == 1:  # P1 elements
            d2v = dof_to_vertex_map(V)
            coor = mesh.coordinates()
            for i, bc in enumerate(bcs):
                print('Dirichlet condition %d' % i)
                boundary_values = bc.get_boundary_values()
		print ('Number of boundary values in the above Dirichlet condition:', len(bc.get_boundary_values()))
#                for dof in boundary_values:
#                    print('   dof %2d: u = %g' % (dof, boundary_values[dof]))
#                    if V.ufl_element().degree() == 1:
#                        print('    at point %s' %
#                              (str(tuple(coor[d2v[dof]].tolist()))))









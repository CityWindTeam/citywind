# This file is to test and see if the generate_mesh function in FEniCS/mshr is approximating fine features as smooth surfaces like we experienced when meshing from .obj file landscape geometry from GAL.

# När man ställer frågor till FEniCS QA will dom att man inkluderar ett minimum working example. Jag försöker skapa ett sådant här och undrar om du kan hjälpa till lite med att skriva .off fil i python...  Ett annat sätt kan vara att länka till befintlig .off (ej .obj) skriven från GAL. Men det är inte lika bra då den länken varar inte för evigt. Är detta görbart? Vill du lägga up kodavsnittet från GAL där .obj filen skapas här på citywind? 


from dolfin import *
from mshr import *

N=float(1000)
n=float(2)

#Here I am creating small surface geometry with primitives
#outer = Box(Point(0, 0, 0), Point(N, N, N))
#inner = Box(Point(N/2-n/2,N/2-n/2,0), Point(N/2+n/2,N/2+n/2,n))
#domain = outer - inner

# Here the same geometry more or less but from .off.
domain = Surface3D('test_mesh2.off')
mesh = generate_mesh(domain, 1)
plot(mesh, interactive=True)

# With the above two geometries I cannot reproduce the surface reduction that we saw with geometry of from GAL.

# Now the only I can think of is to write an .off file in Python with some kind of simple ground topography, reminding of what we tried when we saw surface reduction. 


# write to .off

#Man kan ju ta en enkel topografi - varann hög varanann låg tex.

# generate mesh
domain = Surface3D('off_file_that_we_just_wrote.off')
mesh = generate_mesh(domain, 3)
plot(mesh, interactive=True)

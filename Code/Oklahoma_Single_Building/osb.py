# This script reads inital wind field from file created with Fortran implementation. It defined boundaries and sets up boundary conditions therefrom based on planes defined by corners of domain, as opposed to later scripts that where boundaries are defined by queries i.e. max/min in spatial directions as domain is aligned with coordinate axes.

from fenics import *
from dolfin import *
from mshr import *
from scipy import *
from scipy.interpolate import RegularGridInterpolator
#from astropy.io import ascii

# import arcpy
import numpy as np


# This is based on "poisson_mesh.py" and should encompass oklahoma_single_building topography and initial velocity fields as set up by NUD-Fortran environement.

#### Read initial velocity fields and griddata

u0_car = loadtxt("u0.txt", comments="#", delimiter=",", unpack=False)
v0_car = loadtxt("v0.txt", comments="#", delimiter=",", unpack=False)
w0_car = loadtxt("w0.txt", comments="#", delimiter=",", unpack=False)
#u0v0w0_RGI = loadtxt("u0v0w0_RGI.txt", dtype={'formats':('S1','S1','S1')}, delimiter=" ", unpack=False)
#u0v0w0_RGI = np.loadtxt("u0v0w0_RGI.txt", dtype={'names': ('gender', 'age', 'weight'), 'formats': ('f', 'f', 'f')})
u0v0w0_RGI = loadtxt("u0v0w0_RGI.txt")
#u0_RGI = loadtxt("u0_RGI.txt", comments="#", delimiter=",", unpack=False)
#v0_RGI = loadtxt("v0_RGI.txt", comments="#", delimiter=",", unpack=False)
#w0_RGI = loadtxt("u0_RGI.txt", comments="#", delimiter=",", unpack=False)
GRID_ORIGOZ = 0
GRID_ORIGOX = loadtxt("GRID%ORIGOX.txt", comments="#", delimiter=",", unpack=False)
GRID_ORIGOY = loadtxt("GRID%ORIGOY.txt", comments="#", delimiter=",", unpack=False)
GRID_XMIN = loadtxt("GRID%XMIN.txt", comments="#", delimiter=",", unpack=False)
GRID_XMAX = loadtxt("GRID%XMAX.txt", comments="#", delimiter=",", unpack=False)
GRID_YMIN = loadtxt("GRID%YMIN.txt", comments="#", delimiter=",", unpack=False)
GRID_YMAX = loadtxt("GRID%YMAX.txt", comments="#", delimiter=",", unpack=False)
GRID_ZMIN = loadtxt("GRID%ZMIN.txt", dtype='float', comments="#", delimiter=",", unpack=False)
GRID_ZMAX = loadtxt("GRID%ZMAX.txt", dtype='float', comments="#", delimiter=",", unpack=False)
GRID_DX = loadtxt("GRID%DX.txt", comments="#", delimiter=",", unpack=False)
GRID_DY = loadtxt("GRID%DY.txt", comments="#", delimiter=",", unpack=False)
GRID_DZ = loadtxt("GRID%DZ.txt", comments="#", delimiter=",", unpack=False)
#GRID_NCOLS = loadtxt("GRID%NCOLS.txt", comments="#", delimiter=",", unpack=False)
#GRID_NROWS = loadtxt("GRID%NROWS.txt", comments="#", delimiter=",", unpack=False)
GRID_THETA = loadtxt("GRID%THETA.txt", comments="#", delimiter=",", unpack=False)
GRID_NX = loadtxt("GRID%NX.txt",dtype='int', comments="#", delimiter=",", unpack=False)
GRID_NY = loadtxt("GRID%NY.txt",dtype='int', comments="#", delimiter=",", unpack=False)
GRID_NZ = loadtxt("GRID%NZ.txt",dtype='int', comments="#", delimiter=",", unpack=False)

Vertices_shp = loadtxt("Vertices_shp.txt", dtype='float', delimiter="\t", comments="#", unpack=False)

#### Topography and mesh
#Read .off file that was created manually
#if not has_cgal():
#    print "DOLFIN must be compiled with CGAL to run this demo."
#    exit(0)
mesh = Mesh()  #empty mesh
domain = Surface3D("oklahoma_single_building.off")
mesh = generate_mesh(domain,32)
#plot(mesh, interactive=True)

#### Define function space
V = FunctionSpace(mesh, 'CG', 1) # so that dofs are only in mesh vertices
VFS = VectorFunctionSpace(mesh, 'CG', 1, dim=3)
n = V.dim()
d = mesh.geometry().dim()

#### Find the RHS of the poisson equation (= div (initial velocity field on the FEniCS grid)). 
### Interpolate the initial wind field to the fenics mesh

## FEniCS mesh: coordinates of vertices in numpy format 
CoorFE2 = np.zeros(mesh.num_vertices()) # This is the wrong ordering (plot of initial windfield is corrupt)
CoorFE2 = mesh.coordinates()
CoorFE_d2v = CoorFE2[dof_to_vertex_map(V)]

np.savetxt("CoorFE_d2v.csv", CoorFE_d2v, delimiter=",")
CoorFE = V.tabulate_dof_coordinates() # This is the correct ordering
np.savetxt("tab_dof_coord.csv", CoorFE, delimiter=",")


##V_dof_coordinates = V.tabulate_dof_coordinates()
##V_dof_coordinates.resize((n, d))
##V_dof_x = V_dof_coordinates[:, 0]
##V_dof_y = V_dof_coordinates[:, 1]
##V_dof_z = V_dof_coordinates[:, 2]


# Alternative ways to obtain vertex coordinates
# https://fenicsproject.org/qa/1093/obtaining-vertex-values-after-loading-a-mesh
# https://fenicsproject.org/qa/3975/interpolating-vector-function-from-python-code-to-fenics
# https://fenicsproject.org/qa/3932/accessing-the-coordinates-of-a-degree-of-freedom 

## Cartesian mesh: 
# Coordinates (for the RegularGridInterpolator)!

x_c = linspace(GRID_ORIGOX+GRID_XMIN+0.5*GRID_DX,GRID_ORIGOX+GRID_XMAX-0.5*GRID_DX,GRID_NX)
y_c = linspace(GRID_ORIGOY+GRID_YMIN+0.5*GRID_DY,GRID_ORIGOY+GRID_YMAX-0.5*GRID_DY,GRID_NY)
z_c = linspace(GRID_ZMIN+0.5*GRID_DZ,GRID_ZMAX-0.5*GRID_DZ,GRID_NZ)
# Plot these "simulated" Cartesian coordinates to see if the coincide with NUD-Fortran environment. Print to .vtk


# Initialfield of the Cartesian mesh (for the RegularGridInterpolator)
#The below is applicable (with some error...) when u0 v0 w0 is read 
u0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
v0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
w0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
u0v0w0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ,3))
#ir=0
#for iz in range(0,GRID_NZ):
#	for iy in range(0,GRID_NY):
#		for ix in range(0,GRID_NX):
#			u0_RGI[ix][iy][iz]=u0_car[ir]
#			v0_RGI[ix][iy][iz]=v0_car[ir]
#			w0_RGI[ix][iy][iz]=w0_car[ir]
#			ir+=1

ir=0
for iz in range(0,GRID_NZ):
	for iy in range(0,GRID_NY):
		for ix in range(0,GRID_NX):
			u0v0w0_RGI[ix][iy][iz][0]=u0_car[ir]
			u0v0w0_RGI[ix][iy][iz][1]=v0_car[ir]
			u0v0w0_RGI[ix][iy][iz][2]=w0_car[ir]

			u0_RGI[ix][iy][iz]=u0_car[ir]
			v0_RGI[ix][iy][iz]=v0_car[ir]
			w0_RGI[ix][iy][iz]=w0_car[ir]
			ir+=1

# Here would be nice to export to VTK to verify data transport from Fortran to Python/FEniCS
#plot(u0_RGI, title='Initial wind field component u0') # FEniCS plot command of course does not work
N=GRID_NX*GRID_NY*GRID_NZ
#vtkfile=open("u0v0w0_RGI_python.vtk", "w")
#vtkfile.write("# vtk DataFile Version 3.0\r\n")
#vtkfile.write('Initial velocity field u0_RGI intended for interpolation\r\n')
#vtkfile.write('ASCII\r\n')
#vtkfile.write('DATASET STRUCTURED_POINTS\r\n')
#vtkfile.write('%s\t%s\t%s\t%s\r\n' % ('DIMENSIONS', GRID_NX, GRID_NY, GRID_NZ))
#vtkfile.write('%s\t' % ('DIMENSIONS'))
#vtkfile.write('{}\t {}\t {}\r\n'.format(GRID_NX, GRID_NY, GRID_NZ))
#vtkfile.write('%s\t' % ('ORIGIN'))
#vtkfile.write('{}\t {}\t {}\r\n'.format(GRID_ORIGOX, GRID_ORIGOY, GRID_ORIGOZ))
#vtkfile.write('%s\t' % ('SPACING'))
#vtkfile.write('{}\t {}\t {}\r\n'.format(GRID_DX, GRID_DY, GRID_DZ))
#vtkfile.write('%s\t' % ('POINT_DATA'))
#vtkfile.write('%s\r\n' % N)
#vtkfile.write('{}\t {}\t {}\r\n'.format('VECTORS', 'Winddata', 'float'))
#ascii.write([u0[0]], 'u0v0w0_RGI_python.vtk', formats={})
#
#for i in range(0,N):
#	vtkfile.write('{:f}\t {:f}\t {:f}\r\n'.format(u0[i], v0[i], w0[i]))
#vtkfile.close()

## Interpolation to retrieve initial field defined in the FEniCS mesh.

# Define functions to interpolate each space-dimension of the initialfield
fn_u0 = RegularGridInterpolator((x_c,y_c,z_c), u0_RGI, bounds_error=False, fill_value=None) 
fn_v0 = RegularGridInterpolator((x_c,y_c,z_c), v0_RGI, bounds_error=False, fill_value=None) 
fn_w0 = RegularGridInterpolator((x_c,y_c,z_c), w0_RGI, bounds_error=False, fill_value=None)
 
fn_u0v0w0 = RegularGridInterpolator((x_c,y_c,z_c), u0v0w0_RGI, bounds_error=False, fill_value=None)
 

#Interpolate!
u0_FE=fn_u0(CoorFE)
v0_FE=fn_v0(CoorFE)
w0_FE=fn_w0(CoorFE)
u0v0w0_FE=fn_u0v0w0(CoorFE)

#Assemble to verify with Paraview that the initial wind field is correct.
u0v0w0_FE2=np.zeros((n,3)) ##This yields the same resulting vectorfield
u0v0w0_FE2[:,0]=u0_FE
u0v0w0_FE2[:,1]=v0_FE
u0v0w0_FE2[:,2]=w0_FE

#u0v0w0_FE2=np.zeros((3,n))
#u0v0w0_FE2[0,:]=u0_FE
#u0v0w0_FE2[1,:]=v0_FE
#u0v0w0_FE2[2,:]=w0_FE

## Transfer to FEniCS "function"
#https://fenicsproject.org/qa/2715/coordinates-u_nodal_values-using-numerical-source-function
u0 = Function(V)
v0 = Function(V)
w0 = Function(V)
u0v0w0 = Function(VFS)
u0v0w02 = Function(VFS)
u0.vector()[:] = u0_FE # Also possible to import divergence of the initial field from (i.e. the RHS) from NUD Fortran interpolate with Python and apply to the mesh as a scalar field.
v0.vector()[:] = v0_FE
w0.vector()[:] = w0_FE

#uvw.vector()[:] = u0v0w0_FE
#uvw2.vector()[:] = u0v0w0_FE2
assign(u0v0w0.sub(0), u0)
assign(u0v0w0.sub(1), v0)
assign(u0v0w0.sub(2), w0)

#plot(u, title='Initial wind field component u0 in FEniCS mesh', interactive=True)
#plot(v, title='Initial wind field component v0 in FEniCS mesh', interactive=True)
#plot(w, title='Initial wind field component w0 in FEniCS mesh', interactive=True)
#plot(uvw, title='Initial wind field all components in FEniCS mesh', interactive=True)

vtkfile = File('osb_u0_FEniCS.pvd')
vtkfile << u0
vtkfile = File('osb_v0_FEniCS.pvd')
vtkfile << v0
vtkfile = File('osb_w0_FEniCS.pvd')
vtkfile << w0
vtkfile = File('osb_u0v0w0_FEniCS.pvd')
vtkfile << u0v0w0


##Calculate divergence to obtain the R.H.S.

#Make sure everything is in correct dimension and so forth..
#RHS = grad(uvw)[0] + grad(uvw)[1] + grad(uvw)[2]

RHS_VFS = div(u0v0w0)
RHS = project(RHS_VFS, V)
File("osb_RHS.pvd") << RHS


#plot(RHS, title='Source term',  interactive=True)
# Save RHS to file in VTK format
#vtkfile = File('RHS_div_initialfield.pvd')
#vtkfile << RHS

#plot(RHS_VFS, title='Source term',  interactive=True)
#vtkfile = File('RHS_VFS_div_initialfield.pvd')
#vtkfile << RHS_VFS

#### Define boundary conditions chapter 4.4.4. in the Tutorial and conditions is set out by manual drawing: These boundary conditions are such that the volume aligns with the flow direction, the inflow is thus in line with to x[0]. The inflow is constant (u0) over the leading face (Neumann, dlambda/dn=0) whereas the outflow may vary freely (Dirichlet, lambda=0). No flow is allowed to exit in other directions, x[1] and x[2], hence Neumann, dlambda/dn=0 (and v0=w0=0 in the initial field.) //CL

# First mark the different boundaries using MeshFunction "facetfunction" and SubDomain.
 
boundary_markers = FacetFunction('size_t',mesh)# This is beacause default is SubDomain 0 for the facets which coincides with the numbering of one of the SubDomains defined explicitly below.
boundary_markers.set_all(9999)

class Wholeboundary(SubDomain): #This SubDomain is created for debugging purposes to see that all vertices correctly belong to any of the correct subdomains
    def inside(self, x, on_boundary):
       return on_boundary

bx500 = Wholeboundary()
bx500.mark(boundary_markers, 500)
printnormal = True
class BoundaryX0(SubDomain): # spacedomain x[0] positive; outflow boundary
#Define the plane spanned by points 11,12,15,14 in 1-based indexing, or 10,11,14,13 in 0-based indexing  
	vector1 = [Vertices_shp[14][0] - Vertices_shp[10][0], Vertices_shp[14][1] - Vertices_shp[10][1], Vertices_shp[14][2] - Vertices_shp[10][2]]

	vector2 = [Vertices_shp[11][0] - Vertices_shp[10][0], Vertices_shp[11][1] - Vertices_shp[10][1], Vertices_shp[11][2] - Vertices_shp[10][2]]

	cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

	a = cross_product[0]
	b = cross_product[1]
	c = cross_product[2]
	d = - (cross_product[0] * Vertices_shp[10][0] + cross_product[1] * Vertices_shp[10][1] + cross_product[2] * Vertices_shp[10][2])
#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

	norm=np.linalg.norm([a,b,c])
	nx0=a/norm
	ny0=b/norm
	nz0=c/norm
	if printnormal:
		print('nx0=', nx0)
		print('ny0=', ny0)
		print('nz0=', nz0)

	def inside(self, x, on_boundary):
		tol = 1E-4
		return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

bx0 = BoundaryX0()
bx0.mark(boundary_markers, 0)	

class BoundaryX1(SubDomain): # building: dim x[0] positive
#Define the plane spanned by points 3,4,7,8 in 1-based indexing, or 2,3,6,7 in 0-based indexing  	
	vector1 = [Vertices_shp[7][0] - Vertices_shp[3][0], Vertices_shp[7][1] - Vertices_shp[3][1], 	Vertices_shp[7][2] - Vertices_shp[3][2]]

	vector2 = [Vertices_shp[2][0] - Vertices_shp[3][0], Vertices_shp[2][1] - Vertices_shp[3][1], 	Vertices_shp[2][2] - Vertices_shp[3][2]]

	cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

	a = cross_product[0]
	b = cross_product[1]
	c = cross_product[2]
	d = - (cross_product[0] * Vertices_shp[3][0] + cross_product[1] * Vertices_shp[3][1] + 		cross_product[2] * Vertices_shp[3][2])
#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

	norm=np.linalg.norm([a,b,c])
	nx1=a/norm
	ny1=b/norm
	nz1=c/norm
	if printnormal:
		print('nx1=', nx1)
		print('ny1=', ny1)
		print('nz1=', nz1)

	def inside(self, x, on_boundary):
		tol = 1E-2 #I decrease from machine tolerance because of propating errors while finding the equation of the plane as the precision of the numbers in vertices_shp and in the .off file is perhaps not the same
		return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

bx1 = BoundaryX1()
bx1.mark(boundary_markers, 1)


class BoundaryX2(SubDomain): # building x[2] positive (Roof of building)
		
	nx2=0
	ny2=0
	nz2=-1
	if printnormal:
		print('nx2=', nx2)
		print('ny2=', ny2)
		print('nz2=', nz2)

	def inside(self, x, on_boundary):
		tol = 1E-4
		return on_boundary and near(x[2], Vertices_shp[5][2], tol) # Building has uniform height
bx2 = BoundaryX2()
bx2.mark(boundary_markers, 2)


class BoundaryX3(SubDomain): # building: dim x[0] negative
#Define the plane spanned by points 1,2,6,5 in 1-based indexing, or 0,1,5,4 in 0-based indexing  	
	vector1 = [Vertices_shp[1][0] - Vertices_shp[0][0], Vertices_shp[1][1] - Vertices_shp[0][1], Vertices_shp[1][2] - Vertices_shp[0][2]]

	vector2 = [Vertices_shp[4][0] - Vertices_shp[0][0], Vertices_shp[4][1] - Vertices_shp[0][1], Vertices_shp[4][2] - Vertices_shp[0][2]]
	cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

	a = cross_product[0]
	b = cross_product[1]
	c = cross_product[2]
	d = - (cross_product[0] * Vertices_shp[0][0] + cross_product[1] * Vertices_shp[0][1] + cross_product[2] * Vertices_shp[0][2])
#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

	norm=np.linalg.norm([a,b,c])
	nx3=a/norm
	ny3=b/norm
	nz3=c/norm
	if printnormal:
		print('nx3=', nx3)
		print('ny3=', ny3)
		print('nz3=', nz3)

	def inside(self, x, on_boundary):
		tol = 1E-2 #I decrease from machine tolerance because of propating errors while finding the equation of the plane as the precision of the numbers in vertices_shp and in the .off file is perhaps not the same
		return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

bx3 = BoundaryX3()
bx3.mark(boundary_markers, 3)

class BoundaryX4(SubDomain): # building: dim x[1] positive
#Define the plane spanned by points 2,3,6,7 in 1-based indexing, or 1,2,5,6 in 0-based indexing  	
	vector1 = [Vertices_shp[6][0] - Vertices_shp[2][0], Vertices_shp[6][1] - Vertices_shp[2][1], Vertices_shp[6][2] - Vertices_shp[2][2]]

	vector2 = [Vertices_shp[1][0] - Vertices_shp[2][0], Vertices_shp[1][1] - Vertices_shp[2][1], Vertices_shp[1][2] - Vertices_shp[2][2]]
	cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

	a = cross_product[0]
	b = cross_product[1]
	c = cross_product[2]
	d = - (cross_product[0] * Vertices_shp[2][0] + cross_product[1] * Vertices_shp[2][1] + cross_product[2] * Vertices_shp[2][2])
#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

	norm=np.linalg.norm([a,b,c])
	nx4=a/norm
	ny4=b/norm
	nz4=c/norm
	if printnormal:
		print('nx4=', nx4)
		print('ny4=', ny4)
		print('nz4=', nz4)

	def inside(self, x, on_boundary):
		tol = 1E-2 #I decrease from machine tolerance because of propating errors while finding the equation of the plane as the precision of the numbers in vertices_shp and in the .off file is perhaps not the same
		return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

bx4 = BoundaryX4()
bx4.mark(boundary_markers, 4)

class BoundaryX5(SubDomain):# building: dim x[1] negative
#Define the plane spanned by points 1,4,5,8 in 1-based indexing, or 0,3,4,7 in 0-based indexing  	
	vector1 = [Vertices_shp[4][0] - Vertices_shp[0][0], Vertices_shp[4][1] - Vertices_shp[0][1], Vertices_shp[4][2] - Vertices_shp[0][2]]

	vector2 = [Vertices_shp[3][0] - Vertices_shp[0][0], Vertices_shp[3][1] - Vertices_shp[0][1], Vertices_shp[3][2] - Vertices_shp[0][2]]
	cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

	a = cross_product[0]
	b = cross_product[1]
	c = cross_product[2]
	d = - (cross_product[0] * Vertices_shp[0][0] + cross_product[1] * Vertices_shp[0][1] + cross_product[2] * Vertices_shp[0][2])
#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

	norm=np.linalg.norm([a,b,c])
	nx5=a/norm
	ny5=b/norm
	nz5=c/norm
	if printnormal:
		print('nx5=', nx5)
		print('ny5=', ny5)
		print('nz5=', nz5)

	def inside(self, x, on_boundary):
		tol = 1E-2 #I decrease from machine tolerance because of propating errors while finding the equation of the plane as the precision of the numbers in vertices_shp and in the .off file is perhaps not the same
		return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)
bx5 = BoundaryX5()
bx5.mark(boundary_markers, 5)

class BoundaryX6(SubDomain): # spacedomain x[0] negative
#Define the plane spanned by points 9,10,13,16 in 1-based indexing, or 8,9,12,15 in 0-based indexing  	
	vector1 = [Vertices_shp[12][0] - Vertices_shp[8][0], Vertices_shp[12][1] - Vertices_shp[8][1], Vertices_shp[12][2] - Vertices_shp[8][2]]

	vector2 = [Vertices_shp[9][0] - Vertices_shp[8][0], Vertices_shp[9][1] - Vertices_shp[8][1], Vertices_shp[9][2] - Vertices_shp[8][2]]
	cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

	a = cross_product[0]
	b = cross_product[1]
	c = cross_product[2]
	d = - (cross_product[0] * Vertices_shp[8][0] + cross_product[1] * Vertices_shp[8][1] + cross_product[2] * Vertices_shp[8][2])
#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

	norm=np.linalg.norm([a,b,c])
	nx6=a/norm
	ny6=b/norm
	nz6=c/norm
	if printnormal:
		print('nx6=', nx6)
		print('ny6=', ny6)
		print('nz6=', nz6)

	def inside(self, x, on_boundary):
		tol = 1E-4
		return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

bx6 = BoundaryX6()
bx6.mark(boundary_markers, 6)

class BoundaryX7(SubDomain): # spacedomain x[2] negative
	
	nx7=0
	ny7=0
	nz7=-1
	if printnormal:
		print('nx7=', nx7)
		print('ny7=', ny7)
		print('nz7=', nz7)

	def inside(self, x, on_boundary):
		tol = 1E-4
		return on_boundary and near(x[2], 0, tol)

bx7 = BoundaryX7()
bx7.mark(boundary_markers, 7)

class BoundaryX8(SubDomain): # spacedomain x[1] positive
#Define the plane spanned by points 10,11,15,16 in 1-based indexing, or 9,10,14,15 in 0-based indexing  	
	vector1 = [Vertices_shp[15][0] - Vertices_shp[9][0], Vertices_shp[15][1] - Vertices_shp[9][1], Vertices_shp[15][2] - Vertices_shp[9][2]]

	vector2 = [Vertices_shp[10][0] - Vertices_shp[9][0], Vertices_shp[10][1] - Vertices_shp[9][1], Vertices_shp[10][2] - Vertices_shp[9][2]]
	cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

	a = cross_product[0]
	b = cross_product[1]
	c = cross_product[2]
	d = - (cross_product[0] * Vertices_shp[9][0] + cross_product[1] * Vertices_shp[9][1] + cross_product[2] * Vertices_shp[9][2])
#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

	norm=np.linalg.norm([a,b,c])
	nx8=a/norm
	ny8=b/norm
	nz8=c/norm
	if printnormal:
		print('nx8=', nx8)
		print('ny8=', ny8)
		print('nz8=', nz8)

	def inside(self, x, on_boundary):
		tol = 1E-4
		return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)

bx8 = BoundaryX8()
bx8.mark(boundary_markers, 8)

class BoundaryX9(SubDomain): # spacedomain x[1] negative
#Define the plane spanned by points 9,12,13,14 in 1-based indexing, or 8,11,12,13 in 0-based indexing  	
	vector1 = [Vertices_shp[13][0] - Vertices_shp[11][0], Vertices_shp[13][1] - Vertices_shp[11][1], Vertices_shp[13][2] - Vertices_shp[11][2]]

	vector2 = [Vertices_shp[8][0] - Vertices_shp[11][0], Vertices_shp[8][1] - Vertices_shp[11][1], Vertices_shp[8][2] - Vertices_shp[11][2]]
	cross_product = [vector1[1] * vector2[2] - vector1[2] * vector2[1], -1 * (vector1[0] * vector2[2] - vector1[2] * vector2[0]), vector1[0] * vector2[1] - vector1[1] * vector2[0]]

	a = cross_product[0]
	b = cross_product[1]
	c = cross_product[2]
	d = - (cross_product[0] * Vertices_shp[11][0] + cross_product[1] * Vertices_shp[11][1] + cross_product[2] * Vertices_shp[11][2])
#a,b,c is a normal to the plane, with this definition of vector1 and vector2 it will point out of the domain into the building. Since unitnormal*cos(theta) will appear in the value for the boundary condition I calculate it now.

	norm=np.linalg.norm([a,b,c])
	nx9=a/norm
	ny9=b/norm
	nz9=c/norm
	
	if printnormal:
		print('nx9=', nx9)
		print('ny9=', ny9)
		print('nz9=', nz9)

	def inside(self, x, on_boundary):
		tol = 1E-4
		return on_boundary and near(self.a*x[0]+self.b*x[1]+self.c*x[2]+self.d, 0, tol)
bx9 = BoundaryX9()
bx9.mark(boundary_markers, 9)

class BoundaryX10(SubDomain): # spacedomain x[2] positive
	
	nx10=0
	ny10=0
	nz10=1
	if printnormal:
		print('nx10=', nx10)
		print('ny10=', ny10)
		print('nz10=', nz10)

	def inside(self, x, on_boundary):
		tol = 1E-4
		return on_boundary and near(x[2], Vertices_shp[15][2], tol)

bx10 = BoundaryX10()
bx10.mark(boundary_markers, 10)

### Then specify the form of boundary conditions	

alpha = Constant(1.0)

## Two different implementations of the boundary conditions
#Must verify the sign here!!!
bc1=2*alpha*alpha*(BoundaryX1.nx1*u0+BoundaryX1.ny1*v0+BoundaryX1.nz1*w0)
bc2=2*alpha*alpha*(BoundaryX2.nx2*u0+BoundaryX2.ny2*v0+BoundaryX2.nz2*w0)
bc3=2*alpha*alpha*(BoundaryX3.nx3*u0+BoundaryX3.ny3*v0+BoundaryX3.nz3*w0)
bc4=2*alpha*alpha*(BoundaryX4.nx4*u0+BoundaryX4.ny4*v0+BoundaryX4.nz4*w0)
bc5=2*alpha*alpha*(BoundaryX5.nx5*u0+BoundaryX5.ny5*v0+BoundaryX5.nz5*w0)
bc6=2*alpha*alpha*(BoundaryX6.nx6*u0+BoundaryX6.ny6*v0+BoundaryX6.nz6*w0)
bc7=2*alpha*alpha*(BoundaryX7.nx7*u0+BoundaryX7.ny7*v0+BoundaryX7.nz7*w0)
bc8=2*alpha*alpha*(BoundaryX8.nx8*u0+BoundaryX8.ny8*v0+BoundaryX8.nz8*w0)
bc9=2*alpha*alpha*(BoundaryX9.nx9*u0+BoundaryX9.ny9*v0+BoundaryX9.nz9*w0)
bc10=2*alpha*alpha*(BoundaryX10.nx10*u0+BoundaryX10.ny10*v0+BoundaryX10.nz10*w0)

#from IPython import embed; embed()
#boundary_conditions =  {0: {'Dirichlet': 0},  # spacedomain x[0] positive; outflow boundary
#			1: {'Neumann': bc1}, # building: dim x[0] positive 
#			2: {'Neumann': bc2},   # building x[2] positive (Roof of building)
#			3: {'Neumann': bc3}, # building: dim x[0] negative
#			4: {'Neumann': bc4},   # building: dim x[1] positive
#			5: {'Neumann': bc5},   # building: dim x[1] negative
#			6: {'Neumann': bc6},    # spacedomain x[0] negative
#			7: {'Neumann': bc7},    # spacedomain x[2] negative
#			8: {'Neumann': bc8},    # spacedomain x[1] positive
#			9: {'Neumann': bc9},    # spacedomain x[1] negative
#			10: {'Neumann': bc10}}   # spacedomain x[2] positive

n=FacetNormal(mesh)
#neu_bc=2*alpha*alpha*dot(u0v0w0, n) # There is actually a double minus sign here. one for the facetnormal, found out with the manufactured solution. And one in the boundary condition itself.
neu_vec = as_vector([u0, v0, w0]) 
neu_bc=2*alpha*alpha*dot(neu_vec, n) # There is actually a double minus sign here. one for the facetnormal, found out with the manufactured solution. And one in the boundary condition itself.
#
#
boundary_conditions =  {0: {'Dirichlet': 0},  # spacedomain x[0] positive; outflow boundary
			1: {'Neumann': neu_bc}, # building: dim x[0] positive 
			2: {'Neumann': neu_bc},   # building x[2] positive (Roof of building)
			3: {'Neumann': neu_bc}, # building: dim x[0] negative
			4: {'Neumann': neu_bc},   # building: dim x[1] positive
			5: {'Neumann': neu_bc},   # building: dim x[1] negative
			6: {'Neumann': 0},    # spacedomain x[0] negative; inflow boundary
			7: {'Neumann': 0},    # spacedomain x[2] negative
			8: {'Neumann': 0},    # spacedomain x[1] positive
			9: {'Neumann': 0},    # spacedomain x[1] negative
			10: {'Neumann': 0}}   # spacedomain x[2] positive
#from IPython import embed; embed()
# Visualizee subdomains, to see that they are correct
#plot(boundary_markers, interactive=True)
vtkfile = File('osb_boundary_markers.pvd')
vtkfile << boundary_markers 

# Define variational problem
LagrangeMultiplier = TrialFunction(V)
testv = TestFunction(V)
#f = Constant(RHS)
#f = Constant(-6.0) #-6 is a made up number for the source term, see NUD manual for its real formulation /CL

#Redefine the measure "ds" in terms of our boundary markers
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)

# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are none Robin conditions. 
integrals_N = []
for i in boundary_conditions:
	if 'Neumann' in boundary_conditions[i]:
		if boundary_conditions[i]['Neumann'] != 0:
			g = boundary_conditions[i]['Neumann']
			integrals_N.append(g*testv*ds(i))

# Collect Dirichlet boundary conditions
bcs = []
for i in boundary_conditions:
	if 'Dirichlet' in boundary_conditions[i]:
		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
		bcs.append(bc)
		

# Continue defining variational problem

a = dot(grad(LagrangeMultiplier), grad(testv))*dx
L = div(u0v0w0)*testv*dx-sum(integrals_N)

# Compute solution
LagrangeMultiplier = Function(V)
solve(a == L, LagrangeMultiplier, bcs)

# Plot solution
LagrangeMultiplier.rename('LagrangeMultiplier', 'LagrangeMultiplier_solution')
#plot(LagrangeMultiplier, wireframe=True, interactive=True)


# Save solution to file in VTK format
vtkfile = File('osb_LagrangeMultiplier.pvd')
vtkfile << LagrangeMultiplier

#### Find the winddata

# Final windfield
#u = Function(V)
#v = Function(V)
#w = Function(V)

#u = u0 + (1/2*alpha)*LagrangeMultiplier.dx(0)
#v = v0 + (1/2*alpha)*LagrangeMultiplier.dx(1)
#w = w0 + (1/2*alpha)*LagrangeMultiplier.dx(2)

uvw = Function(VFS)

### Look at the correction from the initialfield
#corru = Function(V)
#corrv = Function(V)
#corrw = Function(V)
LMdx = Function(V)
LMdx = project(LagrangeMultiplier.dx(0), V)
LMdy = Function(V)
LMdy = project(LagrangeMultiplier.dx(1), V)
LMdz = Function(V)
LMdz = project(LagrangeMultiplier.dx(2), V)
LM_derivatives = Function(VFS)
assign(LM_derivatives.sub(0), LMdx)
assign(LM_derivatives.sub(1), LMdx)
assign(LM_derivatives.sub(2), LMdx)
vtkfile = File('osb_LMdx.pvd')
vtkfile << LMdx


#plot(LMdx, wireframe=True, interactive=True)
#plot(LMdy, wireframe=True, interactive=True)
#plot(LMdz, wireframe=True, interactive=True)
LM_derivatives.rename('LM_derivatives', 'LM_derivatives')
#plot(LM_derivatives, wireframe=True, interactive=True)
vtkfile = File('LM_derivatives.pvd')
vtkfile << LM_derivatives

u = project(u0+LMdx, V)
v = project(v0+LMdy, V)
w = project(w0+LMdz, V)
assign(uvw.sub(0), u)
assign(uvw.sub(1), v)
assign(uvw.sub(2), w)

vtkfile = File('osb_uvw.pvd')
vtkfile << uvw
uvw.rename('uvw','osb_uvw')
#plot(uvw, wireframe=True, interactive=True)

## Get the prefactor there
u2v2w2 = Function(VFS)
u2 = project(u0+(1/(2*alpha*alpha))*LMdx, V)
v2 = project(v0+(1/(2*alpha*alpha))*LMdy, V)
w2 = project(w0+(1/(2*alpha*alpha))*LMdz, V)
assign(u2v2w2.sub(0), u2)
assign(u2v2w2.sub(1), v2)
assign(u2v2w2.sub(2), w2)
u2v2w2.rename('u2v2w2','osb_u2v2w2')
plot(u2v2w2, wireframe=True, interactive=True)
plot(div(u2v2w2), wireframe=True, interactive=True)
vtkfile = File('osb_u2v2w2.pvd')
vtkfile << u2v2w2


debug1 = True
if debug1:

	# Print number of vertices in mesh
	#print('%s is the number of vertices in the mesh' % mesh.num_vertices()) 
	print('Number of vertices in the mesh:', mesh.num_vertices())
        # Print all vertices that belong to the boundary (and 9999 WholeBoundary) parts
	for x in mesh.coordinates():
		#if bx0.inside(x, True): print('%s is on bx0' % x)
		#if bx1.inside(x, True): print('%s is on bx1' % x)
		#if bx2.inside(x, True): print('%s is on bx2' % x)
		#if bx3.inside(x, True): print('%s is on bx3' % x)
		#if bx4.inside(x, True): print('%s is on bx4' % x)
		#if bx5.inside(x, True): print('%s is on bx5' % x)
		#if bx6.inside(x, True): print('%s is on bx6' % x)
		#if bx7.inside(x, True): print('%s is on bx7' % x)
		#if bx8.inside(x, True): print('%s is on bx8' % x)
		#if bx9.inside(x, True): print('%s is on bx9' % x)
		#if bx10.inside(x, True): print('%s is on bx10' % x)
		#if bx9999.inside(x, True): print('%s is on bx9999' % x)
		if bx500.inside(x, True) and bx0.inside(x, False) and bx1.inside(x, False) and bx2.inside(x, False) and bx3.inside(x, False) and bx4.inside(x, False) and bx5.inside(x, False) and bx6.inside(x, False) and bx7.inside(x, False) and bx8.inside(x, False) and bx9.inside(x, False) and bx10.inside(x, False): print('%s is exlusively on bx500' % x)
		#if bx500.inside(x, True): print('%s is on bx500' % x)

        # Print the Dirichlet conditions
        print('Number of Dirichlet conditions:', len(bcs))
        if V.ufl_element().degree() == 1:  # P1 elements
            d2v = dof_to_vertex_map(V)
            coor = mesh.coordinates()
            for i, bc in enumerate(bcs):
                print('Dirichlet condition %d' % i)
                boundary_values = bc.get_boundary_values()
		print ('Number of boundary values in the above Dirichlet condition:', len(bc.get_boundary_values()))
#                for dof in boundary_values:
#                    print('   dof %2d: u = %g' % (dof, boundary_values[dof]))
#                    if V.ufl_element().degree() == 1:
#                        print('    at point %s' %
#                              (str(tuple(coor[d2v[dof]].tolist()))))









# This script sets up initial wind field with different types of windzones around buildings as described by Röckle and Kaplan and Dinar. However ot does not describe turbulent zones with interacting buildings such as street canyons, but could be extended to. Geometry is single building. and python expression and classes are used everywhere.

# See also Initfield.py which is based only shapefile data. 
# Now correct order of different types of wakes: Displacement, cavity and wake zone.

from fenics import *
#from dolfin import *
from mshr import *
#from scipy import *
import numpy as np
#import shapefile
from IPython import embed
#from ufl import SpatialCoordinate

#### Load/Import mesh
print("Create the mesh")
mesh=Mesh()
domain = Surface3D("oklahoma_single_building.off")
mesh = generate_mesh(domain,4)
#mesh=UnitCubeMesh(50,50,5)
#plot(mesh, interactive=True)
#File("mesh.pvd") << mesh

#### Define function space
print("Define function space")
V0 = FunctionSpace(mesh,'DG',0) # Discontinues Galerkyn methods yields space functions that are constant over each cell. For kappa (wake markers).
V = FunctionSpace(mesh, 'CG', 1) # so that dofs are only in mesh vertices
VFS = VectorFunctionSpace(mesh, 'CG', 1, dim=3)
ndim = V.dim()
d = mesh.geometry().dim()

V_dof_coordinates = V.tabulate_dof_coordinates()
V_dof_coordinates.resize((ndim, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]

#### Setup initial windfield u0v0w0
print("Setup initial windfield u0v0w0")
# This data is valid for Oklahoma City according to Jan B. 
z0=0.6
Ux=1.97 # at z=10m	
Uy=2.82 # at z=10m

Ustar=(np.sqrt(Ux*Ux+Uy*Uy)*0.4)/np.log(10/z0)
windangle=np.arctan(Uy/Ux) #windangle is the angle of initial wind to true east measured counter-clockwise
Uxstar=(Ux*0.4)/np.log(10/z0)
Uystar=(Uy*0.4)/np.log(10/z0)

#Manipulate so that windangle is 0 < theta < 2*pi. np.arctan returns -pi/2 to pi/2.
if Uy > 0 and Ux > 0: # First quadrant
	windangle=windangle
elif Uy < 0 and Ux > 0: # Fourth quadrant
	windangle=windangle+2*np.pi
elif Uy > 0 and Ux < 0: # Second quadrant
	windangle=windangle+np.pi
elif Uy < 0 and Ux < 0: # Third quadrant
	windangle=windangle+np.pi
print("windangle=",windangle)


degree = 5
Ustar=Constant(Ustar)
Uxstar=Constant(Uxstar)
Uystar=Constant(Uystar)
z0=Constant(z0) # Redfine z0 so FEniCS understands
u0v0w0_abs_exp = Expression("x[2] > 0 ? Ustar/0.4*log(x[2]/z0) : 0", Ustar=Ustar, z0=z0, degree = degree) 
u0_exp = Expression("x[2] > 0 ? Uxstar/0.4*log(x[2]/z0) : 0", Uxstar=Uxstar, z0=z0, degree = degree) 
v0_exp = Expression("x[2] > 0 ? Uystar/0.4*log(x[2]/z0) : 0", Uystar=Uystar, z0=z0, degree = degree) 
w0_exp = Constant(0.0)

#### Topography
# Corners of building from off file, same as used to generate mesh.
X = [-3.368602e+01, -3.335932e+01, 4.287436e+01, 4.254766e+01, -3.368602e+01]
Y = [-1.522800e+01, 1.596804e+01, 1.516969e+01,  -1.602634e+01, -1.522800e+01]
Z = [55,  55,  55,  55,  55]

#### Investigate zones around buildings. Of order of inportance: Displacement zone in front, cavity zone behind, and wake zone farther behind. See Dinar and  Kaplan and Rockles Dissertation.

# Define SubDomains for wakes and mark cells inside different zones. There will be needed three different marker types for different zones since we are iterating over edges and not zone type, otherwise we are risking to overwrite a cell that has already beed marked by another edge and this information we need to keep track of as order of zones matters during wind assignemet. 0 is reserved as an initial mark. 
Displacement_marker_along = CellFunction('size_t', mesh)
Displacement_marker_opposite = CellFunction('size_t', mesh)
Cavity_marker = CellFunction('size_t', mesh)
Wake_marker = CellFunction('size_t', mesh)
Displacement_marker_along.set_all(0)
Displacement_marker_opposite.set_all(0)
Cavity_marker.set_all(0)
Wake_marker.set_all(0)
 
## Procedure is to iterate over all edges (in the future over all buildings also), determine which type of zone the edge gives rise to and mark it accordingly. Then we assign proper winddata to these zones. An edge is the line between to corners on top of the building and can be thought of as a face of the building also.

#Initialize counters to serve as marker numbers. Each zone will have its own marker number
doi=0 # displacement opposite zones 
dai=0 # displacement along zones
ci=0 # cavity
wi=0 # wakes

#Define various stuff that will be needed
edgeangle = [] #edgeangle is the counter clockwise angle between of the current edge to true east
centerY = [] # the center of local coordinate system where the elliptically shaped wakes should have their origin
centerX = [] #
W = [] # Length of edge
Weff = [] # Effective length, projection of edge perpendicular to wind direction. Leff and Weff are used only for diplacement zones.
L = [] # length and width are the same in this implementation as compared to Kaplan and Dinar
Leff = []
H = [] # Height of building/edge
dai_to_i = [] # the key to displacement zones "along" of all zones i.  Use: dai_to_i[dai]=i
doi_to_i = [] # the key to displacement zones "opposite" of all zones i. Use: doi_to_i[doi]=i
wi_to_i = [] # the key to wake zones of all zones i. # wi and ci is redundant as the number of wakes is always the same as number of cavities.
ci_to_i = [] # the key to cavity of all zones i.
LAf = [] # Length of displacement zone "along"
LOf = [] # Length of displacement zone "opposite"
LCr = [] # Length of cavity zone
LWr = [] # Length of wake zone
Corner_coord_along_wind = [] # Coordinate in a rotated system; along wind coordinate. to find building length. Only for wake/cavity zone.
Corner_coord_across_wind = [] # # Coordinate in a rotated system; across wind coordinate. to find building width. Only for wake/cavity zone.
center_r_X = [] # center of ellipse for cavity/wake zones in original coordinate system
center_r_Y = [] # center of ellipse for cavity/wake zones
shearangle = [] # for cavity/wake zones

for i in range(0,4):  # i goes through all edges of the building
	print("i=", i)
	#Determine which type of wake is applicable. So far only Rockle regime I type (free standing building). Corners of buildings are defined oriented clockwise in shapefile for Oklahoma City. Use this info to keep track of front and lee sides of the building.			
			
	tempangle=np.arctan((Y[i+1]-Y[i])/(X[i+1]-X[i]))
		#Manipulate so that 0 < edgeangle < 2*pi. O radians is eastwards, and positive angle is counter-clockwise. Convention of numpy arctan is to return the angle that lies in -pi/2 <-> pi/2
	if (Y[i+1]-Y[i]) > 0 and (X[i+1]-X[i]) > 0: # First quadrant
		edgeangle.append(tempangle)
	elif (Y[i+1]-Y[i]) < 0 and (X[i+1]-X[i]) > 0: # Fourth quadrant
		edgeangle.append(tempangle+2*np.pi)
	elif (Y[i+1]-Y[i]) > 0 and (X[i+1]-X[i]) < 0: # Second quadrant
		edgeangle.append(tempangle+np.pi)
	elif (Y[i+1]-Y[i]) < 0 and (X[i+1]-X[i]) < 0: # Third quadrant
		edgeangle.append(tempangle+np.pi)
	print("edgeangle=",edgeangle[i])

	# Check which type of zone current edge is gives rise to and mark correspondingly. With this implementation the two types of diplacement zones as described by Kapland and Dinan (X-direction and Y-direction) are here named "along" and "opposite". This implementation is a generalisation for buildings that are not cubes. Wind is allowed only along or opposite to the edge direction depending on wind to edge angle.
	
	#Datatypes that are common to all zone types (can be accessed by the counters dai, doi, wi, and ci.)
	centerY.append(Y[i+1]/2+Y[i]/2) 
	centerX.append(X[i+1]/2+X[i]/2)
	W.append(np.sqrt((Y[i+1]-Y[i])**2+(X[i+1]-X[i])**2))
	L.append(W[i]) # without projecting edge to wind 
	H.append(Z[i])
	Weff.append(W[i]*abs(sin(windangle-edgeangle[i]))) # Leff and Weff is only for displacement zone
	Leff.append(Weff[i])
	
	# Displacement zone along
	#if 2*(3/4)*np.pi<windangle-edgeangle[i]<2*np.pi):
	#embed()	
	if ((windangle>edgeangle[i] and 2.0*(3.0/4.0)*np.pi < (windangle-edgeangle[i]) < 2.0*np.pi) or (edgeangle[i]>windangle and 0.0 < (edgeangle[i]-windangle) < np.pi/2.0)):
		dai_to_i.append(i)	
		LAf.append((H[i]*2.0*(Weff[i]/H[i]))/(1.0+0.8*Weff[i]/H[i]))
		print("Displacement zone along mark")
		class Displacement_zone_along(SubDomain):	
			def inside(self, x, on_boundary):
				tol = 1E-14
				#return x[1] > shapeRecs[j].shape.points[i][1] # criteria to be inside wake x[0] x[1] x[2] tol))
				#eq 8 of Kaplan and Dinan:				
				if x[2] < 0.6*H[i]:
					#embed() 
					#Ay=40
					Ay=LAf[dai]*sin(windangle-edgeangle[i])**2*np.sqrt((1-(x[2]/(0.6*H[i]))**2))
					Ax=L[i]/2	#x is the direction along edge and y across edge			
					#return False
					return True if (((x[0]-centerX[i])*cos(-edgeangle[i])-(x[1]-centerY[i])*sin(-edgeangle[i]))/(Ax))**2+(((x[0]-centerX[i])*sin(-edgeangle[i])+(x[1]-centerY[i])*cos(-edgeangle[i]))/(Ay))**2 <= 1 and ((x[0]-centerX[i])*sin(-edgeangle[i])+(x[1]-centerY[i])*cos(-edgeangle[i])) > - tol else False
				else: 
					return False
				
		Displacement_along = Displacement_zone_along()
		Displacement_along.mark(Displacement_marker_along, dai+1)
		dai+=1
	
	# Displacement zone opposite
	elif ((windangle>edgeangle[i] and np.pi < (windangle-edgeangle[i]) < 2.0*(3.0/4.0)*np.pi) or (edgeangle[i] > windangle and np.pi/2.0 < (edgeangle[i]-windangle) < np.pi)):		
		doi_to_i.append(i)	
		LOf.append((H[i]*2.0*(Weff[i]/H[i]))/(1.0+0.8*Weff[i]/H[i]))
		print("Displacement zone opposite mark")
		class Displacement_zone_opposite(SubDomain):
			def inside(self, x, on_boundary):
				tol = 1E-14
				#return x[1] > shapeRecs[j].shape.points[i][1] # criteria to be inside wake x[0] x[1] x[2] tol))
				if x[2] < 0.6*H[i]:
					#embed()
					Ax=L[i]/2
					Ay=LOf[doi]*sin(windangle-edgeangle[i])**2*np.sqrt((1-(x[2]/(0.6*H[i]))**2))
					return True if (((x[0]-centerX[i])*cos(-edgeangle[i])-(x[1]-centerY[i])*sin(-edgeangle[i]))/(Ax))**2+(((x[0]-centerX[i])*sin(-edgeangle[i])+(x[1]-centerY[i])*cos(-edgeangle[i]))/(Ay))**2 <= 1 and ((x[0]-centerX[i])*sin(-edgeangle[i])+(x[1]-centerY[i])*cos(-edgeangle[i])) > - tol else False
					#return False 
				else: 
					return False
		
	
		Displacement_opposite = Displacement_zone_opposite()
		Displacement_opposite.mark(Displacement_marker_opposite, doi+1)
		doi+=1

	# Cavity and wake zone
	elif ((windangle>edgeangle[i] and 0.0 < (windangle-edgeangle[i]) < np.pi) or (edgeangle[i]>windangle and np.pi < (edgeangle[i]-windangle) < 2.0*np.pi)):
		print("This is a Cavity/Wake zone") 				
		ci_to_i.append(i)
		wi_to_i.append(i)
		ci+=1
		wi+=1
	else:
		#Edge of building is aligned with initial wind direction - do nothing
		print("I am not doing anything because edge is aligned with initial wind - or sth else is wrong?")
		#print("windangle=", windangle)
		#print("edgeangle=", edgeangle[i])
	# Cavity and Wakes !
	# Coordinates of the obstacles' corners projected perpendicular/along wind. 
	Corner_coord_along_wind.append(X[i]*cos(-windangle)-Y[i]*sin(-windangle))
	Corner_coord_across_wind.append(Y[i]*cos(-windangle)+X[i]*sin(-windangle))

# Mark Cavity and Wake zones outside for loop since there is only on wake/cavity zone for the whole building. The cavity/wake zone is to be defined in wind-coordinates that are sheared through the angle between wind and the line connecting the two endpoints that span the perpendicular (to wind direction) widest point. Wake and cavity zone is a sheared ellipse with minor semi-axis ending at the widest point of the across-wind-projected building. major semi-axis is LCr and LWr. 

print("Cavity zone mark")
Have = sum(H)/len(H) # The average height of the building is used to estimate the size of cavity/wake zones.
LeffCW = max(Corner_coord_along_wind)-min(Corner_coord_along_wind) #Effective length as the obstacles' projection along to the wind
WeffCW = max(Corner_coord_across_wind)-min(Corner_coord_across_wind) #Effective width as the obstacles' projection perpendicular to the wind
LCr = Have*1.8*(WeffCW/Have)/((LeffCW/Have)**0.3*(1+0.24*WeffCW/Have)) # eq. 3 Kaplan & Dinar
LWr = 3.0*LCr

# Xhi, Xlo, Yhi, Ylo are coordinates of the end points of the building projected perpendicular to wind direction.
Xhi = X[max(enumerate(Corner_coord_across_wind),key=lambda s: s[1])[0]]
Xlo = X[min(enumerate(Corner_coord_across_wind),key=lambda s: s[1])[0]]
Yhi = Y[max(enumerate(Corner_coord_across_wind),key=lambda s: s[1])[0]]
Ylo = Y[min(enumerate(Corner_coord_across_wind),key=lambda s: s[1])[0]]
print("number of the corner with maximum extent across wind", max(enumerate(Corner_coord_across_wind),key=lambda s: s[1])[0])
print("number of the corner with minimum extent across wind", min(enumerate(Corner_coord_across_wind),key=lambda s: s[1])[0])

# Shearangle is the angle from the original coordinate system to the line connecting the obstacles widest points projected perpendicular to the wind. It is from the windangle to this angle that the wake/cavityt ellipse zone needs to be sheared to these widest points. Shearangle is always defined as the line from larger across-wind coordinate to smaller across-wind coordinate. However, the shearing itself should be performed between windangle and shearangle. 	
tempangle=np.arctan((Yhi-Ylo)/(Xhi-Xlo))

#Manipulate so that 0 < shearangle < 2*pi. O radians is eastwards, and positive angle is counter-clockwise. Convention of numpy arctan is to return the angle that lies in -pi/2 <-> pi/2
if (Yhi-Ylo) > 0 and (Xhi-Xlo) > 0: # First quadrant
	shearangle=tempangle
elif (Yhi-Ylo) < 0 and (Xhi-Xlo) > 0: # Fourth quadrant
	shearangle=tempangle+2*np.pi
elif (Yhi-Ylo) > 0 and (Xhi-Xlo) < 0: # Second quadrant
	shearangle=tempangle+np.pi
elif (Yhi-Ylo) < 0 and (Xhi-Xlo) < 0: # Third quadrant
	shearangle=tempangle+np.pi
print("shearangle=", shearangle) 

center_r_X = (Xhi+Xlo) / 2 # (X[ci_to_i[max(enumerate(coord_across_wind),key=lambda s: s[1])[0]]]+X[ci_to_i[min(enumerate(coord_across_wind),key=lambda s: s[1])[0]]])/2
center_r_Y = (Yhi+Ylo) / 2 #(Y[ci_to_i[max(enumerate(coord_across_wind),key=lambda s: s[1])[0]]]+Y[ci_to_i[min(enumerate(coord_across_wind),key=lambda s: s[1])[0]]])/2	

# This definition of cavity zones is only for plotting purposes. The actual wind field is set up by queries of the wake zone. see more below.
class Cavity_zone(SubDomain):
	def inside(self, x, on_boundary):
		tol = 1E-14
		# Here we need to shear the coordinate system so that elliptic zones attaches to the widest points of building. 
		if x[2] < Have:
			Ax=LCr*np.sqrt((1-(x[2]/(Have))**2))
			Ay=WeffCW/2
			Xtran = (x[0]-center_r_X)
			Ytran = (x[1]-center_r_Y)
			Xwind = (x[0]-center_r_X)*cos(-windangle)-(x[1]-center_r_Y)*sin(-windangle) #rotated and translated system: along wind
			Ywind = (x[1]-center_r_Y)*cos(-windangle)+(x[0]-center_r_X)*sin(-windangle) #rotated and translated system: across wind
			Xshear=(Xwind)-(Ywind)*tan(np.pi/2-(shearangle-windangle)) 
			Yshear=Ywind
			return True if (((Xshear)/(Ax))**2 + ((Yshear)/(Ay))**2) <= 1  and Xshear > - tol else False 
		else: 
			return False

Cavity = Cavity_zone()
Cavity.mark(Cavity_marker, 1)

print("Wake zone mark")
class Wake_zone(SubDomain):
	def inside(self, x, on_boundary):
		tol = 1E-14
		# Here we need to shear the coordinate system so that elliptic zones attaches to the widest points of building. 
		if x[2] < Have:
			Ax=LWr*np.sqrt((1-(x[2]/(Have))**2))
			Ay=WeffCW/2				
			Xtran = (x[0]-center_r_X)
			Ytran = (x[1]-center_r_Y)
			Xwind = (x[0]-center_r_X)*cos(-windangle)-(x[1]-center_r_Y)*sin(-windangle) #rotated and translated system: along wind
			Ywind = (x[1]-center_r_Y)*cos(-windangle)+(x[0]-center_r_X)*sin(-windangle) #rotated and translated system: across wind
			Xshear=(Xwind)-(Ywind)*tan(np.pi/2-(shearangle-windangle)) 
			Yshear=Ywind
			return True if (((Xshear)/(Ax))**2 + ((Yshear)/(Ay))**2) <= 1  and Xshear > - tol else False 
		else: 
			return False

Wake = Wake_zone()
Wake.mark(Wake_marker, 1)

#plot(Wake_marker, mesh=mesh, interactive=True, title="Wake_marker")
#plot(Cavity_marker, mesh=mesh, interactive=True, title="Cavity_marker")
#plot(Displacement_marker_along, mesh=mesh, interactive=True, title="Displacement_marker_along")
#plot(Displacement_marker_opposite, mesh=mesh, interactive=True, title="Displacement_marker_opposite")	
#File("Wake_marker.pvd") << Wake_marker
#File("Displacement_marker_along.pvd") << Displacement_marker_along
#File("Displacement_marker_opposite.pvd") << Displacement_marker_opposite
#File("Cavity_marker.pvd") << Wake_marker

#### Assignment of winddata in different zones in order of least importance.

u0v0w0_exp = Expression(("x[2] > 0 ? Uxstar/0.4*log(x[2]/z0) : 0","x[2] > 0 ? Uystar/0.4*log(x[2]/z0) : 0", "w0_exp"), Uxstar=Uxstar, Uystar=Uystar, w0_exp=w0_exp, z0=z0, degree = degree)

class Initfield(Expression):
	print("Assigning initial wind field")
	def __init__(self,Wake_marker, Cavity_marker, Displacement_marker_along, Displacement_marker_opposite, u0v0w0_exp, w0_exp, u0v0w0_abs_exp, edgeangle, center_r_X, center_r_Y, Leff, Weff, LCr, LWr, dai_to_i, doi_to_i, wi_to_i, ci_to_i, windangle, Have, WeffCW, **kwargs):
		self.Wake_marker = Wake_marker
		self.Cavity_marker = Cavity_marker
		self.Displacement_marker_along = Displacement_marker_along
		self.Displacement_marker_opposite = Displacement_marker_opposite
		self.u0v0w0_exp = u0v0w0_exp
		self.u0v0w0_abs_exp = u0v0w0_abs_exp
		self.w0_exp = w0_exp
		self.edgeangle = edgeangle
		self.center_r_X = center_r_X
		self.center_r_Y = center_r_Y
		self.Leff = Leff
		self.Weff = Weff
		self.LCr = LCr
		self.LWr = LWr
		self.dai_to_i = dai_to_i
		self.doi_to_i = doi_to_i
		self.wi_to_i = wi_to_i
		self.ci_to_i = ci_to_i
		self.windangle = windangle
		self.Have = Have
		self.WeffCW = WeffCW

	def eval_cell(self, values, x, cell):
	
	
		# First the Displacement zones - along. the way python elif statements works allows these conditional statements.
		if self.Displacement_marker_along[cell.index] != 0:

			temp_e = self.edgeangle[self.dai_to_i[self.Displacement_marker_along[cell.index]-1]] #edgeangle of the edge that current cell belongs to
			temp_u0v0w0_proj_e = self.u0v0w0_abs_exp(x)*np.cos(temp_e-self.windangle) # Magnitude of the wind projected along the edge	
			values[:] =  [temp_u0v0w0_proj_e*np.cos(temp_e), temp_u0v0w0_proj_e*np.sin(temp_e), self.w0_exp] #Projection of the intial field along edge of buildling. Component-wise defined (X,Y,Z)
	
	# First the Displacement zones - opposite. (this  implementation gives higher precedence to wake zones along the edge since it comes first)
		elif self.Displacement_marker_opposite[cell.index] != 0:
			temp_e = self.edgeangle[self.doi_to_i[self.Displacement_marker_opposite[cell.index]-1]] #edgeangle of the edge that current cell belongs to
			temp_u0v0w0_proj_e = self.u0v0w0_abs_exp(x)*np.cos(temp_e-self.windangle) # Magnitude of the wind projected along the edge	
			values[:] =  [temp_u0v0w0_proj_e*np.cos(temp_e), temp_u0v0w0_proj_e*np.sin(temp_e), self.w0_exp] #Projection of the intial field along edge of buildling. Component-wise defined (X,Y,Z)
		
		# Then Cavity zones. Eq. 5 and 6 of Kaplan and Dinar. with -0.5L of eq. 6 omitted (This offset should be implemented at some point. It reflects that the origo is placed in middle of the rectangular building. The implementation also consider arbirtrary polygones for which the origo of the ellipse describing wake/cavity zones is placed on the middle connecting the points of widest projection across the wind).
 	
		#elif self.Cavity_marker[cell.index] != 0:
		
		#	Ax=LCr*np.sqrt((1-(x[2]/(Have))**2))
		#	Ay=WeffCW/2
		#	Xtran = (x[0]-center_r_X)
		#	Ytran = (x[1]-center_r_Y)
		#	Xwind = (x[0]-center_r_X)*cos(-windangle)-(x[1]-center_r_Y)*sin(-windangle) #rotated and translated system: along wind
		#	Ywind = (x[1]-center_r_Y)*cos(-windangle)+(x[0]-center_r_X)*sin(-windangle) #rotated and translated system: across wind
		#	Xshear=(Xwind)-(Ywind)*tan(np.pi/2-(shearangle-windangle)) 
		#	Yshear=Ywind
		#	dN = self.LCr*np.sqrt((1-(x[2]/self.Have)**2)*(1-(Yshear/Ay)**2)) # The downwind extension of the zone. eq. 6 Kaplan.
		#	Uh = -self.u0v0w0_exp(Xshear, Yshear, Have) #Wind at the roof level
		#	
		#	if dN == 0:
		#		values[:] = Uh
		#	elif dN != 0:
		#		# values[:] = Uh*(1-(Xshear)/dN) #Eq. 5 Kaplan
		#		values[:] = [0, 0, 0]
		#	else:
		#		print("what the heck cavity")

		# Least important are Wake zones. Cavity zones are also treated here! Cavities are a subset of points in Wakes. 
        	elif self.Wake_marker[cell.index] != 0:

        		Ax=LWr*np.sqrt((1-(x[2]/(Have))**2))
			Ay=WeffCW/2
			Xtran = (x[0]-center_r_X)
			Ytran = (x[1]-center_r_Y)
			Xwind = (x[0]-center_r_X)*cos(-windangle)-(x[1]-center_r_Y)*sin(-windangle) #rotated and translated system: along wind
			Ywind = (x[1]-center_r_Y)*cos(-windangle)+(x[0]-center_r_X)*sin(-windangle) #rotated and translated system: across wind
			Xshear=(Xwind)-(Ywind)*tan(np.pi/2-(shearangle-windangle)) 
			Yshear=Ywind
			dN = self.LCr*np.sqrt((1-(x[2]/self.Have)**2)*(1-((Yshear)/Ay)**2)) # The downwind extension of the zone. eq. 6 Kaplan.
			Uh = -self.u0v0w0_exp(Xshear, Yshear, Have) #Wind at the roof level
			
			if Xshear < dN: # This is the condition for a cavity
				if dN == 0:
					values[:] = Uh
				else:
					values[:] = Uh*(1-(Xshear)/dN) #This is a cavity. Eq. 5 Kaplan

			elif Xshear > dN: # Now we are in the wake zone.
				values[:] = self.u0v0w0_exp(x)*(1-(dN/Xshear)**1.5) #Eq. 7 Kaplan & Dinar
				#values[:] = [0, 0, 1]
			else:
				print("what the heck wake")


		# If not inside any zone then it should be just the inflow profile.
		elif  self.Wake_marker[cell.index] == 0 and self.Displacement_marker_along[cell.index] == 0 and self.Displacement_marker_opposite[cell.index] == 0 and self.Cavity_marker[cell.index] == 0:
        		values[:] =  self.u0v0w0_exp(x)
			#values[:] = [0, 0, 0]
		else: 
			print("Something is wrong")

	def value_shape(self): return (3, ) 

print("Start assign initial wind field")
b = Initfield(Wake_marker, Cavity_marker, Displacement_marker_along, Displacement_marker_opposite, u0v0w0_exp, w0_exp, u0v0w0_abs_exp, edgeangle, center_r_X, center_r_Y, Leff, Weff, LCr, LWr, dai_to_i, doi_to_i, wi_to_i, ci_to_i, windangle, Have, WeffCW, degree=degree)
u0v0w0 = project(b, VFS)

	
#### Define SubDomains for boundaries and mark
print("Define boundaries and mark")

boundary_markers = FacetFunction('size_t',mesh) #FacetFunction is a subclass of MeshFunction
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.

class MaxX(SubDomain): 

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], max(V_dof_x), tol)

bx1 = MaxX()
bx1.mark(boundary_markers, 1)	



class MinX(SubDomain): 

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], min(V_dof_x), tol)

bx2 = MinX()
bx2.mark(boundary_markers, 2)	

class MaxY(SubDomain):

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[1], max(V_dof_y), tol)

bx3 = MaxY()
bx3.mark(boundary_markers, 3)	



class MinY(SubDomain): 

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[1], min(V_dof_y), tol)

bx4 = MinY()
bx4.mark(boundary_markers, 4)

class MaxZ(SubDomain): 

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[2], max(V_dof_z), tol)

bx5 = MinY()
bx5.mark(boundary_markers, 5)


#plot(boundary_markers, interactive=True, title="Boundary Markers")
#plot(u0v0w0, mesh=mesh, interactive=True, title="Initial wind field with wakes")
#plot(div(u0v0w0),interactive=True, title="div(u0v0w0)")

#File("u0v0w0.pvd") << u0v0w0
#File("boundary_markers.pvd") << boundary_markers


### Implement the boundary conditions	
print("Implement boundary conditions")

alpha = Constant(1.0)
n=FacetNormal(mesh)
neu_bc=2*alpha*alpha*dot(u0v0w0_exp, n)

# Now is the domain not aligned with the wind direction. Therefore I set Dirichlet=0 (wind is free to vary) on the TWO outflow boundaries. 
# Neumann = 0 is unchanged wind. Dirichlet = 0 is free wind. Neumann = neu_bc is wind is free to slip along the boundary but not go through.

boundary_conditions =  {0: {'Neumann': neu_bc},  # Buildings
			1: {'Dirichlet': 0}, # Spacedomain; MaxX - outflow for Ux,Uy>0
			2: {'Neumann': 0},	# Spacedomains: MinX - inflow for Ux,Uy>0 
			3: {'Dirichlet': 0},   # Spacedomains: MaxY - outflow for Ux,Uy>0
			4: {'Neumann': 0},	# Spacedomain: MinY - inflow for Ux,Uy>0
			5: {'Neumann': 0}}	# Spacedomain: MaxZ (top)

# Define variational problem
LagrangeMultiplier = TrialFunction(V)
testv = TestFunction(V)

#Redefine the measure "ds" in terms of our boundary markers
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)
#dx = Measure('dx', domain=mesh, subdomain_data=wake_markers) #is this needed?? Should not be needed unless I want to integrate over different parts of the domain

# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are none Robin conditions. 
integrals_N = []
for i in boundary_conditions:
	if 'Neumann' in boundary_conditions[i]:
		if boundary_conditions[i]['Neumann'] != 0:
			g = boundary_conditions[i]['Neumann']
			integrals_N.append(g*testv*ds(i))

# Collect Dirichlet boundary conditions
bcs = []
for i in boundary_conditions:
	if 'Dirichlet' in boundary_conditions[i]:
		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
		bcs.append(bc)

# Continue defining variational problem
a = dot(grad(LagrangeMultiplier), grad(testv))*dx
L = div(u0v0w0)*testv*dx-sum(integrals_N)

# Compute solution
print("Compute solution")
LagrangeMultiplier = Function(V)
solve(a == L, LagrangeMultiplier, bcs)

# Plot solution
LagrangeMultiplier.rename('LagrangeMultiplier', 'LagrangeMultiplier_solution')
#plot(LagrangeMultiplier, wireframe=True, interactive=True)

#### Find the winddata

uvw = Function(VFS)

# Correction from initial field to final field

LMdx = Function(V)
LMdx = project(LagrangeMultiplier.dx(0), V)
LMdy = Function(V)
LMdy = project(LagrangeMultiplier.dx(1), V)
LMdz = Function(V)
LMdz = project(LagrangeMultiplier.dx(2), V)
LM_derivatives = Function(VFS)
assign(LM_derivatives.sub(0), LMdx)
assign(LM_derivatives.sub(1), LMdx)
assign(LM_derivatives.sub(2), LMdx)

# The final field
#uvw = u0v0w0+(1/(2*alpha**2))*LM_derivatives
uvw = project(u0v0w0+(1/(2*alpha**2))*LM_derivatives, VFS)

#plot(u0v0w0, wireframe=True, interactive=True, title="Initial windfield")
#plot((1/(2*alpha**2))*LM_derivatives, wireframe=True, interactive=True, title="Correction windfield")
#plot(uvw, wireframe=True, interactive=True, title="Final windfield")
#plot(uvw_2, wireframe=True, interactive=True, title="Final windfield_2")
#Correction = project(1/((2*alpha**2)*LM_derivatives), VFS)
#Correction = (1/(2*alpha**2))*LM_derivatives
#File("Correction.pvd") << Correction
#File("uvw.pvd") << uvw
exit()

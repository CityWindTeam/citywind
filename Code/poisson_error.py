from fenics import  *
#import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
N = 5;

for i in range(1,N):
	if i > 1:
		uold = u
	NN = 5*pow(2,i);
	h = 1.0/NN
	if i == 1:
		h_vector = np.array([h])
	else :
		h_vector = np.append(h_vector,[h])


	mesh = UnitSquareMesh(NN,NN)
	V = FunctionSpace(mesh,"P", 4)

	# Define the boundary condition, degree = ???
	#u_D = Expression("1 + x[0]*x[0] + 2*x[1]*x[1]",degree = 2)
	u_D = Constant(0.0)
	u_ref = Expression("sin(pi*x[0])*sin(pi*x[1])", degree = 6)
	# 
	def boundary(x,on_boundary):
		return on_boundary

	bc = DirichletBC(V,u_D,boundary)

	u = TrialFunction(V)
	v = TestFunction(V)
	#f = Constant(-6.0) #Expression("-6",degree = 0)
	f = Expression("2*pi*pi*sin(pi*x[0])*sin(pi*x[1])",degree = 6)
	a = dot(grad(u),grad(v))*dx
	L = f*v*dx


	u = Function(V)
	solve(a == L, u, bc)

	#vtkfile = File("vtkmapp/solution.pvd")
	#vtkfile << u 
	if i > 1:
		error_L2 = errornorm(u,uold,"L2",3)
		if i == 2:
			error_vector = np.array([error_L2])
		else:	
			error_vector = np.append(error_vector,[error_L2])


plt.loglog(h_vector[0:-1],error_vector)

plt.savefig("testfig.png")
np.savetxt('txt_h.txt', h_vector, delimiter='newline')
np.savetxt('txt_error.txt', error_vector, delimiter='newline')

print("error_L2 = ", error_L2)
print("h = ",h_vector)
print("error_vector = ", error_vector)


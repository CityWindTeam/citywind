from dolfin import *

ksp_setup_by_petsc = True

mesh = UnitSquareMesh(32, 32)

V = FunctionSpace(mesh, 'CG', 1)
u = TrialFunction(V)
v = TestFunction(V)

bc = DirichletBC(V, Constant(0), 'on_boundary')

a = inner(grad(u), grad(v))*dx
f = Constant(1)
L = inner(f, v)*dx

A, b = assemble_system(a, L, bc)
# Here the type of A, b is GenericMatrix, GenericVector
# Recast to PETSc types so that we can use it with PETSc
A = as_backend_type(A)
b = as_backend_type(b)
# Solver setup
if ksp_setup_by_petsc:
    solver = PETScKrylovSolver()
else:
    # Option1, do a lot on dolfin side
    solver = PETScKrylovSolver('cg', 'amg')
solver.set_operator(A)
# petsc4py pointer petsc object underlying solver
ksp = solver.ksp()
# Option one is to use command line options, see
# https://fenicsproject.org/qa/7633/how-to-set-petsc-options-from-command-line
# Alternatively we can work directly with petsc objects via petsc4py

from petsc4py import PETSc
# Setup of solver methods from petsc4py
if ksp_setup_by_petsc:
    ksp.setType(PETSc.KSP.Type.CG)
    # TODO: setup preconditioner

ksp.setTolerances(1E-12, 1E-12, max_it=2000)
ksp.setInitialGuessNonzero(True)

# Getting to vector for solution
uh = interpolate(Constant(1), V)
x = as_backend_type(uh.vector())
# Solve Ax = b, NOTE A is already in the solver rom .set_operator
niters = solver.solve(x, b)

print niters

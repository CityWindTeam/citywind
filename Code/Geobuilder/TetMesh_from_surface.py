# Use TetGen as mesh generator backend to preserve surface structures (default of TetGen "constrained DT") through FEniCS module mshr. Corresponding to TetGen switches -qm (quality, .mtr file)

from fenics import *
#from ipython import embed
from mshr import *
import numpy as np
#import pdb
#import matplotlib.pyplot
import matplotlib.pyplot as plt

#https://bitbucket.org/fenics-project/mshr/wiki/FAQ
# Load surface from file
# http://mypages.iit.edu/~asriva13/?page_id=586

mesh = Mesh("de_3008x3008x179,179557800293_x529075_y6469405.xml");

#mesh = UnitCubeMesh(1, 1, 1)

#domain = Surface3D("oklahoma_single_building.off")
#h_inv=4
#mesh = generate_mesh(domain,h_inv)

#geometry = Surface3D("de_3008x3008x179,179557800293_x529075_y6469405.obj")  # Which file format ?? .off, .obj, .smesh ?
#geometry = Surface3D("oklahoma_single_building.off")  # Which file format ?? .off, .obj, .smesh ?
#domain = CSGCGALDomain3D(geometry)
#generator = mshr.cpp.TetgenMeshGenerator3D(domain)
#generator = TetgenMeshGenerator3D(domain)
#generator.parameters["preserve_surface"] = True
#generator.parameters["mesh_resolution"] = 10
#mesh = generator.generate()
#mesh = generator.generate(CSGCGALDomain3D(domain))

plot(mesh, title="TetMesh_from_surface")
plt.show()
pvd_file = File("TetMesh_from_surface.pvd")
pvd_file << mesh
input("Press Enter to continue ...")
exit()

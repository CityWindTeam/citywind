x = 0:0.05:1;
%% Bottom
[Y,X] = meshgrid(x);
% Gaussian
FWHM=0.2; % Full width at half maximum
amp=0.35;  % Peak height
x0=0.5;
y0=0.5;

Z=amp.*exp(-(((X-x0).^2)+(Y-y0).^2.)/FWHM^2); %The Gaussian
%Replace border of Gaussian by zeros to create a watertight mesh
Z(:,size(Z,1))=0;
Z(size(Z,1),:)=0;
Z(1,:)=0;
Z(:,1)=0;
stlwrite('Gaussian.stl',X,Y,Z,'mode','ascii','direction','Z-')
%% Side X=1
[Z,Y] = meshgrid(x);
X = ones(length(x),length(x));
stlwrite('Gaussian.stl',X,Y,Z,'mode','ascii','direction','X+')

%% Side X=0
[Y,Z] = meshgrid(x);
X = zeros(length(x),length(x));
stlwrite('Gaussian.stl',X,Y,Z,'mode','ascii','direction','X-')

%% Side Y=0
[X,Z] = meshgrid(x);
Y = zeros(length(x),length(x));
stlwrite('Gaussian.stl',X,Y,Z,'mode','ascii','direction','Y-')
%% Side Y=1
Y = ones(length(x),length(x));
[Z,X] = meshgrid(x);
stlwrite('Gaussian.stl',X,Y,Z,'mode','ascii','direction','Y+')
%% Side Z=1
[X,Y] = meshgrid(x);
Z = ones(length(x),length(x));
stlwrite('Gaussian.stl',X,Y,Z,'mode','ascii','direction','Z+')

% Z=amp.*exp(-(((X-x0).^2)+(Y-y0).^2.)/FWHM^2);
% stlwrite('Bottom.stl',X,Y,Z,'mode','ascii','direction','Z-')
% %% Side X=1
% [Z,Y] = meshgrid(x);
% X = ones(length(x),length(x));
% stlwrite('X1.stl',X,Y,Z,'mode','ascii','direction','X+')
% 
% %% Side X=0
% [Y,Z] = meshgrid(x);
% X = zeros(length(x),length(x));
% stlwrite('X0.stl',X,Y,Z,'mode','ascii','direction','X-')
% 
% %% Side Y=0
% [X,Z] = meshgrid(x);
% Y = zeros(length(x),length(x));
% stlwrite('Y0.stl',X,Y,Z,'mode','ascii','direction','Y-')
% %% Side Y=1
% Y = ones(length(x),length(x));
% [Z,X] = meshgrid(x);
% stlwrite('Y1.stl',X,Y,Z,'mode','ascii','direction','Y+')
% %% Side Z=1
% [X,Y] = meshgrid(x);
% Z = ones(length(x),length(x));
% stlwrite('Z1.stl',X,Y,Z,'mode','ascii','direction','Z+')
''' Built on SUMMIT_energyadaptive.py.  Will implicitly add a neumann boundary condition in the weak formulation. This is an attempt to remedy that the factors alph, beta, gamma shall be able to take different values. Changed the variational form definition, added tensor definition for the anisotropic weights (alpha, beta, gamma), and the retrieval of final windfield based on anisotropic weights'''

from dolfin import *
from IPython import embed
import pdb
from mshr import *
import numpy as np
parameters["refinement_algorithm"] = "plaza_with_parent_facets"
import matplotlib.pyplot as plt
import meshio as meshio
from numpy import zeros

#### Load/Import initial mesh
# mesh = meshio.read("de_3008x3008x179,179557800293_x529075_y6469405.1.vtk")
#mesh = Mesh("de_3008x3008x179,179557800293_x529075_y6469405.xml");
mesh = Mesh("Gaussian.xml");
#mesh = UnitCubeMesh(2,2,2);
#box = Box(Point(-1,-1,-1), Point(1,1,1));
#box = Box(Point(-1,-1,-1), Point(2,2,2));
#box = Box(Point(1,1,1), Point(2,2,2));
#mesh = generate_mesh(box,2);

# Parameters for functionspace / vectorfunctionspace. Derived windfield is vectorvalued and Lagrange multplier is scalar
typeelement='CG'
degreeelement_V=2
degreeelement_VFS=2


#### Setup cpp expression for initial windfield u0v0w0
print("Setup initial windfield u0v0w0")
# This data is valid for Oklahoma City according to Jan B. 
y0=0.6
Ux=1.97 # at y=10m y is vertical	
Uz=2.82 # at y=10m
vK=0.4 # von Karman constant
v0_exp = 0.0 # initial wind vertical
alpha = 3.
beta = 3.
gamma = 1.
nothing = 0.

cpp_code = '''
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <math.h>
namespace py = pybind11;
#include <vector>
#include <dolfin/function/Expression.h>
#include <dolfin/function/Constant.h>


class AnisTensor2D : public dolfin::Expression
{
public:
  AnisTensor2D() :
      Expression(3,3),
      alpha(3.),
      beta(3.),
      gamma(1.),
      nothing(0)
      {
      }
      
void eval(Eigen::Ref<Eigen::VectorXd> values, Eigen::Ref<const Eigen::VectorXd> x) const
  {
  
     values[0] = alpha;
     values[1] = nothing;
     values[2] = nothing;
     values[3] = nothing;
     values[4] = beta;
     values[5] = nothing;
     values[6] = nothing;
     values[7] = nothing;
     values[8] = gamma;

  }
  
  void set(double _alpha, double _beta, double _gamma, double _nothing)
  {
  gamma = _gamma;
  beta = _beta;
  alpha  = _alpha;
  nothing = _nothing;
  }
  
private:
  double nothing;
  double gamma;
  double beta;
  double alpha;
  
};



class K : public dolfin::Expression
{
public:
  K() :
      Expression(3),
      y0(0.6),
      Ux(1.97),	
      Uz(2.82),
      vK(0.4),
      v0_exp(0.)
      {}
      
void eval(Eigen::Ref<Eigen::VectorXd> values, Eigen::Ref<const Eigen::VectorXd> x) const
  {

     values [0] = ((Ux*vK)/log(10/y0))/vK*log((x[2]+y0)/y0);
     //values [1] = v0_exp;
     values [1] = ((Uz*vK)/log(10/y0))/vK*log((x[2]+y0)/y0);
     values [2] = v0_exp;
  }
  
  void set(double _y0, double _Ux, double _Uz, double _vK, double _v0_exp)
  {
  y0 = _y0;
  Ux = _Ux;	
  Uz = _Uz;
  vK = _vK;
  v0_exp = _v0_exp;
  }
  
private:
  double y0;
  double Ux;
  double Uz;
  double vK;
  double v0_exp;
};
    
PYBIND11_MODULE(SIGNATURE, m)
    {
    py::class_<AnisTensor2D, std::shared_ptr<AnisTensor2D>, dolfin::Expression>
    (m, "AnisTensor2D")
    .def(py::init<>())
    .def("set", &AnisTensor2D::set);


    py::class_<K, std::shared_ptr<K>, dolfin::Expression>
    (m, "K")
    .def(py::init<>())
    .def("set", &K::set);
    
    }
'''

ExpressionModule = compile_cpp_code(cpp_code)

# Allocate python expression
b = CompiledExpression(ExpressionModule.K(), degree = 2)
b.set(y0, Ux, Uz, vK, v0_exp)

A = CompiledExpression(ExpressionModule.AnisTensor2D(), degree = 2)
A.set(alpha, beta, gamma, nothing)

########### SOLVE - ESTIMATE - MARK - REFINE loop ############

# Stop when sum of eta_T**2 < tolerance**2 or max_iterations is reached 
tolerance = 0.001
print("Set tolerance = ", tolerance)
max_iterations = 1 

for j in range(max_iterations): 
	# *** SOLVE step ***
	# Setup FunctionSpace and VectorFunctionSpace
	V = FunctionSpace(mesh, typeelement, degreeelement_V)
	VFS = VectorFunctionSpace(mesh, typeelement, degreeelement_VFS, dim=3)
	#TFS = TensorFunctionSpace(mesh, "CG", 1)	
	ndim = V.dim()
	d = mesh.geometry().dim()
	V_dof_coordinates = V.tabulate_dof_coordinates()
	V_dof_coordinates.resize((ndim, d))
	V_dof_x = V_dof_coordinates[:, 0]
	V_dof_y = V_dof_coordinates[:, 1]
	V_dof_z = V_dof_coordinates[:, 2] 
	u0v0w0 = interpolate(b, VFS) #cpp code technique / real initial wind
	# u0v0w0 = interpolate(u0v0w0_exp, VFS) # Expression technique / made up initial wind
	# Define SubDomains for boundaries and mark
	# File("Gaussian_u0v0w0.pvd") << u0v0w0

	boundary_markers = MeshFunction('size_t',mesh,dim=2)
	boundary_markers.set_all(0)  
	MaxX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_x)).mark(boundary_markers, 1)
	MinX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_x)).mark(boundary_markers, 2)
	MaxY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_y)).mark(boundary_markers, 3)
	MinY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_y)).mark(boundary_markers, 4)
	MaxZ = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_z)).mark(boundary_markers, 5)
	#File("BoundaryMarkers.pvd") << boundary_markers
	alpha_s = Constant(1.0)
	n=FacetNormal(mesh)
	neu_bc=2*alpha_s*alpha_s*dot(u0v0w0, n)


	#Homogenous Neumann (= 0) is unchanged windfield. Inflow boundary and possible outflow if enough far away from buidlings.
	#Inhomogenous Neumann (~=0) is in requires the final field to be in the plane of the boundary. Wind is Free-slip.
	#Homogenous Dirichlet (= 0) is wind can vary freely. Outflow boundary.
	# Now the implicit Neumann BC in the added term in the weak formulation can be over the whole or part of the boundary. Therefore 	the part of the boundary that the explicit implementation of BC must/should be adjusted accordingly, I suppose?. 

	#The testfunction testv is required to vanish on the part of the boundary where the solution is known (see page 3 in the book). If the 		solution is not known on some part of the boundary (the neumann boundary) this is implemented as neumann boundary conditions. The 		added implicit definition of Neumann condition is applied only to the part of the boundary where the ambiguity arises, i.e. the 	ground.  

	'''boundary_conditions =  {0: {'Neumann': neu_bc},  # MinZ. / Ground
				1: {'Dirichlet': 0}, # MaxX
				2: {'Neumann': 0},	# MinX
				3: {'Dirichlet': 0},   # MaxY
				4: {'Neumann': 0},	# MinY
				5: {'Neumann': 0}}	# MaxZ'''
	
	boundary_conditions =  {0: {'Neumann': neu_bc},  # MinZ. / Ground
				1: {'Dirichlet': 0}, # MaxX
				2: {'Neumann': 0},	# MinX
				3: {'Dirichlet': 0},   # MaxY
				4: {'Neumann': 0},	# MinY
				5: {'Neumann': 0}}	# MaxZ

	#Redefine the measure "ds" in terms of our boundary markers
	ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)
	# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be 		collected. There are none Robin conditions. 
	testv = TestFunction(V)
	integrals_N = []
	for i in boundary_conditions:
		if 'Neumann' in boundary_conditions[i]:
			if boundary_conditions[i]['Neumann'] != 0:
				g = boundary_conditions[i]['Neumann']
				integrals_N.append(g*testv*ds(i))
	
	# Collect Dirichlet boundary conditions
	bcs = []
	for i in boundary_conditions:
		if 'Dirichlet' in boundary_conditions[i]:
			bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
			bcs.append(bc)
	
	# Define variational problem
	#V = FunctionSpace(mesh, typeelement, degreeelement_V)
	#testv = TestFunction(V)
	#Aint = interpolate(A, TFS)
	LagrangeMultiplier = TrialFunction(V)
	#n=FacetNormal(mesh)
	C=1e10 # A large number forcing the added implicit addition of neumann boundary to zero 
	#pdb.set_trace()
	#C*dot(A*grad(LagrangeMultiplier),n)*ds(0) # The added term
	
	## The standard variational form definition
	a = dot(grad(LagrangeMultiplier), grad(testv))*dx 
	#L = div(u0v0w0)*testv*dx-sum(integrals_N) 

	#a = inner(C*grad(LagrangeMultiplier), grad(testv))*dx + C*dot(A*grad(LagrangeMultiplier),n)*testv*ds(0) 
	L = div(u0v0w0)*testv*dx-sum(integrals_N)

	'''# Define variational problem
	
	LagrangeMultiplier = TrialFunction(V)
	a = dot(grad(LagrangeMultiplier), grad(testv))*dx
	L = div(u0v0w0)*testv*dx-sum(integrals_N)
	'''
	
	# Define function for the solution
	LagrangeMultiplier = Function(V)
	
	# Solve on current mesh using linear solver 
	solve(a == L, LagrangeMultiplier, bcs)
	# *** ESTIMATE step ***
	
	# Define cell and edge residuals 
	R_T = div(u0v0w0) + div(grad(LagrangeMultiplier)) 
	# get the normal to the cells 
	# n = FacetNormal(mesh) 
	R_dT = 2*avg(dot(grad(LagrangeMultiplier), n)) 
	# Use space of piecewise constants to localize indicator form 
	Constants = FunctionSpace(mesh, "DG", 0) 
	w = TestFunction(Constants) 
	h = CellDiameter(mesh) 
	# Assemble squared error indicators, eta_T^2, and store into a numpy array 
	eta2 = assemble(h**2 * R_T**2 * w * dx + avg(h) * R_dT**2 * avg(w) * dS) 
	# dS is integral over interior edges only 
	eta2 = eta2.get_local() 
	# Compute maximum and sum (the estimate for squared H1 norm of error) 
	eta2_max = max(eta2) 
	sum_eta2 = sum(eta2) 
	
	# Stop if error estimate is less than tolerance 
	if sum_eta2 < tolerance**2: 
		print("Final mesh %g: %d tetrahedra, %d vertices, hmax = %g, hmin = %g, errest = %g" % (j+1, mesh.num_cells(), 			mesh.num_vertices(), mesh.hmax(), mesh.hmin(), sqrt(sum_eta2))) 
		print("\nTolerance = %g: achieved. Exiting. Error estimate = %g" % (tolerance, sqrt(sum_eta2))) 
		break 
	# *** MARK step ***
	# Mark cells for refinement for which eta > frac eta_max for frac = .95, .90, ...; 
	# choose frac so that marked elements account for a given part of total error 
	frac = .95 
	delfrac = .05 
	part = .25 
	marked = zeros(eta2.size, dtype='bool') 
	# marked starts as False for all elements 
	sum_marked_eta2 = 0. 
	# sum over marked elements of squared error indicators 
	while sum_marked_eta2 < part*sum_eta2: 
		new_marked = (~marked) & (eta2 > frac*eta2_max) 
		sum_marked_eta2 += sum(eta2[new_marked]) 
		marked += new_marked 
		frac -= delfrac 
	# attach Boolean array marked to a cell function 
	cells_marked = MeshFunction("bool", mesh, 3) 
	cells_marked.array()[:] = marked 
	# embed()
	# *** REFINE step ***
	print("Mesh %g: %d triangles, %d vertices, hmax = %g, hmin = %g, errest = %g" % (j, mesh.num_cells(), mesh.num_vertices(), 		mesh.hmax(), mesh.hmin(), sqrt(sum_eta2)))
	#pdb.set_trace()
	mesh = refine(mesh, cells_marked)
	#File("mesh_de %g.pvd" % (j)) << mesh
	#plt.figure(1) 
	#plot(mesh, title="Mesh q" + str(j))
	#plt.show()



############### Find the winddata on final mesh ##################### 
uvw = Function(VFS)

print("Find the winddata from the sought Lagrange Multiplier")
#print("project partial derivatives Lagrange Multiplier to retrieve final wind; by solving inner(TrialFunction(VFS), TestFunction(VFS))*dx = inner(TestFunction(VFS), grad(LagrangeMultiplier))*dx")

# This method, by solving the projection problem with "dolfin functionality", seems not to work with the latest release of fenics/dolfin that uses pybind11 wrappers.
# https://fenicsproject.org/qa/1425/derivatives-at-the-quadrature-points?show=1425#q1425

uvfs = TrialFunction(VFS)
vvfs = TestFunction(VFS)

# The mass matrix is diagonal. Get the diagonal 
lhs = assemble(inner(uvfs, vvfs)*dx)
ones = Function(VFS)
ones.vector()[:] = 1.
lhs_d = lhs*ones.vector()
# If you only want to assemble right hand side once:
#rhs = assemble(inner(vvfs, (u0v0w0+(1/(2*A**2))*grad(LagrangeMultiplier)))*dx)
pdb.set_trace()
rhs = assemble(inner(vvfs, (u0v0w0+(1/(2*alpha_s**2))*grad(LagrangeMultiplier)))*dx)
# solve the projection problem
uvw = Function(VFS)
#embed()
uvw.vector().set_local(rhs.get_local() / lhs_d.get_local())

# Correction = Function(VFS)
# Correction = uvw.vector().get_local - u0v0w0.vector().get_local
# Correction = uvw.vector() - u0v0w0.vector()
# Correction = uvw - u0v0w0
# embed()

######## Visualize #########
# mesh_leaf = mesh.leaf_node()
# plt.figure(1)
# plot(mesh, title="Initial mesh")
# plot(mesh.leaf_node(), title="leaf_node mesh")
# plot(mesh.child(), title="Child mesh")
# plot(mesh.parent(), title="Parent mesh")

#plt.figure(2)
#plot(LagrangeMultiplier.root_node(), title="LagrangeMultiplier on initial mesh .root_node()")
#plt.figure(3)
#plot(LagrangeMultiplier.leaf_node(), title="LagrangeMultiplier on final mesh .leaf_node()")
#plt.figure(4)
#plot(LagrangeMultiplier.child(), title="LagrangeMultiplier on child")
#plt.show()
# input("Press Enter to continue ...")

#plt.figure(1)
#plot(u0v0w0, title="Initial windfield")
#plt.figure(2)
#plot(LagrangeMultiplier, title="LagrangeMultiplier")
#plt.show()
# plt.figure(2)
# plot(div(u0v0w0), title="Divergence of the initial wind field")
# plt.figure(3)
# plot(uvw, title="Final windfield")
# exit()
# plt.show()
#plt.figure(4)
#plot(LagrangeMultiplier, title="LagrangeMultiplier")
#plt.show()
#plt.figure(5)
#plot(Correction, title="Correction from initial to final wind field")
#cbar = plt.fig(3).colorbar(cax, ticks=[-1, 0, 1])
#ax = plt.subplots()
#cax = ax.imshow(data, interpolation='nearest', cmap=cm.afmhot)

#plt.show()
#plt.figure(6)
#plt.clf
#plot(div(uvw), title="Divergence of the final wind field")
#plt.show()


######## Write to file ##########


# File("mesh.pvd") << mesh
# File("mesh_child.pvd") << mesh.child()
# File("mesh_parent.pvd") << mesh.parent()
#File("u0v0w0.pvd") << u0v0w0
#File("boundary_markers.pvd") << boundary_markers
File("uvw_gaussian_XYin_XoutYout_free_implicit.pvd") << uvw
# File("u0v0w0_de_adapt.pvd") << u0v0w0
# File("LagrangeMultiplier_de_adapt.pvd") << LagrangeMultiplier
#input("Press Enter to continue ...")
exit()

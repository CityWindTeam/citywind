# This should be used in the implementation, it solves the Sherman & Rockle equation. Geometry is based on .off or .obj (if .obj file reader is present in mshr). 

from dolfin import *
#from IPython import embed
from mshr import *
import numpy as np

#### Load/Import mesh
domain = Surface3D("oklahoma_single_building.off")
#domain = Surface3D("obj/de_16x16x1000_x529075_y6469405.obj")
#domain = Surface3D("obj/de_3008x3008x100_x529075_y6469405.obj")
#domain = Surface3D("obj/de_3008x3008x1000_x529075_y6469405.obj")

h_inv=4
mesh = generate_mesh(domain,h_inv)
#mesh = Mesh("de_3008x3008x179,179557800293_x529075_y6469405.xml");
# Create functionspace
typeelement='CG'
degreeelement_V=2
degreeelement_VFS=2
V = FunctionSpace(mesh, typeelement, degreeelement_V)
VFS = VectorFunctionSpace(mesh, typeelement, degreeelement_VFS, dim=3)

ndim = V.dim()
d = mesh.geometry().dim()
V_dof_coordinates = V.tabulate_dof_coordinates()
V_dof_coordinates.resize((ndim, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]

#### Setup initial windfield u0v0w0
print("Setup initial windfield u0v0w0")
# This data is valid for Oklahoma City according to Jan B. 
z0=0.6
Ux=1.97 # at z=10m	
Uy=2.82 # at z=10m
vK=0.4 # von Karman constant

Ustar=(np.sqrt(Ux*Ux+Uy*Uy)*vK)/np.log(10/z0)
Uxstar=(Ux*vK)/np.log(10/z0)
Uystar=(Uy*vK)/np.log(10/z0)

w0_exp_f = Constant(0.0)
w0_exp = 0.0
degree = 5
# Redfine constants so FEniCS understands
Ustar_f=Constant(Ustar)
Uxstar_f=Constant(Uxstar)
Uystar_f=Constant(Uystar)
vK_f = Constant(vK)
z0_f=Constant(z0) 

u0v0w0_exp = Expression(("x[2] > 0 ? Uxstar/vK*log((x[2]+z0)/z0) : 0","x[2] > 0 ? Uystar/vK*log((x[2]+z0)/z0) : 0", "w0_exp"), Uxstar=Uxstar_f, Uystar=Uystar_f, w0_exp=w0_exp_f, z0=z0_f, vK = vK_f, degree = degree)

cppcode = """
class K : public Expression
{
public:
K() : Expression(3) {}

void eval(Array<double>& values,
const Array<double>& x,
const ufc::cell& cell) const
{
values [0] = Uxstar/vK*log((x[2]+z0)/z0);
values [1] = Uystar/vK*log((x[2]+z0)/z0);
values [2] = w0_exp;
}

double vK;
double Uxstar;
double Uystar;
double w0_exp;
double z0;

};
"""
b = Expression(cppcode=cppcode, degree=degree)
b.z0 = z0
b.Uxstar = Uxstar
b.Uystar = Uystar
b.w0_exp = w0_exp
b.vK = vK

u0v0w0 = interpolate(b, VFS)

#### Define SubDomains for boundaries and mark

boundary_markers = FacetFunction('size_t',mesh) #FacetFunction is a subclass of MeshFunction
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.
MaxX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_x)).mark(boundary_markers, 1)
MinX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_x)).mark(boundary_markers, 2)
MaxY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_y)).mark(boundary_markers, 3)
MinY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_y)).mark(boundary_markers, 4)
MaxZ = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_z)).mark(boundary_markers, 5)

alpha = Constant(1.0)
n=FacetNormal(mesh)
neu_bc=2*alpha*alpha*dot(u0v0w0, n)
 
boundary_conditions =  {0: {'Neumann': neu_bc},  # MinZ. / Building&Ground
			1: {'Dirichlet': 0}, # MaxX
			2: {'Neumann': 0},	# MinX
			3: {'Dirichlet': 0},   # MaxY
			4: {'Neumann': 0},	# MinY
			5: {'Neumann': 0}}	# MaxZ

#Redefine the measure "ds" in terms of our boundary markers
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)

# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are nonevK Robin conditions. 
testv = TestFunction(V)
integrals_N = []
for i in boundary_conditions:
	if 'Neumann' in boundary_conditions[i]:
		if boundary_conditions[i]['Neumann'] != 0:
			g = boundary_conditions[i]['Neumann']
			integrals_N.append(g*testv*ds(i))

# Collect Dirichlet boundary conditions
bcs = []
for i in boundary_conditions:
	if 'Dirichlet' in boundary_conditions[i]:
		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
		bcs.append(bc)


# Define variational problem
LagrangeMultiplier = TrialFunction(V)
a = dot(grad(LagrangeMultiplier), grad(testv))*dx
L = div(u0v0w0)*testv*dx-sum(integrals_N)

# Define function for the solution
LagrangeMultiplier = Function(V)
solve(a == L, LagrangeMultiplier, bcs)

#### Find the winddata on the refined mesh

print("Find the winddata")
print("project partial derivatives Lagrange Multiplier to retrieve final wind; by solving inner(TrialFunction(VFS), TestFunction(VFS))*dx = inner(TestFunction(VFS), grad(LagrangeMultiplier))*dx")
# https://fenicsproject.org/qa/1425/derivatives-at-the-quadrature-points?show=1425#q1425

uvfs = TrialFunction(VFS)
vvfs = TestFunction(VFS)

# The mass matrix is diagonal. Get the diagonal 
lhs = assemble(inner(uvfs, vvfs)*dx)
ones = Function(VFS)
ones.vector()[:] = 1.
lhs_d = lhs*ones.vector()
# If you only want to assemble right hand side once:
rhs = assemble(inner(vvfs, (u0v0w0+(1/(2*alpha**2))*grad(LagrangeMultiplier)))*dx)

# solve the projection problem
uvw = Function(VFS)
uvw.vector().set_local(rhs.array() / lhs_d.array())
plot(uvw, wireframe=True, interactive=True, title="Final windfield")
interactive()

## Visualize
#plot(boundary_markers, interactive=True, title="Boundary Markers")
#LagrangeMultiplier.rename('LagrangeMultiplier', 'LagrangeMultiplier_solution')
#plot(LagrangeMultiplier, wireframe=True, interactive=True)
#plot(div(u0v0w0), wireframe=True, interactive=True, title="Divergence of the initial wind field")
#plot(div(uvw), wireframe=True, interactive=True, title="Divergence of the final wind field")
#plot(u0v0w0, wireframe=True, interactive=True, title="Initial windfield")
#plot(uvw, wireframe=True, interactive=True, title="Final windfield")
#plot(mesh, interactive=True)

## Write to file
#File("mesh.pvd") << mesh
#File("u0v0w0.pvd") << u0v0w0
#File("boundary_markers.pvd") << boundary_markers
File("uvw.pvd") << uvw

exit()



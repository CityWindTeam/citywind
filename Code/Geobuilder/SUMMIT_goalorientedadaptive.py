# Testing part of SUMMIT_2.py to debug compilation error: Projection problem resolved by using get_local after post question on AllAnswered.  
# Added adaptive solver: Need to specify correct goal functional! Trying Laplacian of Lagrange multiplier. Need to solve visualization of parent/child meshes.  
# Also need to add correct initial wind field: Solved by Hippylib example from FEniCS Slack forum.
# Solution on de_ mesh (created in TetGen output as MEDIT .mesh format and converted with dolfin-convert) is -nan for unknown reason. UnitCubeMesh works so I believe the fault is the mesh anyhow, although it visualizes properly in Paraview. I've been trying to convert the mesh from .vtk (.vtk output from TetGen) to dolfin-xml using meshio but can't get meshio to work. With pytest: "ImportError: No module named meshio" From terminal meshio-convert: AttributeError: module 'meshio' has no attribute 'input_filetypes. --- This problem arised because the coordinates dofs took negative values and upon setting initial windfield thorugh logarithmic function (which is only defined for positive values) An offest yields valid results.

from dolfin import *
from IPython import embed
from mshr import *
import numpy as np
parameters["refinement_algorithm"] = "plaza_with_parent_facets"
import matplotlib.pyplot as plt
import meshio as meshio

#### Load/Import mesh
# mesh = meshio.read("de_3008x3008x179,179557800293_x529075_y6469405.1.vtk")
mesh = Mesh("de_3008x3008x179,179557800293_x529075_y6469405.xml");
#mesh = UnitCubeMesh(2,2,2);
#box = Box(Point(-1,-1,-1), Point(1,1,1));
#box = Box(Point(0,0,0), Point(1,1,1));
#mesh = generate_mesh(box,2);
# Create functionspace
typeelement='CG'
degreeelement_V=2
degreeelement_VFS=2
V = FunctionSpace(mesh, typeelement, degreeelement_V)
VFS = VectorFunctionSpace(mesh, typeelement, degreeelement_VFS, dim=3)

ndim = V.dim()
d = mesh.geometry().dim()
V_dof_coordinates = V.tabulate_dof_coordinates()
V_dof_coordinates.resize((ndim, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]

#### Setup initial windfield u0v0w0
print("Setup initial windfield u0v0w0")
# This data is valid for Oklahoma City according to Jan B. 
y0=0.6
Ux=1.97 # at y=10m y is vertical	
Uz=2.82 # at y=10m
vK=0.4 # von Karman constant
v0_exp = 0.0 # initial wind vertical

cpp_code = '''
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <math.h>
namespace py = pybind11;
#include <vector>
#include <dolfin/function/Expression.h>
#include <dolfin/function/Constant.h>

class K : public dolfin::Expression
{
public:
  K() :
      Expression(3),
      y0(0.6),
      Ux(1.97),	
      Uz(2.82),
      vK(0.4),
      v0_exp(0.)
      {}
      
void eval(Eigen::Ref<Eigen::VectorXd> values, Eigen::Ref<const Eigen::VectorXd> x) const
  {

     values [0] = ((Ux*vK)/log(10/y0))/vK*log((100+x[1]+y0)/y0);
     values [1] = v0_exp;
     values [2] = ((Uz*vK)/log(10/y0))/vK*log((100+x[1]+y0)/y0);
  }
  
  void set(double _y0, double _Ux, double _Uz, double _vK, double _v0_exp)
  {
  y0 = _y0;
  Ux = _Ux;	
  Uz = _Uz;
  vK = _vK;
  v0_exp = _v0_exp;
  }
  
private:
  double y0;
  double Ux;
  double Uz;
  double vK;
  double v0_exp;
};
    
PYBIND11_MODULE(SIGNATURE, m)
    {
    py::class_<K, std::shared_ptr<K>, dolfin::Expression>
    (m, "K")
    .def(py::init<>())
    .def("set", &K::set);
    
    }
'''

ExpressionModule = compile_cpp_code(cpp_code)

# Allocate python expression
b = CompiledExpression(ExpressionModule.K(), degree = 2)
b.set(y0, Ux, Uz, vK, v0_exp)
u0v0w0 = interpolate(b, VFS)

#plt.figure()
#plot(u0v0w0, title="Initial windfield")
#plt.show()
#input("Press Enter to continue ...")

#u0v0w0 = Expression(("cos(x[0])","sin(x[0])","sin(x[0])"),degree=2)
#u0v0w0 = interpolate(u0v0w0, VFS)
#### Define SubDomains for boundaries and mark

boundary_markers = MeshFunction('size_t',mesh,dim=2)
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.
MaxX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_x)).mark(boundary_markers, 1)
MinX = CompiledSubDomain('on_boundary && near(x[0], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_x)).mark(boundary_markers, 2)
MaxY = CompiledSubDomain('on_boundary && near(x[1], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_y)).mark(boundary_markers, 3)
MinZ = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=min(V_dof_z)).mark(boundary_markers, 4)
MaxZ = CompiledSubDomain('on_boundary && near(x[2], nearwhat, tol)', tol=1E-14, nearwhat=max(V_dof_z)).mark(boundary_markers, 5)

alpha = Constant(1.0)
n=FacetNormal(mesh)
neu_bc=2*alpha*alpha*dot(u0v0w0, n)

boundary_conditions =  {0: {'Neumann': neu_bc},  # MinY. / Ground
			1: {'Dirichlet': 0}, # MaxX
			2: {'Neumann': 0},	# MinX
			3: {'Neumann': 0},   # MaxY
			4: {'Neumann': 0},	# MinZ
			5: {'Dirichlet': 0}}	# MaxZ

#Redefine the measure "ds" in terms of our boundary markers
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)

# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are none Robin conditions. 
testv = TestFunction(V)
integrals_N = []
for i in boundary_conditions:
	if 'Neumann' in boundary_conditions[i]:
		if boundary_conditions[i]['Neumann'] != 0:
			g = boundary_conditions[i]['Neumann']
			integrals_N.append(g*testv*ds(i))

# Collect Dirichlet boundary conditions
bcs = []
for i in boundary_conditions:
	if 'Dirichlet' in boundary_conditions[i]:
		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
		bcs.append(bc)


# Define variational problem
LagrangeMultiplier = TrialFunction(V)
a = dot(grad(LagrangeMultiplier), grad(testv))*dx
L = div(u0v0w0)*testv*dx-sum(integrals_N)


# Define function for the solution
LagrangeMultiplier = Function(V)

## Solve using linear solver 
#solve(a == L, LagrangeMultiplier, bcs)

## Solve using adaptive solver
# Define goal functional (quantity of interest)
M = div(grad(LagrangeMultiplier))*dx()

# Define error tolerance
#tol = 0.01
# tol = 5.e6
# tol = 2.84e-16 #suitable for mesh = UnitCubeMesh(2,2,2); exiting after iterations no. 1
tol = 727 # # For mesh = Mesh("de_3008x3008x179,179557800293_x529075_y6469405.xml"); offset 100


# Solve equation a = L with respect to u and the given boundary
# conditions, such that the estimated error (measured in M) is less
# than tol
problem = LinearVariationalProblem(a, L, LagrangeMultiplier, bcs)
solver = AdaptiveLinearVariationalSolver(problem, M)
solver.parameters["error_control"]["dual_variational_solver"]["linear_solver"] = "gmres"
solver.parameters["error_control"]["dual_variational_solver"]["symmetric"] = True
solver.solve(tol)

solver.summary()


#### Find the winddata 
uvw = Function(VFS)

print("Find the winddata from the sought Lagrange Multiplier")
#print("project partial derivatives Lagrange Multiplier to retrieve final wind; by solving inner(TrialFunction(VFS), TestFunction(VFS))*dx = inner(TestFunction(VFS), grad(LagrangeMultiplier))*dx")

# This method, by solving the projection problem with "dolfin functionality", seems not to work with the latest release of fenics/dolfin that uses pybind11 wrappers.
# https://fenicsproject.org/qa/1425/derivatives-at-the-quadrature-points?show=1425#q1425

uvfs = TrialFunction(VFS)
vvfs = TestFunction(VFS)

# The mass matrix is diagonal. Get the diagonal 
lhs = assemble(inner(uvfs, vvfs)*dx)
ones = Function(VFS)
ones.vector()[:] = 1.
lhs_d = lhs*ones.vector()
# If you only want to assemble right hand side once:
rhs = assemble(inner(vvfs, (u0v0w0+(1/(2*alpha**2))*grad(LagrangeMultiplier)))*dx)

# solve the projection problem
uvw = Function(VFS)
#embed()
uvw.vector().set_local(rhs.get_local() / lhs_d.get_local())

# Correction = Function(VFS)
# Correction = uvw.vector().get_local - u0v0w0.vector().get_local
# Correction = uvw.vector() - u0v0w0.vector()
# Correction = uvw - u0v0w0
# embed()

######## Visualize #########
# mesh_leaf = mesh.leaf_node()
# plt.figure(1)
# plot(mesh, title="Initial mesh")
# plot(mesh.leaf_node(), title="leaf_node mesh")
# plot(mesh.child(), title="Child mesh")
# plot(mesh.parent(), title="Parent mesh")

#plt.figure(2)
#plot(LagrangeMultiplier.root_node(), title="LagrangeMultiplier on initial mesh .root_node()")
#plt.figure(3)
#plot(LagrangeMultiplier.leaf_node(), title="LagrangeMultiplier on final mesh .leaf_node()")
#plt.figure(4)
#plot(LagrangeMultiplier.child(), title="LagrangeMultiplier on child")
#plt.show()
# input("Press Enter to continue ...")

#plt.figure(1)
#plot(u0v0w0, title="Initial windfield")
#plt.figure(2)
#plot(LagrangeMultiplier, title="LagrangeMultiplier")
#plt.show()
# plt.figure(2)
# plot(div(u0v0w0), title="Divergence of the initial wind field")
# plt.figure(3)
# plot(uvw, title="Final windfield")
# exit()
# plt.show()
#plt.figure(4)
#plot(LagrangeMultiplier, title="LagrangeMultiplier")
#plt.show()
#plt.figure(5)
#plot(Correction, title="Correction from initial to final wind field")
#cbar = plt.fig(3).colorbar(cax, ticks=[-1, 0, 1])
#ax = plt.subplots()
#cax = ax.imshow(data, interpolation='nearest', cmap=cm.afmhot)

#plt.show()
#plt.figure(6)
#plt.clf
#plot(div(uvw), title="Divergence of the final wind field")
#plt.show()


######## Write to file ##########


# File("mesh.pvd") << mesh
# File("mesh_child.pvd") << mesh.child()
# File("mesh_parent.pvd") << mesh.parent()
#File("u0v0w0.pvd") << u0v0w0
#File("boundary_markers.pvd") << boundary_markers
File("uvw_de_adapt.pvd") << uvw
File("u0v0w0_de_adapt.pvd") << u0v0w0
File("LagrangeMultiplier_de_adapt.pvd") << LagrangeMultiplier
#input("Press Enter to continue ...")
exit()

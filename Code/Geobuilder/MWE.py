# https://fenicsproject.org/qa/1425/derivatives-at-the-quadrature-points/?show=1425#q1425
from dolfin import *

mesh = UnitSquareMesh(10,10)

Vq = VectorFunctionSpace(mesh, "CG", 1)
Vu = FunctionSpace(mesh, 'CG', 1)
u = interpolate(Expression("pow(x[0],2)", degree=2),Vu)
eps = grad(u)

# This is the result you're after
v0 = project(eps, Vq)

# Now we'll do it more efficiently
# You want to solve this variational form
# inner(uq, vq)*dx = inner(vq, grad(u))*dx
uq = TrialFunction(Vq)
vq = TestFunction(Vq)

# The mass matrix is diagonal. Get the diagonal 
M = assemble(inner(uq, vq)*dx)
ones = Function(Vq)
ones.vector()[:] = 1.
Md = M*ones.vector()

# If you only want to assemble right hand side once:
grad_eps = assemble(inner(vq, grad(u))*dx)

# For repeated projects it is much faster to compute the rhs 
# like this, where P can be preassembled just once
#P = assemble(inner(vq, grad(TrialFunction(Vu)))*dx)
#grad_eps = P*u.vector()

# solve
v1 = Function(Vq)
v1.vector().set_local(grad_eps.array() / Md.array())

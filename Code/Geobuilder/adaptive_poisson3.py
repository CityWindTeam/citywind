'''
This is 3D version of adaptive_poisson2.py in an attempt to understand why not SUMMIT_energyadaptive.py is not working. There seems to be some problem in error estimation step when going from 2D to 3D, the first cell has an enourmous error estimate.  
'''

""" Adaptive Poisson solver using a residual-based energy-norm error estimator 
eta_h**2 = sum_T eta_T**2 with 
eta_T**2 = h_T**2 ||R_T||_T**2 + 1/2 h_T ||R_dT||_dT**2 
where R_T = f + div grad uh R_dT = 2 avg(grad uh * n) 
(2*avg is jump, since n switches sign across edges) 
and a Dorfler marking strategy 
By Douglas Arnold building on code of Marie Rognes. Extended to 3-dimensions and updated to dolfin 2018.1.0 by Christian Lejon""" 
from fenics import * 
from mshr import * 
from numpy import zeros
import matplotlib.pyplot as plt
import pdb
 
# Stop when sum of eta_T**2 < tolerance**2 or max_iterations is reached 
tolerance = 0.05
print("Set tolerance = ", tolerance)
max_iterations = 6 
# Create initial mesh 
# mesh = RectangleMesh(Point(-2., -2.), Point(2., 2.), 12, 12)
box = Box(Point(-2,-2,-2), Point(2,2,2));
# box = Box(Point(0.1,0.1,0.1), Point(2,2,2));
mesh = generate_mesh(box,6);
 
# Dirichlet boundary condition and right hand side 
g = Constant(0.) 
# Right hand side: zero, except for on a thin spherein3D-annulus inside the domain 
class fex(UserExpression): 
	def eval(self, value, x): 
		r2 = x[0]*x[0] + x[1]*x[1] + x[2]*x[2]
		if (r2 > .9**2) and (r2 < 1.**2): 
			value[0] = 6. 
		else: 
			value[0] = 0.

 
f = fex(degree=5) 
# SOLVE - ESTIMATE - MARK - REFINE loop 
for i in range(max_iterations): 
# *** SOLVE step # Define variational problem and boundary condition 
# Solve variational problem on current mesh 
	V = FunctionSpace(mesh, "Lagrange", 2) 
	u = TrialFunction(V) 
	v = TestFunction(V) 
	a = inner(grad(u), grad(v))*dx 
	L = f*v*dx 
	uh = Function(V) 
	solve(a==L, uh, DirichletBC(V, g, DomainBoundary())) 
# *** ESTIMATE step 
# Define cell and edge residuals 
	R_T = f + div(grad(uh)) 
# get the normal to the cells 
	n = FacetNormal(mesh) 
	R_dT = 2*avg(dot(grad(uh), n)) 
# Use space of piecewise constants to localize indicator form 
	Constants = FunctionSpace(mesh, "DG", 0) 
	w = TestFunction(Constants) 
	h = CellDiameter(mesh) 
# Assemble squared error indicators, eta_T^2, and store into a numpy array 
	eta2 = assemble(h**2 * R_T**2 * w * dx + avg(h) * R_dT**2 * avg(w) * dS) 
# dS is integral over interior edges only 
	eta2 = eta2.get_local() 
# Compute maximum and sum (the estimate for squared H1 norm of error) 
	eta2_max = max(eta2) 
	sum_eta2 = sum(eta2)
	#pdb.set_trace()
# Stop if error estimate is less than tolerance 
	if sum_eta2 < tolerance**2: 
		print("Final mesh %g: %d triangles, %d vertices, hmax = %g, hmin = %g, errest = %g" % (i+1, mesh.num_cells(), 			mesh.num_vertices(), mesh.hmax(), mesh.hmin(), sqrt(sum_eta2))) 
		print("\nTolerance = %g achieved. Exiting. Error estimate = %g", tolerance, sqrt(sum_eta2)) 
		break 
	# *** MARK step 
	# Mark cells for refinement for which eta > frac eta_max for frac = .95, .90, ...; 
	# choose frac so that marked elements account for a given part of total error 
	frac = .95 
	delfrac = .05 
	part = .25 
	marked = zeros(eta2.size, dtype='bool')
	# marked starts as False for all elements 
	sum_marked_eta2 = 0. 
	# sum over marked elements of squared error indicators 
	while sum_marked_eta2 < part*sum_eta2: 
		new_marked = (~marked) & (eta2 > frac*eta2_max) 
		sum_marked_eta2 += sum(eta2[new_marked]) 
		marked += new_marked 
		frac -= delfrac 
	# attach Boolean array marked to a cell function 
	cells_marked = MeshFunction("bool", mesh, 3)
	cells_marked.array()[:] = marked 
	# *** REFINE step 
	mesh = refine(mesh, cells_marked)
	File("mesh %g.pvd" % (i)) << mesh
	#plt.figure(1) 
	#plot(mesh, title="Mesh q" + str(i))
	#plt.show()
	print("Mesh %g: %d tetrahedra, %d vertices, hmax = %g, hmin = %g, errest = %g" % (i, mesh.num_cells(), mesh.num_vertices(), 		mesh.hmax(), mesh.hmin(), sqrt(sum_eta2)))	 
	# input("Press Enter to continue ...")

#plt.figure(1)
#plot(mesh, title="mesh")
#plt.figure(2)
#plot(uh, title="uh")
#plt.show()


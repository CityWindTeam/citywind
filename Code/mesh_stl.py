from fenics import *
from dolfin import *
from mshr import *
from scipy import *
from scipy.interpolate import RegularGridInterpolator
#from astropy.io import ascii

# import arcpy
import numpy as np


#### Topography and mesh
#Read .off file that was created manually
#if not has_cgal():
#    print "DOLFIN must be compiled with CGAL to run this demo."
#    exit(0)
mesh = Mesh()  #empty mesh
#domain = Surface3D("OKC_BUSY_DIST_ext_8_extrude_air_ManuallyFixedMesh - OBJECT (repaired).stl")
#domain = Surface3D("Cube_tri.stl")
#mesh = generate_mesh(domain,1)
#plot(mesh, interactive=True)
## output the mesh. p.209 in the book
#mesh_file = File("OKC_BUSY_DIST_ext_8_extrude_air_ManuallyFixedMesh - OBJECT (repaired).xml")
#mesh_file << mesh


# Read mesh
#mesh=Mesh("OKC_BUSY_DIST_ext_8_extrude_air_ManuallyFixedMesh - OBJECT (repaired).xml")
#plot(mesh, interactive=True)
# Read mesh 2
mesh_file = File("OKC_BUSY_DIST_ext_8_extrude_air_ManuallyFixedMesh - OBJECT (repaired).xml")
mesh_file >> mesh
#plot(mesh, interactive=True)

#### Define function space
V = FunctionSpace(mesh, 'CG', 1) # so that dofs are only in mesh vertices
VFS = VectorFunctionSpace(mesh, 'CG', 1, dim=3)
n = V.dim()
d = mesh.geometry().dim()


### output cell midpoints to set up initial wind field in NUD-fortran

vector_file = File("midpoints.xml")
for cell in cells(mesh):
	p = cell.midpoint()
	#print(p)	
#	vector_file << p



## FEniCS mesh: coordinates of vertices in numpy format 
CoorFE = np.zeros(mesh.num_vertices()) 
CoorFE = mesh.coordinates() 
# The function mesh.coordinates returns the coordinates of the vertices as a numpy array with shape (M,d), M being the number of vertices in the mesh and d being the number of space dimensions.  --- corresponds to the coordinates of the degrees of freedom for P1 elements but not higher.
np.savetxt("meshcoordinates.csv", CoorFE, delimiter=",")
CoorFE2 = V.tabulate_dof_coordinates()
np.savetxt("meshcoordinates2.csv", CoorFE2, delimiter=",")

# Alternative ways to obtain vertex coordinates
# https://fenicsproject.org/qa/1093/obtaining-vertex-values-after-loading-a-mesh
# https://fenicsproject.org/qa/3975/interpolating-vector-function-from-python-code-to-fenics
# https://fenicsproject.org/qa/3932/accessing-the-coordinates-of-a-degree-of-freedom 


from fenics import *
from dolfin import *
from mshr import *
from scipy import *
from scipy.interpolate import RegularGridInterpolator
#from astropy.io import ascii

# import arcpy
import numpy as np


# This code is extended from "oklahoma_single_building" topography (which is originally written from based "poisson_mesh.py"). Initial velocity fields is set up by NUD-Fortran environement. 

# At this moment 20170719 this needs to be done
# Redefine boundaries in terms of the mesh - should be done now (20170719)
# Obtain an appropriate initial windfield. Check the files that are uploaded! 

#### Read initial velocity fields and griddata
print("Read initial velocity fields and griddata")
u0_car = loadtxt("u0.txt", comments="#", delimiter=",", unpack=False)
v0_car = loadtxt("v0.txt", comments="#", delimiter=",", unpack=False)
w0_car = loadtxt("w0.txt", comments="#", delimiter=",", unpack=False)
#u0v0w0_RGI = loadtxt("u0v0w0_RGI.txt", dtype={'formats':('S1','S1','S1')}, delimiter=" ", unpack=False)
#u0v0w0_RGI = np.loadtxt("u0v0w0_RGI.txt", dtype={'names': ('gender', 'age', 'weight'), 'formats': ('f', 'f', 'f')})
u0v0w0_RGI = loadtxt("u0v0w0_RGI.txt")
#u0_RGI = loadtxt("u0_RGI.txt", comments="#", delimiter=",", unpack=False)
#v0_RGI = loadtxt("v0_RGI.txt", comments="#", delimiter=",", unpack=False)
#w0_RGI = loadtxt("u0_RGI.txt", comments="#", delimiter=",", unpack=False)
GRID_ORIGOZ = 0
GRID_ORIGOX = loadtxt("GRID%ORIGOX.txt", comments="#", delimiter=",", unpack=False)
GRID_ORIGOY = loadtxt("GRID%ORIGOY.txt", comments="#", delimiter=",", unpack=False)
GRID_XMIN = loadtxt("GRID%XMIN.txt", comments="#", delimiter=",", unpack=False)
GRID_XMAX = loadtxt("GRID%XMAX.txt", comments="#", delimiter=",", unpack=False)
GRID_YMIN = loadtxt("GRID%YMIN.txt", comments="#", delimiter=",", unpack=False)
GRID_YMAX = loadtxt("GRID%YMAX.txt", comments="#", delimiter=",", unpack=False)
GRID_ZMIN = loadtxt("GRID%ZMIN.txt", dtype='float', comments="#", delimiter=",", unpack=False)
GRID_ZMAX = loadtxt("GRID%ZMAX.txt", dtype='float', comments="#", delimiter=",", unpack=False)
GRID_DX = loadtxt("GRID%DX.txt", comments="#", delimiter=",", unpack=False)
GRID_DY = loadtxt("GRID%DY.txt", comments="#", delimiter=",", unpack=False)
GRID_DZ = loadtxt("GRID%DZ.txt", comments="#", delimiter=",", unpack=False)
#GRID_NCOLS = loadtxt("GRID%NCOLS.txt", comments="#", delimiter=",", unpack=False)
#GRID_NROWS = loadtxt("GRID%NROWS.txt", comments="#", delimiter=",", unpack=False)
GRID_THETA = loadtxt("GRID%THETA.txt", comments="#", delimiter=",", unpack=False)
GRID_NX = loadtxt("GRID%NX.txt",dtype='int', comments="#", delimiter=",", unpack=False)
GRID_NY = loadtxt("GRID%NY.txt",dtype='int', comments="#", delimiter=",", unpack=False)
GRID_NZ = loadtxt("GRID%NZ.txt",dtype='int', comments="#", delimiter=",", unpack=False)

# Vertices of single building Vertices_shp = loadtxt("Vertices_shp.txt", dtype='float', delimiter="\t", comments="#", unpack=False)

#### Topography and mesh
print("Create the mesh")

#Read .off file that was created manually
#if not has_cgal():
#    print "DOLFIN must be compiled with CGAL to run this demo."
#    exit(0)
mesh = Mesh()  #empty mesh
#domain = Surface3D("OKC_BUSY_DIST_ext_8_extrude_air_small_IntersectOnly_Netfabb_Rhino.stl")
#mesh = generate_mesh(domain,1)
mesh_file = File("OKC_BUSY_DIST_ext_8_extrude_air_ManuallyFixedMesh - OBJECT (repaired).xml")
mesh_file >> mesh
# plot(mesh, interactive=True)

#### Define function space
print("Define function space")
V = FunctionSpace(mesh, 'CG', 1) # so that dofs are only in mesh vertices
VFS = VectorFunctionSpace(mesh, 'CG', 1, dim=3)
n = V.dim()
d = mesh.geometry().dim()

#### Find the RHS of the poisson equation (= div (initial velocity field on the FEniCS grid)). 
### Interpolate the initial wind field to the fenics mesh

## FEniCS mesh: coordinates of vertices in numpy format 
#CoorFE = np.zeros(mesh.num_vertices()) 
#CoorFE = mesh.coordinates() 
print("Interpolate to FEniCS mesh and calculate RHS (=div(initialfield))")
CoorFE = V.tabulate_dof_coordinates()
V_dof_coordinates = V.tabulate_dof_coordinates()
V_dof_coordinates.resize((n, d))
V_dof_x = V_dof_coordinates[:, 0]
V_dof_y = V_dof_coordinates[:, 1]
V_dof_z = V_dof_coordinates[:, 2]


# Alternative ways to obtain vertex coordinates
# https://fenicsproject.org/qa/1093/obtaining-vertex-values-after-loading-a-mesh
# https://fenicsproject.org/qa/3975/interpolating-vector-function-from-python-code-to-fenics
# https://fenicsproject.org/qa/3932/accessing-the-coordinates-of-a-degree-of-freedom 

## Cartesian mesh: 
# Coordinates (for the RegularGridInterpolator)!

x_c = linspace(GRID_ORIGOX+GRID_XMIN+0.5*GRID_DX,GRID_ORIGOX+GRID_XMAX-0.5*GRID_DX,GRID_NX)
y_c = linspace(GRID_ORIGOY+GRID_YMIN+0.5*GRID_DY,GRID_ORIGOY+GRID_YMAX-0.5*GRID_DY,GRID_NY)
z_c = linspace(GRID_ZMIN+0.5*GRID_DZ,GRID_ZMAX-0.5*GRID_DZ,GRID_NZ)
# Plot these "simulated" Cartesian coordinates to see if the coincide with NUD-Fortran environment. Print to .vtk


# Initialfield of the Cartesian mesh (for the RegularGridInterpolator)
#The below is applicable (with some error...) when u0 v0 w0 is read 
u0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
v0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
w0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ))
u0v0w0_RGI=np.zeros((GRID_NX,GRID_NY,GRID_NZ,3))
#ir=0
#for iz in range(0,GRID_NZ):
#	for iy in range(0,GRID_NY):
#		for ix in range(0,GRID_NX):
#			u0_RGI[ix][iy][iz]=u0_car[ir]
#			v0_RGI[ix][iy][iz]=v0_car[ir]
#			w0_RGI[ix][iy][iz]=w0_car[ir]
#			ir+=1

ir=0
for iz in range(0,GRID_NZ):
	for iy in range(0,GRID_NY):
		for ix in range(0,GRID_NX):
			u0v0w0_RGI[ix][iy][iz][0]=u0_car[ir]
			u0v0w0_RGI[ix][iy][iz][1]=v0_car[ir]
			u0v0w0_RGI[ix][iy][iz][2]=w0_car[ir]

			u0_RGI[ix][iy][iz]=u0_car[ir]
			v0_RGI[ix][iy][iz]=v0_car[ir]
			w0_RGI[ix][iy][iz]=w0_car[ir]
			ir+=1

# Here would be nice to export to VTK to verify data transport from Fortran to Python/FEniCS
#plot(u0_RGI, title='Initial wind field component u0') # FEniCS plot command of course does not work
N=GRID_NX*GRID_NY*GRID_NZ
#vtkfile=open("u0v0w0_RGI_python.vtk", "w")
#vtkfile.write("# vtk DataFile Version 3.0\r\n")
#vtkfile.write('Initial velocity field u0_RGI intended for interpolation\r\n')
#vtkfile.write('ASCII\r\n')
#vtkfile.write('DATASET STRUCTURED_POINTS\r\n')
#vtkfile.write('%s\t%s\t%s\t%s\r\n' % ('DIMENSIONS', GRID_NX, GRID_NY, GRID_NZ))
#vtkfile.write('%s\t' % ('DIMENSIONS'))
#vtkfile.write('{}\t {}\t {}\r\n'.format(GRID_NX, GRID_NY, GRID_NZ))
#vtkfile.write('%s\t' % ('ORIGIN'))
#vtkfile.write('{}\t {}\t {}\r\n'.format(GRID_ORIGOX, GRID_ORIGOY, GRID_ORIGOZ))
#vtkfile.write('%s\t' % ('SPACING'))
#vtkfile.write('{}\t {}\t {}\r\n'.format(GRID_DX, GRID_DY, GRID_DZ))
#vtkfile.write('%s\t' % ('POINT_DATA'))
#vtkfile.write('%s\r\n' % N)
#vtkfile.write('{}\t {}\t {}\r\n'.format('VECTORS', 'Winddata', 'float'))
#ascii.write([u0[0]], 'u0v0w0_RGI_python.vtk', formats={})
#
#for i in range(0,N):
#	vtkfile.write('{:f}\t {:f}\t {:f}\r\n'.format(u0[i], v0[i], w0[i]))
#vtkfile.close()

## Interpolation to retrieve initial field defined in the FEniCS mesh.

# Define functions to interpolate each space-dimension of the initialfield
fn_u0 = RegularGridInterpolator((x_c,y_c,z_c), u0_RGI, bounds_error=False, fill_value=None) 
fn_v0 = RegularGridInterpolator((x_c,y_c,z_c), v0_RGI, bounds_error=False, fill_value=None) 
fn_w0 = RegularGridInterpolator((x_c,y_c,z_c), w0_RGI, bounds_error=False, fill_value=None)
 
fn_u0v0w0 = RegularGridInterpolator((x_c,y_c,z_c), u0v0w0_RGI, bounds_error=False, fill_value=None)
 

#Interpolate!
u0_FE=fn_u0(CoorFE)
v0_FE=fn_v0(CoorFE)
w0_FE=fn_w0(CoorFE)
u0v0w0_FE=fn_u0v0w0(CoorFE)

#Assemble to verify with Paraview that the initial wind field is correct.
u0v0w0_FE2=np.zeros((n,3)) ##This yields the same resulting vectorfield
u0v0w0_FE2[:,0]=u0_FE
u0v0w0_FE2[:,1]=v0_FE
u0v0w0_FE2[:,2]=w0_FE

#u0v0w0_FE2=np.zeros((3,n))
#u0v0w0_FE2[0,:]=u0_FE
#u0v0w0_FE2[1,:]=v0_FE
#u0v0w0_FE2[2,:]=w0_FE

## Transfer to FEniCS "function"
#https://fenicsproject.org/qa/2715/coordinates-u_nodal_values-using-numerical-source-function
u0 = Function(V)
v0 = Function(V)
w0 = Function(V)
u0v0w0 = Function(VFS)
u0v0w02 = Function(VFS)
u0.vector()[:] = u0_FE # Also possible to import divergence of the initial field from (i.e. the RHS) from NUD Fortran interpolate with Python and apply to the mesh as a scalar field.
v0.vector()[:] = v0_FE
w0.vector()[:] = w0_FE

#uvw.vector()[:] = u0v0w0_FE
#uvw2.vector()[:] = u0v0w0_FE2
assign(u0v0w0.sub(0), u0)
assign(u0v0w0.sub(1), v0)
assign(u0v0w0.sub(2), w0)

#plot(u, title='Initial wind field component u0 in FEniCS mesh', interactive=True)
#plot(v, title='Initial wind field component v0 in FEniCS mesh', interactive=True)
#plot(w, title='Initial wind field component w0 in FEniCS mesh', interactive=True)
#plot(uvw, title='Initial wind field all components in FEniCS mesh', interactive=True)

vtkfile = File('oc_u0_FEniCS.pvd')
vtkfile << u0
vtkfile = File('oc_v0_FEniCS.pvd')
vtkfile << v0
vtkfile = File('oc_w0_FEniCS.pvd')
vtkfile << w0
vtkfile = File('oc_u0v0w0_FEniCS.pvd')
vtkfile << u0v0w0


##Calculate divergence to obtain the R.H.S.

#Make sure everything is in correct dimension and so forth..
#RHS = grad(uvw)[0] + grad(uvw)[1] + grad(uvw)[2]

RHS_VFS = div(u0v0w0)
RHS = project(RHS_VFS, V)
File("oc_RHS.pvd") << RHS


#plot(RHS, title='Source term',  interactive=True)
# Save RHS to file in VTK format
#vtkfile = File('RHS_div_initialfield.pvd')
#vtkfile << RHS

#plot(RHS_VFS, title='Source term',  interactive=True)
#vtkfile = File('RHS_VFS_div_initialfield.pvd')
#vtkfile << RHS_VFS

#### Define boundary conditions chapter 4.4.4. in the Tutorial. The boundary conditions are such that the volume aligns with the flow direction, the inflow is thus in line with to x[0]. The inflow is constant (u0) over the leading face (Neumann, dlambda/dn=0) whereas the outflow may vary freely (Dirichlet, lambda=0). No flow is allowed to exit in other directions, buildings and spaceboundaries x[1] and x[2], hence inhomogenous Neumann, dlambda/dn=2alpha^2*uo*nx (and v0=w0=0 in the initial field.) See project description for FEniCS course for details. //CL

# First mark the different boundaries using MeshFunction "facetfunction" and SubDomain.
print("Mark boundaries and setup BC's") 
boundary_markers = FacetFunction('size_t',mesh)
boundary_markers.set_all(0)  # Only inflow and outflow boundary needs to be modified from this default.

class OutflowBND(SubDomain): # spacedomain x[0] positive; outflow boundary

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], max(V_dof_x), tol)

bx1 = OutflowBND()
bx1.mark(boundary_markers, 1)	



class InflowBND(SubDomain): # spacedomain x[0] negative; inflow boundary

	def inside(self, x, on_boundary):
		tol = 1E-14
		return on_boundary and near(x[0], min(V_dof_x), tol)

bx2 = InflowBND()
bx2.mark(boundary_markers, 2)	


### Implement the boundary conditions	

alpha = Constant(1.0)

n=FacetNormal(mesh)
#neu_bc=2*alpha*alpha*dot(u0v0w0, n) # There is actually a double minus sign here. one for the facetnormal, found out with the manufactured solution. And one in the boundary condition itself.
neu_vec = as_vector([u0, v0, w0]) 
neu_bc=2*alpha*alpha*dot(neu_vec, n) # There is actually a double minus sign here. one for the facetnormal, found out with the manufactured solution. And one in the boundary condition itself.


boundary_conditions =  {0: {'Neumann': neu_bc},  # buildings and spacedomains aligned with wind (u0 or x[0])
			1: {'Dirichlet': 0}, # spacedomain; outflow boundary
			2: {'Neumann': 0}}   # spacedomains: inflow 







#from IPython import embed; embed()
# Visualizee subdomains, to see that they are correct
#plot(boundary_markers, interactive=True)
vtkfile = File('oc_boundary_markers.pvd')
vtkfile << boundary_markers 

# Define variational problem
print("Define variational problem, redefine surface measure ds, and collect BC's")
LagrangeMultiplier = TrialFunction(V)
testv = TestFunction(V)
#f = Constant(RHS)
#f = Constant(-6.0) #-6 is a made up number for the source term, see NUD manual for its real formulation /CL

#Redefine the measure "ds" in terms of our boundary markers
ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)

# Collect (inhomogenous) Neumann boundary conditions. There is only one Dirichlet conditions and therefore it does not need to be collected. There are none Robin conditions. 
integrals_N = []
for i in boundary_conditions:
	if 'Neumann' in boundary_conditions[i]:
		if boundary_conditions[i]['Neumann'] != 0:
			g = boundary_conditions[i]['Neumann']
			integrals_N.append(g*testv*ds(i))

# Collect Dirichlet boundary conditions
bcs = []
for i in boundary_conditions:
	if 'Dirichlet' in boundary_conditions[i]:
		bc = DirichletBC(V, boundary_conditions[i]['Dirichlet'], boundary_markers, i)
		bcs.append(bc)
		

# Continue defining variational problem

a = dot(grad(LagrangeMultiplier), grad(testv))*dx
# g = Expression('4*x[1]') #is replaced with other bc's 
# L = f*v*dx-sum(integrals_N)
L = div(u0v0w0)*testv*dx-sum(integrals_N)

#L = f*v*dx #This is the code for Dirichlet bc's only. Should be equivalent as long as the Neumann-bc-term g=0./CL

# Compute solution
print("Compute solution")
LagrangeMultiplier = Function(V)
solve(a == L, LagrangeMultiplier, bcs)

# Plot solution
LagrangeMultiplier.rename('LagrangeMultiplier', 'LagrangeMultiplier_solution')
#plot(LagrangeMultiplier, wireframe=True, interactive=True)


# Save solution to file in VTK format
vtkfile = File('oc_LagrangeMultiplier.pvd')
vtkfile << LagrangeMultiplier

#### Find the winddata

# Final windfield
#u = Function(V)
#v = Function(V)
#w = Function(V)

#u = u0 + (1/2*alpha)*LagrangeMultiplier.dx(0)
#v = v0 + (1/2*alpha)*LagrangeMultiplier.dx(1)
#w = w0 + (1/2*alpha)*LagrangeMultiplier.dx(2)

print("Obtain final windfield from Euler-Lagrange equations")
uvw = Function(VFS)

### Look at the correction from the initialfield
#corru = Function(V)
#corrv = Function(V)
#corrw = Function(V)
LMdx = Function(V)
LMdx = project(LagrangeMultiplier.dx(0), V)
LMdy = Function(V)
LMdy = project(LagrangeMultiplier.dx(1), V)
LMdz = Function(V)
LMdz = project(LagrangeMultiplier.dx(2), V)
LM_derivatives = Function(VFS)
assign(LM_derivatives.sub(0), LMdx)
assign(LM_derivatives.sub(1), LMdx)
assign(LM_derivatives.sub(2), LMdx)
vtkfile = File('oc_LMdx.pvd')
vtkfile << LMdx


#plot(LMdx, wireframe=True, interactive=True)
#plot(LMdy, wireframe=True, interactive=True)
#plot(LMdz, wireframe=True, interactive=True)
LM_derivatives.rename('LM_derivatives', 'LM_derivatives')
#plot(LM_derivatives, wireframe=True, interactive=True)
vtkfile = File('LM_derivatives.pvd')
vtkfile << LM_derivatives

u = project(u0+LMdx, V)
v = project(v0+LMdy, V)
w = project(w0+LMdz, V)
assign(uvw.sub(0), u)
assign(uvw.sub(1), v)
assign(uvw.sub(2), w)

vtkfile = File('oc_uvw.pvd')
vtkfile << uvw
uvw.rename('uvw','oc_uvw')
#plot(uvw, wireframe=True, interactive=True)

## Get the prefactor there
u2v2w2 = Function(VFS)
u2 = project(u0+(1/(2*alpha*alpha))*LMdx, V)
v2 = project(v0+(1/(2*alpha*alpha))*LMdy, V)
w2 = project(w0+(1/(2*alpha*alpha))*LMdz, V)
assign(u2v2w2.sub(0), u2)
assign(u2v2w2.sub(1), v2)
assign(u2v2w2.sub(2), w2)
u2v2w2.rename('u2v2w2','oc_u2v2w2')
plot(u2v2w2, wireframe=True, interactive=True)
vtkfile = File('oc_u2v2w2.pvd')
vtkfile << u2v2w2


debug1 = True
if debug1:

	# Print number of vertices in mesh
	#print('%s is the number of vertices in the mesh' % mesh.num_vertices()) 
	print('Number of vertices in the mesh:', mesh.num_vertices())

        # Print the Dirichlet conditions
        print('Number of Dirichlet conditions:', len(bcs))
        if V.ufl_element().degree() == 1:  # P1 elements
            d2v = dof_to_vertex_map(V)
            coor = mesh.coordinates()
            for i, bc in enumerate(bcs):
                print('Dirichlet condition %d' % i)
                boundary_values = bc.get_boundary_values()
		print ('Number of boundary values in the above Dirichlet condition:', len(bc.get_boundary_values()))
#                for dof in boundary_values:
#                    print('   dof %2d: u = %g' % (dof, boundary_values[dof]))
#                    if V.ufl_element().degree() == 1:
#                        print('    at point %s' %
#                              (str(tuple(coor[d2v[dof]].tolist()))))









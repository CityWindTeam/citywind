from fenics import *
from dolfin import *
from mshr import *
from scipy import *
from scipy.interpolate import RegularGridInterpolator
#from astropy.io import ascii

# import arcpy
import numpy as np

#### Topography and mesh

#if not has_cgal():
#    print "DOLFIN must be compiled with CGAL to run this demo."
#    exit(0)
mesh = Mesh()  #empty mesh
mesh_file = File("OKC_BUSY_DIST_ext_8_extrude_air_ManuallyFixedMesh - OBJECT (repaired).xml")
mesh_file >> mesh
plot(mesh, interactive=True)

# https://fenicsproject.org/olddocs/dolfin/1.3.0/python/programmers-reference/mesh/refinement/refine.html

mesh = refine(mesh)
plot(mesh, interactive=True)

#To only refine cells with too large error, define a boolean MeshFunction over the mesh and mark the cells to be refined as True.
#cell_markers = CellFunction("bool", mesh)
#cell_markers.set_all(False)
#for cell in cells(mesh):
#    # set cell_markers[cell] = True if the cell's error
#    # indicator is greater than some criterion.
#mesh = refine(mesh, cell_markers)

# https://fenicsproject.org/olddocs/dolfin/1.5.0/python/demo/documented/auto-adaptive-poisson/python/documentation.html

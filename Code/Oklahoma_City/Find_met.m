Ustar=sqrt(1.97^2+2.82^2)*0.4/log(10/0.6) %Given by Oklahoma city field trials (Jan B)

Ustar =

    0.4891

z0=0.6      %Given by Oklahoma city field trials (Jan B)

z0 =

    0.6000
    
    HL=[10 32 64 100 200 550]; % The selected Hirlam levels. 10 m is to verify.
    
    U=Ustar/0.4*log(HL./z0) % wind speed and height for Hirlam points. There will be only u-wind since grid is rotatated to align with u-wind 
    U =

    3.4400    4.8621    5.7097    6.2553    7.1028    8.3397
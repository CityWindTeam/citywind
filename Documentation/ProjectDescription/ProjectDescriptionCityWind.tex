% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

%%% The "real" document content comes below...

\title{CityWind}
\author{Christian Lejon,\\ Swedish Defence Research Agency,\\christian.lejon@foi.se,\\ +4690106641\\
Gustav Ludvigsson,\\ Uppsala University, Department of Information Technology,\\ gustav.ludvigsson@it.uu.se,\\ +46761852662}

\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle

\section{Summary}

The main purpose of this three week project of the Ph.D. course in the finite element method is to simulate the windfield in a city under realistic assumptions. This includes to create a mesh over the computational domain (the city), choose suitable mathematical formulation, solve with FEniCS, compare, and visualize the results. Two formulations are suggested; one CFD method that rely on the Reynolds-Averaged Navier-Stokes equation and one simpler non-CFD semi-empiric after Sherman and Röckle.

\section{Background}

One direct application from knowing the windfield in a city is to calculate the atmospheric spread of a hazardous substances (may it be accidental or by-purpose release of chemical, biological or radiological/nuclear substances) over a city. This is of interest particularly to the Swedish Armed Forces as well as the Swedish Civil Contingency Agency (MSB) with their rescue services. The Swedish Defence Research Agency (FOI) is on behalf of the Swedish Radiation Safety Authority (SSM) developing a fast and robust "dispersion engine" and part of this is the windfield generator. 
Furthermore “One of the major forces of our modern society is urbanization. ... it becomes important to simulate cities in order to study effects of wind, temperature, pedestrian flow, traffic flow etc. In other words, this calls for the development of virtual cities that can be simulated, analyzed and optimized before the actual city is built.” as stated by prof. Logg.

\section{Tasks in detail}

\subsection{Create mesh from GIS-data}
The goal is create a script (in Python) that reads shapefile data and outputs a .stl file, which is a triangular mesh of the surface of the domain. This data structures is valid to create the computational mesh for FEM solvers such as FEniCS. Information sources to start with for this task are: https://pypi.python.org/pypi/pyshp, http://www.gdal.org/, https://pcjericks.github.io/py-gdalogr-cookbook/
However, brought into this project is a computational mesh over Oklahoma city that can be used for the tasks below. FOI is currently developing a tool to create 3D computational meshes over the entire Sweden based on map data (.shp) of Lantmäteriet.

\subsection{CFD Solution Method: Reynolds Averged Navier-Stokes (RANS) equation}
Initial trials showed that the Navier-Stokes equation is not suitable for a fast windfield simulator or large domains because of time/resource requirements. Turbulent flow is commonly modelled with the RANS-equation with different schemes for the turbulent viscosity parameter, and, luckily a FEniCS framework already seems to exist.\cite{FEniCS_RANS}  "Extensive experimentation with both models and numerical solution methods" is believed required.  

\subsection{Non-CFD Solution Method: Röckles method}
The procedure is outlined in Röckles' dissertation.\cite{Rockle} In essence; an initial realistic vertically profiled windfield ($\vec{u_0}$) with various empirically determined wake-zones around, between, and above buildings is to be modified to meet a zero divergence requirement while minimizing the difference between the inital field and the final windfield ($\vec{u}$). With the method of Lagrange multiplicators ($\lambda$) and variational formalism this gives a poisson equation for $\lambda$ and $\vec{u}$ is then obtained from the Euler-Lagrange equations. 

On the \textit{prescribed boundary} (in terms of $\vec{u}$) equations for $\lambda$ is obtained directly from the Euler-Lagrange equations; $\frac{\partial\lambda}{\partial n} = 2\alpha^2(u-u_0)n_x + 2\beta^2(v-v_0)n_y + 2\gamma^2(w-w_0)n_z $.\cite{FOI_memo_1608} On the in-flow boundary we prescribe the final windfield to be unchanged, ($\vec{u} = \vec{u_0}$), hence a homogenous Neumann BC $\frac{\partial\lambda}{\partial n} = 0$. As we progress downwind we encounter parts of the boundary where $\vec{u}$ is prescribed to be in the plane of boundary, i.e. \textit{free-slip condition} $ \vec{u} \cdot \vec{n} = 0$. Equivalently one can say that no flow is allowed in or out of the buildings, and, since the domain shall be aligned with the $\vec{u_0}$, nor across the sides or top of the domain. Rearranging terms we derive explicit expression for the \textit{free-slip} Neumann condition;  $ 0 = \frac{1}{2\alpha^2}\frac{\partial\lambda}{\partial x}n_x + \frac{1}{2\beta^2}\frac{\partial\lambda}{\partial y}n_y + \frac{1}{2\gamma^2}\frac{\partial\lambda}{\partial z}n_z + u_0n_x  + v_0n_y + w_0n_z $. Note that this is anisotropic and only in the case $\alpha = \beta = \gamma$ the problem can be solved with explicit imposition ($\frac{\partial\lambda}{\partial n} = 2\alpha^2u_0n_x$) of the BC. Finally, the outflow boundary adjacent to the in-flow is the only \textit{free boundary} to accomodate for topography induced changes downwind and $\vec{u}$ is to vary freely, hence $\lambda=0$ to fullfill boundary variation requirements. \cite{FOI_memo_1608} 

One problem to resolve is anisotropy in the Neumann BC’s, originating from different lateral vs. vertical weightfactors ($\alpha = \beta \neq \gamma$) in the modification reflecting stratification in the atmosphere. The suggested method to fix this is to implicitly add the free-slip boundary ($\Gamma$) in the weak formulation: 

%d\Gamma_{N}
\begin{equation}
 \int_{\Omega} (k\nabla \lambda) \cdot \nabla v d\Omega - 
 \int_{\Gamma} (k\nabla\lambda) v d\Gamma + 
 C(h) \cdot\int_{\Gamma} (k\nabla\lambda) \cdot n v d\Gamma -
 \int_{\Omega} fv d\Omega = 0
\end{equation}

where $h$ is the mesh size, $C(h)$ is a sufficiently large to force the added boundary integral to zero (i.e impose the Neumann condition) and $v$ the trial function. The source term is the divergence of $\vec{u_0}$, $f=\frac{\partial u_0}{\partial x} + \frac{\partial v_0}{\partial y} + \frac{\partial w_0}{\partial z}$ and $k$ the 3x3 diagonal matrix with $\alpha$, $\beta$ and $\gamma$ as its diagonal elements.  First we aim to solve in standard procedure with explicit isotropic BC's, then we will try implement the implicit procedure and validate against the isotropic case. If successful then the final step will be implicit procedure with anisotropic BC's.

\subsection{Compare, visualize and disseminate the results}
Benchmark derived results with other simulations and the field trials in Oklahoma City 2003 \cite{QUIC-URB} (and references therein). Build a 3D model of the city in the Unity Game Engine (https://unity3d.com/) and add textures to create an appealing visualization. Qualitative comparison to FOI "in-house" CFD models with e.g. cross-sectional views around certain terrain features in Paraview. Prepare poster and oral presentation for the last week of the course August 14th -18th, 2017.  

\begin{thebibliography}{9}
\bibitem{FEniCS_RANS} 
Mikael Mortensen, Hans Petter Langtangen and Garth N. Wells
\textit{A FEniCS-based programming framework for modeling turbulent flow by the Reynolds-averaged Navier–Stokes equations}. 
Advances in Water Resources, Volume 34, Issue 9, September 2011, Pages 1082–1101
 
\bibitem{Rockle} 
Rainer Röckle 
\textit{Bestimmung der Strömungsverhältnisse im Beriech komplexer Bebaungsstrukturen}. (German) 
[\textit{Determination of the flow conditions around complex building structures}]. 
Darmstadt, Techn. Hochsch., Dissertation; 1990.

\bibitem{QUIC-URB} 
Michael J. Brown, Matthew A. Nelson, Michael D. Williams, Akshay Gowardhan, and Eric R. Pardyjak
\textit{A non-CFD modeling system for computing 3D wind and concentration fields in urban environments}. 
The Fifth International Symposium on Computational Wind Engineering, Chapel Hill, North Carolina, USA May 23-27, 2010

\bibitem{FOI_memo_1608} 
Lage Jonsson, Leif Persson, Lennart Thaning, Pontus von Schoenberg, Sune Westman och Jan Burman
\textit{Plan för utveckling av vindfältsmodell för NUD, Nordic Urban Dispersion Model}. 
Totalförsvarets Forskningsinstitut, FOI memo 1608, 2005

\end{thebibliography}


\end{document}
